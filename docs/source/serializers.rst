.. automodule:: interfaces_adapters.serializers.json.dataByMeterSerializer
    :imported-members:
    :members:
    :no-inherited-members:

    .. autoclass:: dataByMeterEncoder
        :members:

.. automodule::: interfaces_adapters.serializers.json.metersSerializer
    :imported-members:
    :members:
    :no-inherited-members:

    .. autoclass:: metersListEncoder
        :members: