.. automodule:: use_cases.classEnd2End
    :no-members:
    :no-inherited-members: 

    .. autoclass:: Classend2end
        :members:


.. automodule:: use_cases.classifier
    :no-members:
    :no-inherited-members: 

    .. autoclass:: Classifier
        :members:

.. automodule:: use_cases.DNN
    :no-members:
    :no-inherited-members:

    .. autoclass:: RNNSeries
        :members:
    
    .. autoclass:: SeqDataset
        :members:

    .. autoclass:: SeqDataset_pred
        :members:

    .. autoclass:: Trainer
        :members:

.. automodule:: use_cases.featureExtractionTime
    :no-members:
    :no-inherited-members:

    .. autoclass:: FeatureExtraction
        :members:

.. automodule:: use_cases.fromTensors
    :no-members:
    :no-inherited-members:

    .. autoclass:: FormTensors
        :members:

.. automodule:: use_cases.getLabelsProblems
    :no-members:
    :no-inherited-members:

    .. autoclass:: labelsProblem
        :members:

.. automodule:: use_cases.kmeansMethod
    :no-members:
    :no-inherited-members:

    .. autoclass:: kMeansML
        :members:

.. automodule:: use_cases.lSVMMethod
    :no-members:
    :no-inherited-members:

    .. autoclass:: linearSVM
        :members:

.. automodule:: use_cases.manageIncomingFiles
    :no-members:
    :no-inherited-members:

    .. autoclass:: manageInputFile
        :members:

.. automodule:: use_cases.read_preprocess
    :no-members:
    :no-inherited-members:

    .. autoclass:: ReadPreprocess
        :members:

.. automodule:: use_cases.utils.a_sync_methods
    :members:
    :no-inherited-members:

.. automodule:: use_cases.utils.run_tech
    :members:
    :no-inherited-members:

.. automodule:: use_cases.utils.tools
    :members:
    :no-inherited-members:
