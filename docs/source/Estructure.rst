        ├── app.py |br|
        ├── data_files |br|
        ├── docs |br|
        ├── __init__.py |br|
        ├── install.sh |br|
        ├── logs |br|
        │   └── pme.log |br|
        ├── manage.py |br|
        ├── pme_app |br| 
        │   ├── domain |br|
        │   │   ├── abstract_entities |br|
        │   │   │   ├── chech_forms.py |br|
        │   │   │   └── __init__.py |br|
        │   │   ├── entities |br|
        │   │   │   ├── blackListTokenModel.py |br|
        │   │   │   ├── dataByMeterModel.py |br|
        │   │   │   ├── filesByUserModel.py |br|
        │   │   │   ├── __init__.py |br|
        │   │   │   ├── metersModel.py |br|
        │   │   │   ├── performanceBytechniqueModel.py |br|
        │   │   │   └── userModel.py |br|
        │   │   └── __init__.py |br|
        │   ├── external_interfaces |br|
        │   │   └── flask_server |br|
        │   │       ├── app.py |br|
        │   │       ├── __init__.py |br|
        │   │       └── settings.py |br|
        │   ├── __init__.py |br|
        │   ├── interfaces_adapters |br|
        │   │   ├── __init__.py |br|
        │   │   ├── rest_adapters |br|
        │   │   │   ├── __init.py |br|
        │   │   │   └── web_services |br|
        │   │   │       ├── dataByMeterService.py |br|
        │   │   │       ├── featuresAndTechniquesService.py |br|
        │   │   │       ├── getPerformanceDataByModel.py |br|
        │   │   │       ├── __init__.py |br|
        │   │   │       ├── loadFileService.py |br|
        │   │   │       ├── loadLabelsService.py |br|
        │   │   │       ├── loginService.py |br|
        │   │   │       ├── logoutService.py |br|
        │   │   │       ├── meterListService.py |br|
        │   │   │       ├── registerUserService.py |br|
        │   │   │       ├── rootService.py |br|
        │   │   │       └── userInfoService.py |br|
        │   │   └── serializers |br|
        │   │       └── json |br|
        │   │           ├── dataByMeterSerializer.py |br|
        │   │           ├── __init__.py |br|
        │   │           └── metersSerializer.py |br|
        │   ├── repository |br|
        │   │   ├── db_services |br|
        │   │   │   ├── checkLoginService.py |br|
        │   │   │   ├── checkLogoutService.py |br|
        │   │   │   ├── filesByuser.py |br|
        │   │   │   ├── getDataByMeterService.py |br|
        │   │   │   ├── getMetersService.py |br|
        │   │   │   ├── getUserInfo.py |br|
        │   │   │   ├── __init__.py |br|
        │   │   │   ├── meterTableServices.py |br|
        │   │   │   ├── performaceByModelService.py |br|
        │   │   │   └── registerUserService.py |br|
        │   │   └── __init__.py |br|
        │   └── use_cases |br|
        │       ├── classEnd2End.py |br|
        │       ├── classifier.py |br|
        │       ├── DNN.py |br|
        │       ├── featureExtractionTime.py |br|
        │       ├── frecFeatureExtraction.py |br|
        │       ├── fromTensors.py |br|
        │       ├── getLabelsProblems.py |br|
        │       ├── __init__.py |br|
        │       ├── kmeansMethod.py |br|
        │       ├── lSVMMethod.py |br|
        │       ├── manageIncomingFiles.py |br|
        │       ├── read_preprocess.py |br|
        │       └── utils |br|
        │           ├── a_sync_methods.py |br|
        │           ├── __init__.py |br|
        │           ├── run_tech.py |br|
        │           └── tools.py |br|
        ├── requirements |br|
        │   ├── dev.txt |br|
        │   ├── prod.txt |br|
        │   └── test.txt |br|
        ├── requirements.txt |br|
        ├── run.py |br|
        ├── shared.py |br|
        ├── test |br|
        └── uploads |br|

.. |br| raw:: html

      <br>