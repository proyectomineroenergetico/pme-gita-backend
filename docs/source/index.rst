.. Proyecto Minero Energético documentation master file, created by
   sphinx-quickstart on Sun Mar  7 14:39:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Proyecto Minero Energético's documentation!
======================================================

Este proyecto fue desarrollado bajo la metodologia de arquitectura limpia.:


.. toctree::
   :maxdepth: 3
   :caption: Contents:

Estructura del proyecto
=======================

.. include:: Estructure.rst

Documentación por capas acorde a la estructura del proyecto
===========================================================

Dominio
^^^^^^^

.. include:: domain.rst

Casos de uso
^^^^^^^^^^^^

.. include:: use_cases.rst

Repositorio
^^^^^^^^^^^

.. include:: repository.rst

Interfaces
^^^^^^^^^^
.. include:: interfaces.rst

Serializadores
^^^^^^^^^^^^^^

.. include:: serializers.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
