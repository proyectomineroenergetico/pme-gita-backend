#!/usr/bin/env bash
trap "set +x; sleep 1; set -x" DEBUG
#========================== CONSTANT VALUES ===============================
#==========================================================================
PROJECT_DIR="$(pwd)"
PARENT_DIR="$(dirname $PROJECT_DIR)";
CURRENT_GROUP="$(id -g -n)"
SERVER_ADDRESS=127.0.0.1
NAME_DATA_BASE="pme_db"
PASSWORD_DATABASE="ADMpj@01"
USER_DATABASE="pmeadm"
USAGE="
NAME
	$(basename "$0") - UTILS 

OPTIONS:

    -h |--help  show this help text

    -i |--install=[APP INSTALLATION]

    -ui |--install-gui=[GUI INSTALLATION]

    -g |--gui=[INSTALL GRAPHICAL USER INTERFACE]

"


create_pmeMain_serviceFile()
{
PYTHON_BIN="$(which python3.8)"
echo "[Unit]
Description=PME APP
Requires=postgresql.service,rqworker@1.service
After=network.target

[Service]
WorkingDirectory=${PROJECT_DIR}
EnvironmentFile=${PROJECT_DIR}/.env
ExecStart=${PYTHON_BIN} run.py
Restart=on-failure
    RestartSec=5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pmeapp
User=${USER}

[Install]
WantedBy=multi-user.target" > ./pme_app/use_cases/utils/systemd/pmeapp.service 
}

create_pmeMain_envFile()
{
echo "PYTHON_ENV=production
HTTP_PORT=8181
SECRET_KEY = "MySuperKey"
ROUNDS = 13
UPLOAD_FOLDER = "/uploads"
DATABASE=${NAME_DATA_BASE}
DATABASE_USER=${USER_DATABASE}
HOST="0.0.0.0"
DATABASE_PASSWORD=${PASSWORD_DATABASE}
LOG_FILES_DIRECTORY="/logs"
EMAIL_USER="proyectomineroenergetico@gmail.com"
EMAIL_PASSWORD="proyectomineroenergeticoUdeA2020"" > $PROJECT_DIR/.env

}
create_rq_worker_serviceFile()
{
rw_path=$(type -a rq | awk '{print $NF}')
home=$(echo $HOME)
echo "[Unit]
Description=RQ Worker Number %i
After=network.target

[Service]
Type=simple
WorkingDirectory=${home}
Environment=LANG=en_US.UTF-8
Environment=LC_ALL=en_US.UTF-8
Environment=LC_LANG=en_US.UTF-8
ExecStart=${rw_path} worker
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID
PrivateTmp=true
Restart=always

[Install]
WantedBy=multi-user.target
"> ./pme_app/use_cases/utils/systemd/rqworker@.service
}

install_project()
{
sudo apt update 
sudo apt install gcc g++ make -y
sudo apt install build-essential -y

echo "*******Creating a env file********"
$(create_pmeMain_envFile)
echo "******PYTHON3"
if ! hash python3.8 2>/dev/null; then
    sudo apt install python3 python3-dev -y

fi
python3 --version
echo "python3 installed"

echo "******PYTHON-PIP"
if ! hash pip3 2>/dev/null; then
    sudo apt install libpq-dev python3-pip -y 
fi
pip3 --version
echo "pip3 installed"
echo "******Redis Server"
if ! hash redis-server 2>/dev/null; then
    sudo apt install redis-server -y
fi 
redis-server --version
echo "redis installed"

# PYTHON MODULES 
echo "******PYTHON MODULES REQUIRED"
export TMPDIR='/var/tmp'
pip3 install --user -r requirements.txt
pip3 install torch==1.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
sudo apt-get install python3-matplotlib python3-numpy python3-pil python3-scipy build-essential cython


# POSTGRES
sudo apt remove postgresql -y
echo "******POSTGRES"
if ! hash psql 2>/dev/null; then
    sudo apt -y install postgresql postgresql-contrib;
    
    sudo -u postgres createuser --superuser ${USER_DATABASE}
    sudo -u postgres psql -c "ALTER USER ${USER_DATABASE} WITH PASSWORD '${PASSWORD_DATABASE}';"
    sudo service postgresql restart
fi
psql --version   
echo "postgresql installed"

echo "================ Enabling PME process management =================="
if [ ! -f /etc/systemd/system/pmeapp.service ]; then
    if [ ! -d ./pme_app/use_cases/utils/systemd ]; then
        mkdir ./pme_app/use_cases/utils/systemd
    fi
    $(create_pmeMain_serviceFile)
    sudo cp ./pme_app/use_cases/utils/systemd/pmeapp.service /etc/systemd/system
    sudo systemctl daemon-reload
    sudo systemctl enable pmeapp.service
    sudo systemctl start pmeapp.service
fi

echo "================ Enabling rqworker process management =================="
if [ ! -f /etc/systemd/system/rqworker@.service ]; then
    if [ ! -d ./pme_app/use_cases/utils/systemd ]; then
        mkdir ./pme_app/use_cases/utils/systemd
    fi
    $(create_rq_worker_serviceFile)
    sudo cp ./pme_app/use_cases/utils/systemd/rqworker@.service /etc/systemd/system
    sudo systemctl daemon-reload
    sudo systemctl enable rqworker@1.service
    sudo systemctl start rqworker@1.service
fi

#Reloading environment
. ~/.bashrc

#Finishing and cleaning
sudo apt -y autoremove
mkdir ${PROJECT_DIR}/uploads
mkdir ${PROJECT_DIR}/data_files
mkdir ${PROJECT_DIR}/logs
touch ${PROJECT_DIR}/logs/pme.log
sudo chown ubuntu ${PROJECT_DIR}}/* 
}



configure_db()
{
echo "========== SETTING UP SION CONTROLLER DATABASE ========="
sudo -u postgres dropdb --if-exists ${NAME_DATA_BASE}
sudo -u postgres createdb ${NAME_DATA_BASE}

#Create needed tables into sdiot database
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
}

install_gui()
{
echo "========== Installing Front End ========="
cd $PARENT_DIR
mkdir gui
cd gui
echo "-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABCNTkFFoI
GttvLRU+Wr7VJMAAAAEAAAAAEAAAGXAAAAB3NzaC1yc2EAAAADAQABAAABgQD3IfBvgvpD
F0WCHW8Rm3NPOvZO5g18i9CS5cS14E+i6kWJFFrLJFwDFG6NZo71qaiZOMWYZuFVIae7DY
H/Y12nT2tyuuzPNHjdr5euz21n33u7PdTKQsxoZGF27c5csBYveECTwO891LobkNB7/Oxr
A2nKA8CDgYXLP9xd9lS69GeKpBH1LnPlcpOcHQHygaTkYHthpWjZM0DHmSddAWUT9u9LY+
YiPTIUnr9tuQDRH64FZI37Py7XA9Pmcwg5O2RsIIHPTzTwsMsEwMz0lTJaR63lwpEoZXYa
FzYhkGmNi03NKnf8mM1QXItrWT78Nu/9RsSaxhHntCNYtHTbs/5P5bZe/zsck8+/5tiZvZ
J4XXjXNEAkqOr+h+3qxfI2VzvDxLQwD/4rQOqH8HGvDpezBPTQhYOZi+esFVJvGUzEgrYx
n8gk77WnS7KfMppDSPnX+HsqFSE2AGOmMe257u3Q7mDv0cXzTJzEj8PHzE+T8WCYA2i6rT
EBKpX8PufVHV0AAAWQzQRSnL/3Cprh7T2+40vAVB+nQKV9y4snFEJr9HlnDmDvzV+EGXDP
05bP0RIOYR/+RfGMkyI5JymfGNKD6Qqdpq+xdSZcWBDtBm1v4a/Ym5rX5hea/FnPknGxkU
OhIsPGrYPEqczWESKeBDiqpFuIcwps6ZWIcJh+OdXynvf7s0bTtwuWB6vniWv4K5C9ELG1
9nDDVMJcEAeu68iiztRawo//bf54zUsaOPFj3/ZeRtXRQaIEVmCWboCKZqELpCCRgqYaRI
8tKij7koYvhykdkr1NWFEIF97ylUyo+keVWrO9hViNDlp7i28ZmD/0JCdLZR6aSITQ3J2c
0OFLrBjpl4J684lBuhIhBQn08fPyyicLcS1sxUb+EP78mVVSNin76NxQVXpefCgllyMgjP
lIhM+WzN5V481jbugOUxoR7UpiItiQK0UdTUehpYesWRQSXENH7dwL1luw15rl8W2wV/J/
4ZUxFAYC/UfS13ElmDbCvfiej4sW5lEHieojUG0EnGy2RiHxWBexvaGcOgsCx7qDghnQ+A
kDusa/NgeGc+eqJutrc1MdMMnMhUqzwB6I/iTNTUpav9r2/Lye2NmBNc0bLFSJmCrxfhCG
C5P0PXdoTBFwCb3dwpa9DJre5IO/QBDoq5uSkov33wq/AdZcdmJGtGfDup5/L9MEBuuxOm
O33H2FYLINTSsaglKnvRGUSy4z7OvZfdjft78RUJ9p6GvzW8noeqarJjYvk3HgtO+2OcXg
kt1BAWo23MyICrFLFgHcO/PYiSKmEL6rgUn3xAhkvoRQrSPvHBus7euJczZ0/odi8RrL2j
QxQHcRjcDBFLz5iTWz7PXfeE+2J4xxooXx5nIV8kGZHgqBdXTngmConpFq/vyK+TudE8Vn
5iy+jE5Y+ECSbtrrY9ri9+XdZyQoexSjC9jkiylJyAzQSufB/ZIaTokk1DUJbco4SN5/NL
TW2hw+PlUQozRSZxkZ0E7LHBfhG5jelw+03t4Y9d8j9zfMV0Wbqfu+XXfcIxA4a9+9iU9p
2lL8sv0NSnBL+4QnMoZ1q15YRO4mxUNBxaGQYwTzWcbcpHbxPHXYnHPRoi56zCOEUQZKKg
3JR8LYU+P1ZOg6gbC30ZZZwxE52QjfAlH3oYb84yj10gM/DvXXfwzuBg3IByPGg2IUrLN2
qZpzhYvsQmIB/oAC8U8fxIDNu7CTqkKrvkbu1td/gnCJKLikajsOjyVCG22ulJh+Gpe58q
xNSjIc39FJQc1DILaKDxnZvzOs7p6UO3LaMmr9r85LHJxw7sCNezwLivz1reAYTW1ZhTuy
nTVTjSduzkG7oqrcN7a7wUsx84hAD6yJyegsCxmOSerHKyieqVH41JMAK33+5KwdlxEM2y
vHnzb09TKslsGOH7Suv561/7nFKr0QT/UQthFGne6GbAOKq0yI+XqH7+7Bj+X5d3jf7w2e
B2A52EeUICtAByBmblOfnMSiDqcgxczI5UAh5qhPPDQ5GrwyEvD9MU5lWVPCA0oghwECwf
jbKrneB7YT6LW9LKcEz7qj2mtt0wJHqjA/q+nN695OZimnrMbojRohjV5xnUVI9LUhg1Vr
fy13LH7kxkGODMvjPR9Wx03QzpGriqA+/d6ounGmE8R3vjlH/Tw2JZFGY1nOYqvPqnpWPM
rGZUgsRPQ33mxk0/U87V9bUOui0o5CuOqoRelOrci0160TeuN7V+jsBh4kMqmfny6RaW1r
acVafX+9UcOqcFS7GDtd+3xXjhAiZv1KrR82VZ2zol3dj4jHrkbgbsfQ6GSdRINqgiP0WF
/dIMpFi5hlUNs97hS2ReDc76PPx18ZdhP9ubP8Ux8Xa+hw2c2AytZuA1utloOd3U9wKHGK
H+4YtVjHzV7lojvfZBr/nrB+7wg=
-----END OPENSSH PRIVATE KEY-----">id_rsa_gui
eval `ssh-agent -s`
ssh-add $PARENT_DIR/gui/id_rsa_gui
sudo apt install xsel
git clone git@bitbucket.org:proyectomineroenergetico/pme-gita-ui.git
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
source ~/.bashrc
nvm install 14
node --version
npm --version



echo "replace url back"
fe=<<EOF 
export const HTTP_STATUS = {
  UKNOWN: 0,
  FORBIDDEN: 403,
  ACCEPTED: 202,
  RESET_CONTENT: 205,
  OK: 200,
  InternalServerError: 500,
  NOT_FOUND: 404,
  CONFLICT: 409,
  NO_CONTENT: 204,
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
  CREATED: 201,
  FAIL_DEPENDENCY: 424,
};

export const API_ROUTES = {
  UPLOAD_FILE: "upload_file?action=create",
  LOGIN: "login",
  ALL_METERS: "get_meters",
  EDGEDEV_DATA: "get_data_by_meter?siccode=",
  LABELS: 'upload_labels_file',
  FEAT_TECH:'features_and_tech', 
  HISTORY_MODEL: 'hist_by_technique',
  PERFORMANCE: 'performance_by_model',
  USER:{
    RECOVERY:'recovery',
    RESET:'reset',
    SIGNUP: 'register_user'
  }
};

export const ENVIRONMENT={
  IP_BASE: 'http://localhost:8181/'
}
"
new="export const ENVIRONMENT={
  IP_BASE: 'http://localhost:8181/'
}
EOF
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
echo ${fe} > pme-gita-ui/src/presentation/constants/api.constants.ts
cd pme-gita-ui
npm install serve -g
npm install
npm run build

cp build/ ../../pme-gita-backend/
serve -s build
}

#================================= MAIN THREAD===================================
#================================================================================
while [ "$1" != "" ]; do
    case $1 in
        
        -i|--install)
            install_project
            configure_db
            exit
            ;;
        -ui|--gui)
            install_gui
            exit
            ;;
        -h | --help) 
            echo "$USAGE"
            exit
            ;;
                        
        *)  echo "$USAGE"
            exit 1
            ;;
    esac
    shift
done

#export PIPENV_VENV_IN_PROJECT="enabled"
# 8KbBWWD3jLKF48AvLdbG pass bitbuket