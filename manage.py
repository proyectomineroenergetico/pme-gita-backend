"""
manage.py

Este archivo define los comandos para inicializar, migrar o 
actualizar la estructura de la base de datos según los modelos 
definidos

How to run:
manage.py db init
manage.py db migrate
manage.py db upgrade
"""
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from run import app
from shared import db

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()  #To create the migrations folder -python manage.py db init
                   #To create the tables into database -python manage.py db migrate
                   #To apply the migrations to the database (create the tables)-python manage.py db upgrade 
