"""
app.py 

Este archivo crea el objeto app e inicializa este 
con los parámotros definidos en el environment
"""
from pme_app.external_interfaces.flask_server.app import create_app
from shared import CustomConfig, bcrypt, db, env, mail

if env == "production":
    app = create_app('pme_app.external_interfaces.flask_server.settings.ProdConfig', env)
    app.config.from_object(CustomConfig)
elif env == "testing":
    pass
else:
    app = create_app()
    app.config.from_object(CustomConfig)

bcrypt.init_app(app)
db.init_app(app)
mail.init_app(app)
