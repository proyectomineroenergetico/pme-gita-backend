"""
Shared.py

Este archivo contiene todos los elementos compartidos que 
serán usados por otros objetos o servicios

"""
import logging
import os

import redis
from dotenv import load_dotenv
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_sse import sse
from rq import Queue

db = SQLAlchemy()
bcrypt = Bcrypt()
load_dotenv()
mail = Mail()
secret_key = os.getenv("SECRET_KEY")
rounds = os.getenv("ROUNDS")
upload_folder = os.path.dirname(os.path.realpath(__file__)) + os.getenv("UPLOAD_FOLDER")
data_files = os.path.dirname(os.path.realpath(__file__)) + "/data_files"
app_port = os.getenv("HTTP_PORT")
env = os.getenv("PYTHON_ENV")
db_user = os.getenv("DATABASE_USER")
db_pass = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE")
host = os.getenv("HOST")
db_url = f"postgresql://{db_user}:{db_pass}@localhost/{db_name}"
email_user = os.getenv('EMAIL_USER')
email_password = os.getenv('EMAIL_PASSWORD')
build = os.path.dirname(os.path.realpath(__file__)) + "/build"


SSE = sse
CORS(SSE)
r = redis.Redis()
q = Queue(connection=r)

#Logger config
logs_directory = os.path.dirname(os.path.realpath(__file__)) + os.getenv('LOG_FILES_DIRECTORY')
logging.basicConfig(filename=logs_directory+'/pme.log', level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger("pmeapp")

class CustomConfig():
    """Configuración personalisada para el environment"""
    BCRYPT_LOG_ROUNDS = rounds
    SECRET_KEY = secret_key
    SQLALCHEMY_DATABASE_URI = db_url
    REDIS_URL = "redis://localhost"
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = email_user
    MAIL_PASSWORD = email_password
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_DEFAULT_SENDER = email_user
