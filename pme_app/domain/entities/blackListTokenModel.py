import datetime

from shared import db


class BlacklistToken(db.Model):
    """
    Modelo que define la estructura de los tokens almacenados en la lista negra.

    :param token: Token del usuario 
    :type token: str
    """
    __tablename__ = 'blacklist_tokens'
    __table_args__ = {'extend_existing': True} 


    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    blacklisted_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.datetime.now()

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        """
        Verifica si el token está en la lista negra.
        
        :param auth_token: Token de usuario
        :type auth_token: str
        :returns: Booleano para verificar si el token está en la lista negra o no
        :rtype: bool
        """
        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False
