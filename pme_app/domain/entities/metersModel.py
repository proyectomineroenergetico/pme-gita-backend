from shared import db
from sqlalchemy.dialects.postgresql import JSONB


class metersByUserModel(db.Model):
    """
     Modelo que define la estructura de los parámetros por 
     medidor que seŕan almacenados en la base de datos.

     :param sicCode: Nombre del medidor
     :type sicCode: str
     :param userId: Id del usuario actual
     :type userId: int
     :param dataByMeter: La información del medidor
     :type dataByMeter: json
    """
    __tablename__ = 'metersByUser'
    __table_args__ = {'extend_existing': True} 

    sicCode = db.Column(db.String(20), primary_key=True,unique=True)
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    dataByMeter = db.Column(JSONB)

    rel = db.relationship('UserModel')

    def __init__(self, sicCode, userId, dataByMeter):
        self.sicCode = sicCode
        self.userId = userId
        self.dataByMeter = dataByMeter
    