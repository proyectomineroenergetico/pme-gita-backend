from shared import db
from sqlalchemy.dialects.postgresql import JSONB


class historyByTechnique(db.Model):
    """
    Modelo que define las historil de ejecución de cada técnica y que serán guardadas en base de datos.
    
    :param name: Nombre del modelo
    :type name: str
    :param userId: Id del usuario actual
    :type userId: int
    :param date: Fecha de ejecución
    :type date: datetime
    :param history: Historial
    :type history: json
    """
    __tablename__ = 'history'
    __table_args__ = {'extend_existing': True} 

    model_name = db.Column(db.String(60), primary_key=True,unique=True)
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    history = db.Column(JSONB)
    date = db.Column(db.DateTime)

    rel = db.relationship('UserModel')

    def __init__(self, name, userId, date,msg):
        self.model_name = name
        self.userId = userId
        self.date = date
        self.history = msg
