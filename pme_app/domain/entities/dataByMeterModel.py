class dataByMeter():
    """
    Modelo que define la estructura de los datos para cada medidor.
    
    :param date: Fecha de las capturas de datos
    :type date: datetime
    :param activePower: Potencia activa para las respectivas fechas 
    :type activePower: float
    :param reactivePower: Potencia reactiva para las respectivas fechas 
    :type reactivePower: float
    :param penalty: Penalización para las respectivas feca 
    :type penalty: float
    """

    def __init__(self, date, activePower, reactivePower, penalty):
        self.date = date
        self.activePower = activePower
        self.reactivePower = reactivePower
        self.penalty = penalty
