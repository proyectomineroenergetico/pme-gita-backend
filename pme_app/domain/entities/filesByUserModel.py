import json

from shared import db
from sqlalchemy.dialects.postgresql import JSONB


class filesByUserModel(db.Model):
    """
    Modelo que define los la estructura donde serán almacenados los
    paths de los modelos de ML de acuerdo a la técnica seleccionada.

    :param userId: Id del usuario actual
    :type userId: int
    """
    __tablename__ = 'filesByUser'
    __table_args__ = {'extend_existing': True} 

    userId = db.Column(db.Integer, db.ForeignKey('users.id'), unique=True, primary_key=True)
    data_path = db.Column(db.String(255), nullable=True)
    features_path = db.Column(db.String(255), nullable=True)
    kmeans_path = db.Column(JSONB, nullable=True)
    lda_path = db.Column(JSONB, nullable=True)
    svm_path = db.Column(JSONB, nullable=True)
    rf_path = db.Column(JSONB, nullable=True)
    nn_path = db.Column(JSONB, nullable=True)
    labels_path = db.Column(db.String(255), nullable=True)
    features_path2 = db.Column(db.String(255), nullable=True)


    rel = db.relationship('UserModel')

    def __init__(self, userId):

        self.userId = userId
        self.data_path = ""
        self.features_path = ""
        self.kmeans_path = json.dumps({'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' })
        self.svm_path = json.dumps({'label_planta':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' },
                            'label_planta2':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' },
                            'label_planta3':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }, 
                            'label_tension':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' },
                            'label_tension2':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }})
        self.nn_path = json.dumps({'label_planta':'', 'label_planta2':'',
                            'label_planta3':'', 'label_tension':'','label_tension2':''})
        self.rf_path = json.dumps({'label_planta':'', 'label_planta2':'',
                            'label_planta3':'', 'label_tension':'','label_tension2':''})
        self.lda_path = json.dumps({'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' })
        self.labels_path = ""
        self.features_path2 = ""
    