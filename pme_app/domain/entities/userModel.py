import datetime

import jwt
from pme_app.domain.abstract_entities.chech_forms import utils_to_check_forms
from pme_app.domain.entities.blackListTokenModel import BlacklistToken
from shared import bcrypt, db, rounds, secret_key


class UserModel(db.Model, utils_to_check_forms):
    """
    Modelo de usuario utilizado para almacenar los detalles de el usuario.
    
    :param email: E-mail de usuario
    :type email: str
    :param password: Password del usuario
    :type password: str
    :param admin: Define si el usuario es administrador o no
    :type admin: bool
    """
    __tablename__ = "users"
    __table_args__ = {'extend_existing': True} 

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    #agregar nombre completo

    def __init__(self, email, password, admin=False):
        self.email = email
        self.password = bcrypt.generate_password_hash(
            password,int(rounds)).decode()
        self.registered_on = datetime.datetime.now()
        self.admin = admin

        utils_to_check_forms.__init__(self)

    def encode_auth_token(self, user_id, admin, email):
        """
        Genera el token de autenticación.

        :param user_id: El id del usuario actual
        :type user_id: int
        :param admin: define si el usuario es administrador o no
        :type admin: bool
        :param email: e-mail del usuario
        :type email: str
        :returns: Token de autenticacción del usuario
        :rtype: str
        """
        try:
            payload = {
                #'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=60,seconds=0),
                'iat': datetime.datetime.utcnow(),
                'usr-id': user_id,
                'adm': admin,
                'e-mail': email
            }
            return jwt.encode(
                payload,
                secret_key,
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Valida el token del usuario.

        :param auth_token: Token de autenticación
        :type auth_token: str
        :return: Payload con el id del usuario
        :rtype: int|str
        """
        try:
            payload = jwt.decode(auth_token,secret_key)
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['usr-id']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

