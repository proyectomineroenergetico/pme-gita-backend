from shared import db
from sqlalchemy.dialects.postgresql import JSONB


class performanceByTechnique(db.Model):
    """
    Modelo que define las métricas de desempeño que serán guardadas en base de datos.
    
    :param name: Nombre del modelo
    :type name: str
    :param userId: Id del usuario actual
    :type userId: int
    :param dataByModel: Métricas de desempeño
    :type dataByModel: json
    """
    __tablename__ = 'performance'
    __table_args__ = {'extend_existing': True} 

    model_name = db.Column(db.String(60), primary_key=True,unique=True)
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    dataByModel = db.Column(JSONB)

    rel = db.relationship('UserModel')

    def __init__(self, name, userId, dataByModel):
        self.model_name = name
        self.userId = userId
        self.dataByModel = dataByModel
