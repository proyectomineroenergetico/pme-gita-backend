import re


class utils_to_check_forms():
    """
    Verifica el formato de e-mail o de los requerimientos del password.

    """

    def __init__(self):
        self.info = 'Validation tools'

    @staticmethod
    def check_password_requirements(passwd):
        """ 
        Chequea los requerimientos de password.

        :param passdw: password
        :type passwd: str
        :returns: Un booleano para verificar que el password cumple los requerimientos
        :rtype: bool
        """
        if re.fullmatch(r'.*[A-Z]{1,}.*[a-z]{1,}.*[0-9]{1,}.*|.*[0-9]{1,}.*[a-z]{1,}.*[A-Z]{1,}.*|.*[A-Z]{1,}.*[0-9]{1,}.*[a-z]{1,}.*|.*[a-z]{1,}.*[0-9]{1,}.*[A-Z]{1,}.*|.*[a-z]{1,}.*[A-Z]{1,}.*[0-9]{1,}.*|.*[0-9]{1,}.*[A-Z]{1,}.*[a-z]{1,}.*',passwd) and len(passwd)>6:
            #match
            return True
        else:
            #no match
            return False
            
    @staticmethod
    def check_email_format(email):
        """ 
        Verifica el formato del e-mail

        :param email: e-mail
        :type email: str
        :returns: Un booleano para verificar que el formato está correcto
        :rtype: bool
        """
        if re.match(r'[^@]+@[^@]+\.[^@]+',email):
            #match
            return True
        else:
            #no match
            return False
