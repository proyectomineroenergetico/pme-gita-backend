import flask


class metersListEncoder(flask.json.JSONEncoder):
    """
    Codifica la lista de medidores y retorna un diccionario 
    con la lista y el conteo de elementos de esa lista.
    """

    def encode(self, o):  # pylint: disable=E0202
        """
        Serializa el la lista y agrega el numero de elementos.
        
        :param o: Objeto a codificar
        :type o: list
        :returns: un json con los datos codificados
        :rtype: dict
        """
        try:
            to_serialize = {
                "siccodes": o,
                "count": len(o),
            }
            return to_serialize
        except AttributeError:
            return super().default(o)
