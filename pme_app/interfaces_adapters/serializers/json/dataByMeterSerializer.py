import flask


class dataByMeterEncoder(flask.json.JSONEncoder):
    """
    Codifica los datos de cada medidor y returna un JSON.
    """

    def encode(self, o):  # pylint: disable=E0202
        """
        Serializa el dataframe 'o'.

        :param o: Objeto a codificar
        :type o: Dataframe
        :returns: un json con los datos codificados
        :rtype: json
        """
        try:
            to_serialize = o.to_json()
            return to_serialize

        except AttributeError:
            return super().default(o)
