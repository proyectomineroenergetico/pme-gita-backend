from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.getDataByMeterService import getDataByMeter

api_get_data_by_meter= Blueprint('get_data_by_meter', __name__)
CORS(api_get_data_by_meter)
@api_get_data_by_meter.route('/api/get_data_by_meter',methods=['GET'])
def getDataByMeterS():
    """
    Servicio que retorna los datos por medidor seleccionado de acuerdo al usuario actual.

    method: {GET}

    url: hots:port/api/get_data_by_meter
    """
    if request.method == 'GET':
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        if 'siccode' not in request.args:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad siccodes no existe en el request'
            }
            return make_response(jsonify(responseObject), 400)
        siccode = request.args.get('siccode')
        data = getDataByMeter(siccode,user_id)
        responseObject = {
            'status': 'success',
            'message': f'Obteniendo la data para el medidor {siccode}.',
            'data': data
        }
        return make_response(jsonify(responseObject), 200)
    responseObject = {
        'status': 'fail',
        'message': 'Bad request method'
    }
    return make_response(jsonify(responseObject), 400)
