from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.filesByuser import getFilesByUser
from pme_app.repository.db_services.getMetersService import getMeterList
from pme_app.use_cases.utils.a_sync_methods import (frecuency_methods,
                                                    time_method)
from pme_app.use_cases.utils.run_tech import (nn_tech_fit_predict,
                                              nn_tech_predict)
from shared import logger, q

api_featuresAndTech = Blueprint('features_and_tech', __name__)
CORS(api_featuresAndTech)
@api_featuresAndTech.route('/api/features_and_tech',methods=['POST'])
def featuresAndTech():
    """
    Servicio que habilita a los usuario a crear un modelo personalizado
    seleccionando el tipo de características, la técnica y la clase de problema
    según las etiquetas cargadas en la plataforma.

    method: {POST}

    url: hots:port/api/features_and_tech
    """
    if request.method == 'POST':
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        params = request.get_json()

        if 'selected_feature' not in params:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad selected_feature no existe en el body.'
            }
            return make_response(jsonify(responseObject), 400)
        sf = params['selected_feature']

        if 'selected_technique' not in params:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad selected_technique no existe en el body.'
            }
            return make_response(jsonify(responseObject), 400)
        technique = params['selected_technique']

        if 'class_problem' not in params:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad class_problem no existe en el body.'
            }
            return make_response(jsonify(responseObject), 400)
        cp = params['class_problem']

        if 'selected_bands' not in params:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad selected_bands no existe en el body.'
            }
            return make_response(jsonify(responseObject), 400)
        selectedBands = params['selected_bands']

        if 'do' not in params:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad do no existe en el body.'
            }
            return make_response(jsonify(responseObject), 400)
        do = params['do']

        if 'recalc_features' not in params:
            responseObject = {
                'status': 'fail',
                'message': 'Propiedad recalc_features no existe en el body.'
            }
            return make_response(jsonify(responseObject), 400)
        rerc_feat = params['recalc_features']
        meters2 = getMeterList(user_id)
        meters = []
        for m in meters2:
            m =m.replace(f'_{user_id}','')
            meters.append(m)

        logger.debug(params)

        
        if sf ==  "FRECUENCY":
            logger.info("frecuency")
            featurespath = getFilesByUser(user_id=user_id,features_path=True)
            if rerc_feat == True or featurespath[1] == '':
                logger.info('Calc new features')
                args = (True, featurespath[1], user_id, list(selectedBands), meters, cp, do, technique)
                job = q.enqueue(frecuency_methods, args=args, job_timeout=86400)
            else:
                logger.info("Using existing features")
                args = (False, featurespath[1], user_id, list(selectedBands), meters, cp, do, technique)
                job = q.enqueue(frecuency_methods, args=args, job_timeout=86400)
            responseObject = {
                    'status': 'Pending',
                    'message': f'La extracción de características {sf} y {do} para el método {technique} están corriendo en background'
                }
            logger.info(f"Task ({job.id}) added to queue at {job.enqueued_at}")
            return make_response(jsonify(responseObject), 204)
        elif sf == "TIME":
            if technique == "NEURAL-NETWORK":
                logger.info('nn')
                dataPath = getFilesByUser(user_id=user_id,data_path=True)
                if dataPath[0] == 400:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un problema ocurrió obteniendo el path para las características temporales, por favor suba de nuevo el archivo. {dataPath[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)
                label_path = getFilesByUser(user_id=user_id,labels_path=True)
                if label_path[0] == 400:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un problema ocurrió obteniendo el path para las etiquetas, por favor suba de nuevo el archivo. {label_path[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)
                if do == 'fit_and_predict' or rerc_feat == True:
                    responseObject = {
                        'status': 'Pending',
                        'message': f'La extracción de características {sf} y {do} para el método {technique} están corriendo en background'
                    }
                    args = (dataPath[1], label_path[1],meters,user_id,cp)
                    job = q.enqueue(nn_tech_fit_predict, args=args, job_timeout=86400)
                    logger.info(f"Task ({job.id}) added to queue at {job.enqueued_at}")
                    return make_response(jsonify(responseObject), 204)
                elif do == 'predict':
                    logger.info('predict')
                    modelPath = getFilesByUser(user_id=user_id,nn_path=True,cp=cp)
                    if modelPath[0] == 400:
                        responseObject = {
                            'status': 'fail',
                            'message': f'Un problema ocurrió obteniendo el path del modelo, por favor corra de nuevo entrenar y clasificar. {modelPath[1]}'
                        }
                        return make_response(jsonify(responseObject), 400)
                    responseObject = {
                        'status': 'Pending',
                        'message': f'La extracción de características {sf} y {do} para el modelo {technique} están corriendo en background'
                    }
                    args = (dataPath[1], modelPath[1], meters, cp, user_id)
                    job = q.enqueue(nn_tech_predict, args=args, job_timeout=86400)
                    logger.info(f"Task ({job.id}) added to queue at {job.enqueued_at}")
                    return make_response(jsonify(responseObject), 204)
            elif technique == 'RANDOM-FOREST':
                label_path = getFilesByUser(user_id=user_id,labels_path=True)
                dataPath = getFilesByUser(user_id=user_id,data_path=True)
                if dataPath[0] == 400:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un problema ocurrió obteniendo el path para las características temporales, por favor suba de nuevo el archivo. {dataPath[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)
                if label_path[0] == 400:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un problema ocurrió obteniendo el path para las etiquetas, por favor suba de nuevo el archivo. {label_path[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)
                featurespath = getFilesByUser(user_id=user_id,features_path2=True)
                if rerc_feat or featurespath[1] == '':
                    logger.info('Calc new features')
                    responseObject = {
                        'status': 'Pending',
                        'message': f'The feature extraction {sf} and {do} for method {technique} are running in background'
                    }
                    args = (True,featurespath[1],user_id,dataPath[1],label_path[1],cp,technique,do)
                    job = q.enqueue(time_method, args=args, job_timeout=86400)
                    logger.info(f"Task ({job.id}) added to queue at {job.enqueued_at}")
                    return make_response(jsonify(responseObject), 204)
                else:
                    logger.info('Using existing features')
                    logger.info(f'{dataPath[1]},{label_path[1]}')
                    args = (False,featurespath[1],user_id,dataPath[1],label_path[1],cp,technique,do)
                    job = q.enqueue(time_method, args=args, job_timeout=86400)
                responseObject = {
                        'status': 'Pending',
                        'message': f'La extracción de características {sf} y {do} para el modelo {technique} están corriendo en background'
                    }  
                logger.info(f"Task ({job.id}) added to queue at {job.enqueued_at}")
                return make_response(jsonify(responseObject), 204)
            else:
                responseObject = {'message':f'La técnica de ML seleccionada no fue reconocida. esta debe de ser Neural-NETWORK o RANDOM-FOREST en vez de  {technique}.',
                                    'status': 'fail'
                    }   
                return make_response(jsonify(responseObject), 400)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Extracción de características no reconocida. Esta debe de ser FRECUENCY o TIME.'
            }
            return make_response(jsonify(responseObject), 400)
