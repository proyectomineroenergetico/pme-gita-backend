from flask import Blueprint, render_template
from flask_cors import CORS
from shared import SSE

api_home = Blueprint('api', __name__)
CORS(api_home)
@api_home.route('/api',methods=['GET'])
def api():
    """
    Servicio root api para testing.

    method: {GET}

    url: host:port/api    
    """
    SSE.publish({"message": "Hello!"}, type='running.method')
    return "Hello World"
