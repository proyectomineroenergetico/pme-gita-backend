from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.repository.db_services.registerUserService import registerUserdb

api_register_user= Blueprint('register_user', __name__)
CORS(api_register_user)
@api_register_user.route('/api/register_user',methods=['POST'])
def registerUser():
    """
    Servicio que permite a los usuarios realizar un registro en la plataforma.
    
    method: {POST}

    url: host:port/api/register_user
    """
    if request.method == 'POST':
        post_data = request.get_json()
        code = registerUserdb(post_data)
        if code[0] == 201:
            responseObject = {
                'status': 'success',
                'message': 'Registro satisfactorio.',
                'auth_token': code[1].decode()
            }
            return make_response(jsonify(responseObject), code[0])
        elif code[0] == 202:
            responseObject = {
                'status': 'fail',
                'message': 'El usuario ya existe, por favor logeese.',
            }
            return make_response(jsonify(responseObject), code[0])
        elif code[0] == 412:
            responseObject = {
                'status': 'fail',
                'message': 'Verifique lor requerimientos de password o la estructura del email.',
            }
            return make_response(jsonify(responseObject), code[0])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Ocurrió un error, por favor intente de nuevo.'
            }
            return make_response(jsonify(responseObject), code[0])          
