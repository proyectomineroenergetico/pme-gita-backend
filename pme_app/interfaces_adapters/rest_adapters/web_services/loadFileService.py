import os

import pandas as pd
from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.deleteMetersByUserService import \
    deleteMetersByUser
from pme_app.repository.db_services.filesByuser import saveFilesByUser
from pme_app.repository.db_services.getMetersService import getMeterList
from pme_app.repository.db_services.meterTableServices import createMeterTable
from pme_app.use_cases.manageIncomingFiles import manageInputFile
from pme_app.use_cases.read_preprocess import ReadPreprocess
from shared import data_files, logger, upload_folder
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = {'csv', 'xlsx'}


def allowed_file(filename):
    # """
    # Verifica si la extensión del archivo es valido.

    # :param filename: Nombre del archivo
    # :type filename: str
    # :returns: Booleano indicando si el archivo es valido o no
    # :rtype: bool
    # """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

api_upload_file = Blueprint('upload_file', __name__)
CORS(api_upload_file)
@api_upload_file.route('/api/upload_file',methods=['POST','PUT'])
def uploadFile():
    """
    Servicio que recibe el archivo que contiene los datos de los
    medidores que serán usados en la generación de los modelos o en las predicciones.

    method: {POST}

    url: host:port/api/upload_file
    """
    if request.method == 'POST' or request.method == 'PUT':
        # get the auth token
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        
        # check if the post request has the file part
        if 'file' not in request.files:
            responseObject = {
                'status': 'fail',
                'message': 'propiedad file no existe en el request.'
            }
            return make_response(jsonify(responseObject), 400)
        file = request.files['file']
        if 'action' not in request.args:
            responseObject = {
                'status': 'fail',
                'message': 'propiedad action no existe en el request.'
            }
            return make_response(jsonify(responseObject), 400)
        action = request.args.get('action')
        if file.filename == '':
            responseObject = {
                'status': 'fail',
                'message': 'Nombre del archivo vacío'
            }
            return make_response(jsonify(responseObject), 400)

        if 'delete_meters' in request.args:
            delete_m = request.args.get('delete_meters')
            if delete_m == 'True':
                code = deleteMetersByUser(user_id)
                if code[0] == 200:
                    logger.info('Los medidores han sido eliminados correctamente.')
                else:
                    logger.info(f'No fue posible eliminar los medidores -> {code[1]}')
                    responseObject = {
                        'status': 'fail',
                        'message': f'No fue posible eliminar los medidores -> {code[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(upload_folder, filename))
            if filename.endswith('.csv'):
                dffile = pd.read_csv(os.path.join(upload_folder, filename))
            elif filename.endswith('.xlsx'):
                dffile = pd.read_excel(os.path.join(upload_folder, filename))
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Formato de archivo no reconocido, debe ser .csv o .xlsx'
                }
                return make_response(jsonify(responseObject), 400)
            mgmIF = manageInputFile()
            n_file = mgmIF.formattingFile(dffile)
            rpp = ReadPreprocess()
            n_file2 = rpp.run_process(dffile,data_files,user_id)
            if n_file[0] == 200 and n_file2[0] == 200:
                if action == 'create':
                    code = createMeterTable(n_file[1],user_id)
                    code2 = saveFilesByUser(data_path=n_file2[1],user_id=user_id)
                    if code[0] == 200 and code2[0] == 200:
                        os.remove(os.path.join(upload_folder, filename))
                        logger.info(f"File {filename} has been removed")
                        meters = getMeterList(user_id)
                        responseObject = {
                            'status': 'success',
                            'message': 'El archivo ha sido guardado correctamente.',
                            'siccodes': meters
                        }
                        return make_response(jsonify(responseObject), 200)
                    else:
                        if code[0] == 400:
                            logger.info("some error occurred in createMeterTable")
                            responseObject = {
                                'status': 'fail',
                                'message': f'Un error ocurriÓ guardando el archivo-> {code[1]}.'
                            }
                            return make_response(jsonify(responseObject), 400)
                        elif code2[0] == 400:
                            logger.info('some error occurred in saveFilesByUser')
                            responseObject = {
                                'status': 'fail',
                                'message': f'Un error ocurriÓ guardando el archivo ->{code2[1]}.',
                            }
                            return make_response(jsonify(responseObject), 400)
                elif action ==  'replace':
                    pass
                else:
                    pass
            elif n_file[0] == 400:
                responseObject = {
                    'status': 'fail',
                    'message': 'Un error ocurriÓ parseando el archivo, '+n_file[1]
                }
                return make_response(jsonify(responseObject), 400)
            elif n_file2[0] == 400:
                responseObject = {
                    'status': 'fail',
                    'message': 'Un error ocurriÓ parseando el archivo, '+n_file2[1]
                }
                return make_response(jsonify(responseObject), 400)
