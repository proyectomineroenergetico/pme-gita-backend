from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.repository.db_services.checkLogoutService import checkLogout

api_logout= Blueprint('logout', __name__)
CORS(api_logout)
@api_logout.route('/api/logout',methods=['POST'])
def logoutUser():
    """
    Servicio que permite a los usuarios realizar un correcto logout de la plataforma.

    method: {POST}

    url: host:port/api/logout
    """
    # get auth token
    auth_token = request.headers.get('Authorization')
    if auth_token:
        code = checkLogout(auth_token)
        if code == [200, 'success']:
            responseObject = {
            'status': 'success',
            'message': 'Logout correcto.'
            }
            return make_response(jsonify(responseObject), 200)
        elif code == [200,'fail']:
            responseObject = {
                'status': 'fail',
                'message': code[2]
            }
            return make_response(jsonify(responseObject), 200)
        else:
            responseObject = {
            'status': 'fail',
            'message': code[1]
            }
            return make_response(jsonify(responseObject), 401)   
    else:
        responseObject = {
            'status': 'fail',
            'message': 'Token invalido.'
        }
        return make_response(jsonify(responseObject)), 403
