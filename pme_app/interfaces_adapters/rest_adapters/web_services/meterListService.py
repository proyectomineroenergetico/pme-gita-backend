from flask import Blueprint, json, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.interfaces_adapters.serializers.json.metersSerializer import \
    metersListEncoder
from pme_app.repository.db_services.getMetersService import getMeterList

api_get_meters= Blueprint('get_meters', __name__)
CORS(api_get_meters)
@api_get_meters.route('/api/get_meters',methods=['GET'])
def getMeters():
    """
    Servicio que retorna la lista de medidores almacenados en la plataforma para el usuario actual.
    
    method: {GET}

    url: host:port/api/get_meters
    """
    if request.method == 'GET':
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Es requerido el login.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        meters = getMeterList(user_id)
        meters_json = json.dumps(meters, cls=metersListEncoder)
        responseObject = {
                'status': 'success',
                'message': 'Datos por medidor obtenidos.',
                "data": meters_json
            }
        return make_response(jsonify(responseObject),200)
    responseObject = {
        'status': 'fail',
        'message': 'Request mal formado.'
    }
    return make_response(jsonify(responseObject), 401)
