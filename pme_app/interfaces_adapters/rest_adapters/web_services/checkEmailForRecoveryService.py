from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.repository.db_services.getUserInfo import checkUserEmail
from pme_app.use_cases.utils.sendEmail import sendEmail
from shared import logger

api_recovery= Blueprint('recovery', __name__)
CORS(api_recovery)
@api_recovery.route('/api/recovery',methods=['POST'])
def recoveryPassword():
    """
    Servicio que permite a los usuarios recuperar la contraseña.
    
    method: {POST}

    url: host:port/api/recovery
    """
    if request.method == 'POST':
        
        post_data = request.get_json()
        logger.info(post_data)
        if 'email' in post_data:
            email = post_data['email']
            code = checkUserEmail(email)
            logger.info(f'email {code}')
            if code[0] == 200:
                code = sendEmail(email)
                logger.info(f"Sending an email to {email} to recover the password {code}")
                responseObject = {
                    'status': 'success',
                    'message': f'Un correo será enviado a {email} para recuperar la contraseña',
                }
                return make_response(jsonify(responseObject), code)
            elif code[0] == 401:
                logger.info('email malformed')
                responseObject = {
                    'status': 'fail',
                    'message': 'El email dado no tiene el formato correcto, por favor verifique el formato del email',
                }
                return make_response(jsonify(responseObject), code[0])
            else:
                logger.info('email does not exist')
                responseObject = {
                    'status': 'fail',
                    'message': f'{code[1]}',
                }
                return make_response(jsonify(responseObject), code[0])
        else:
            logger.info('bad body')
            responseObject = {
                'status': 'fail',
                'message': 'Body mal formado, por favor verifique la estructura del body',
            }
            return make_response(jsonify(responseObject), 400)
