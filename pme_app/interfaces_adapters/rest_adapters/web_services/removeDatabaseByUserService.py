from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.deleteMetersByUserService import \
    deleteMetersByUser
from shared import logger

api_delete_metters = Blueprint('delete_meters', __name__)
CORS(api_delete_metters)
@api_delete_metters.route('/api/delete_meters',methods=['POST'])
def delmeters():
    """
    Servicio que elimina los medidores almacenados en la base de datos.

    method: {POST}

    url: hots:port/api/delete_meters
    """
    if request.method == 'POST':
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        logger.info('clalling delete meters function')
        code = deleteMetersByUser(user_id)

        if code[0] == 200:
            logger.info('Los medidores han sido eliminados correctamente.')
            responseObject = {
                'status': 'success',
                'message': 'Los medidores han sido eliminados correctamente.'
            }
            return make_response(jsonify(responseObject), 200)
        else:
            logger.info(f'No fue posible eliminar los medidores -> {code[1]}')
            responseObject = {
                'status': 'fail',
                'message': f'No fue posible eliminar los medidores -> {code[1]}'
            }
            return make_response(jsonify(responseObject), 400)
