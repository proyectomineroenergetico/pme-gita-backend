from app import app
from flask import Blueprint
from flask_cors import CORS

index_r = Blueprint('/', __name__)
CORS(index_r)
@index_r.route('/')
def index():
    """
    Index

    url: host:port/    
    """
    return app.send_static_file("index.html")
