from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.performaceByModelService import \
    getPerformanceByData
from shared import logger

api_perByModel = Blueprint('performance_by_model', __name__)
CORS(api_perByModel)
@api_perByModel.route('/api/performance_by_model',methods=['GET'])
def perByModel():
    """
    Servicio que retorna las métricas de desempeño que fueron calculadas
    cuando se entreno un modelo específico.

    method: {GET}

    url: hots:port/api/performance_by_model
    """
    if request.method == 'GET':
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        if request.args.get('all') == 'True':
            code = getPerformanceByData(all=True,user_id=user_id,technique='',cp='')
                
            if code[0]==400:
                responseObject = {
                    'status': 'fail',
                    'message': f'Un error ocurrió extrayendo el historial {code[1]}'
                }
                return make_response(jsonify(responseObject), 400)
            logger.info(code[1])
            results = code[1]
            responseObject = {
                'status': 'success',
                'message': 'El historial fue extraido correctamente',
                'results': results
            }
        else:
            model_name = request.args.get('model_name')
            if model_name != '':
                code = getPerformanceByData(technique='',user_id=user_id,cp='',model_name=model_name)
                
                if code[0]==400:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un error ocurrió extrayendo la información del modelo {model_name} {code[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)
                
                results = code[1]
                responseObject = {
                    'status': 'success',
                    'message': f'La data para el modelo {model_name} fue extraida',
                    'results': results
                }
                return make_response(jsonify(responseObject), 200)
            else:
                technique = request.args.get('selected_technique')
                if technique == '':
                    responseObject = {
                        'status': 'fail',
                        'message': 'Propiedad selected_technique no existe en el body.'
                    }
                    return make_response(jsonify(responseObject), 400)
                
                cp = request.args.get('class_problem')
                if cp is None:
                    responseObject = {
                        'status': 'fail',
                        'message': 'Propiedad class_problem no existe en el body.'
                    }
                    return make_response(jsonify(responseObject), 400)
                elif cp == '':
                    code = getPerformanceByData(technique=technique,user_id=user_id)
                else:
                    code = getPerformanceByData(technique=technique,user_id=user_id,cp=cp)
                
                if code[0]==400:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un error ocurrió obteniendo la información para el método{technique} {code[1]}'
                    }
                    return make_response(jsonify(responseObject), 400)

                results = code[1]
                
                responseObject = {
                    'status': 'success',
                    'message': f'Los datos del método {technique} fueron extraidos',
                    'results': results
                }
                return make_response(jsonify(responseObject), 200)
