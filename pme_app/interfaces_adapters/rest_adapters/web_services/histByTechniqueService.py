from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.histByMethodService import \
    getHistByTechnique

api_histByTech = Blueprint('hist_by_technique', __name__)
CORS(api_histByTech)
@api_histByTech.route('/api/hist_by_technique',methods=['GET'])
def histByTech():
    """
    Servicio que retorna el historial de acciones cuando se ejecuta una técnica en específico.

    method: {GET}

    url: hots:port/api/hist_by_technique
    """
    if request.method == 'GET':
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        if request.args.get('all') == 'True':
            code = getHistByTechnique(modelName='',user_id=user_id,all=True)
            
            if code[0]==400:
                responseObject = {
                    'status': 'fail',
                    'message': f'Un error ocurrió extrayendo el historial de ejecución compoleto  {code[1]}'
                }
                return make_response(jsonify(responseObject), 400)
            
            results = code[1]
            
            responseObject = {
                'status': 'success',
                'message': 'El historial se extrajo correctamente',
                'results': results
            }
            return make_response(jsonify(responseObject), 200)
        else:
            model_name = request.args.get('model_name')
            if model_name == '':
                responseObject = {
                        'status': 'fail',
                        'message': 'propiedad model_name no existe dentro del body.'
                    }
                return make_response(jsonify(responseObject), 400)
            

            code = getHistByTechnique(model_name,user_id)
            
            if code[0]==400:
                responseObject = {
                    'status': 'fail',
                    'message': f'Ocurrió un error obteniendo el historial de ejecución del modelo {model_name} {code[1]}'
                }
                return make_response(jsonify(responseObject), 400)
            
            results = code[1]
            
            responseObject = {
                'status': 'success',
                'message': 'El historial se extrajo correctamente',
                'results': results
            }
            return make_response(jsonify(responseObject), 200)
