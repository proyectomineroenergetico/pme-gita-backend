from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.repository.db_services.checkLoginService import checkLogin

api_login= Blueprint('login', __name__)
CORS(api_login)
@api_login.route('/api/login',methods=['POST'])
def loginUser():
    """
    Servicio que permite realizar un correcto login en la plataforma teniendo las credenciales correspondientes.

    method: {POST}

    url: host:port/login
    """
    if request.method == 'POST':
        # get the post data
        post_data = request.get_json()
        
        code = checkLogin(post_data)
        if code[0] == 200:
            responseObject = {
                'status': 'success',
                'message': 'Login correcto.',
                'auth_token': code[1].decode()
            }
            return make_response(jsonify(responseObject), 200)
        elif code[0] == 404:
            responseObject = {
                'status': 'fail',
                'message': 'Usuario y/o contraseña incorrectos.'
            }
            return make_response(jsonify(responseObject), 404)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Pruebe de nuevo'
            }
            return make_response(jsonify(responseObject), 500)
