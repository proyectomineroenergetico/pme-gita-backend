import os

import pandas as pd
from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.filesByuser import (getFilesByUser,
                                                        saveFilesByUser)
from pme_app.use_cases.utils.tools import parseLabelFile
from shared import data_files, logger
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = {'csv', 'xlsx'}


def allowed_file(filename):
    # """
    # Verifica si la extensión del archivo es valido.

    # :param filename: Nombre del archivo 
    # :type filename: str
    # :returns: Booleano indicando si es valido o no
    # :rtype: bool
    # """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

api_upload_labels_file = Blueprint('upload_labels_file', __name__)
CORS(api_upload_labels_file)
@api_upload_labels_file.route('/api/upload_labels_file',methods=['POST','GET'])
def uploadLabelsFile():
    """
    Servicio que recibe un archivo .xml o .csv con las etiquetas que serán
    utilizadas para los entrenamiento de los modelos de ML supervisados.
    Adicionalmente pueden ser consultadas dichas etiquetas a través del método GET.

    method: {POST,GET}

    url: host:port/api/upload_labels_file
    """
    if request.method == 'POST':
        # get the auth token
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        
        # check if the post request has the file part
        if 'file' not in request.files:
            responseObject = {
                'status': 'fail',
                'message': 'Parámetro file no existe en el request.'
            }
            return make_response(jsonify(responseObject), 400)
        file = request.files['file']
        if file.filename == '':
            responseObject = {
                'status': 'fail',
                'message': 'Nombre del archivo vacío'
            }
            return make_response(jsonify(responseObject), 400)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(data_files, f'{user_id}_{filename}')
            file.save(path)
            if filename.endswith('.csv'):
                dffile = pd.read_csv(path)
            elif filename.endswith('.xlsx'):
                dffile = pd.read_excel(path)
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Formato del archivo no reconocido. Este debe de ser .csv o .xlsx'
                }
                return make_response(jsonify(responseObject), 400)

        code = saveFilesByUser(labels_path=path,user_id=user_id)
        if code[0] == 200:
            meterslabels = parseLabelFile(dffile)
            responseObject = {
                'status': 'success',
                'message': 'El archivo ha sido guardado correctamente.',
                'siccodes': meterslabels
            }
            return make_response(jsonify(responseObject), 200)
        else:
            if code[0] == 400:
                logger.info("some error occurred in saving the labels file")
                responseObject = {
                    'status': 'fail',
                    'message': 'Ocurrieron algunos problemas guardado el archivo.',
                    'error': code[1]
                }
                return make_response(jsonify(responseObject), 400)

    if request.method == 'GET':
        # get the auth token
        auth_token = request.headers.get('Authorization')
        if auth_token:
            user_id = UserModel.decode_auth_token(auth_token)
            if not isinstance(user_id,int):
                responseObject = {
                    'status': 'fail',
                    'message': 'Login es requerido.'
                }
                return make_response(jsonify(responseObject), 401)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Token invalido.'
            }
            return make_response(jsonify(responseObject), 401)
        
        code = getFilesByUser(labels_path=True,user_id=user_id)
        if code is not None:
            logger.info(code)
            if code[0] == 200:
                path = code[1]
                if path.endswith('.csv'):
                    dffile = pd.read_csv(path)
                elif path.endswith('.xlsx'):
                    dffile = pd.read_excel(path)
                else:
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un error ocurrió obteniendo las etiquetas  -> {code[1]}',
                    }
                    return make_response(jsonify(responseObject), 400)
                meterslabels = parseLabelFile(dffile)
                responseObject = {
                    'status': 'success',
                    'message': 'Etiquetas obtenidas correctamente.',
                    'siccodes': meterslabels
                }
                return make_response(jsonify(responseObject), 200)
            else:
                if code[0] == 400:
                    logger.info("Un error ocurrió obteniendo las etiquetas")
                    responseObject = {
                        'status': 'fail',
                        'message': f'Un error ocurrió obteniendo las etiquetas  -> {code[1]}',
                    }
                    return make_response(jsonify(responseObject), 400)
        else:
            logger.info("Label's file does not exist")
            responseObject = {
                'status': 'fail',
                'message': f'No existe el archivo de las etiquetas',
            }
            return make_response(jsonify(responseObject), 400)
