from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.abstract_entities.chech_forms import utils_to_check_forms
from pme_app.repository.db_services.updateUserInfoService import updatePassword
from pme_app.use_cases.utils.recoveryToken import decode_recovery_token
from shared import logger

api_reset= Blueprint('reset', __name__)
CORS(api_reset)
@api_reset.route('/api/reset',methods=['POST'])
def resetPassword():
    """
    Servicio que permite a los usuarios recuperar la contraseña.
    
    method: {POST}

    url: host:port/api/reset
    """
    if request.method == 'POST':
        
        post_data = request.get_json()
        if 'token' in post_data and 'password' in post_data:
            token = post_data['token']
            code = decode_recovery_token(token)
            passwd = post_data['password']
            if code[0] == 200:
                check = utils_to_check_forms()
                if check.check_password_requirements(passwd):
                    email = code[1]
                    code = updatePassword(email, passwd)
                    logger.info(code)
                    if code[0] == 200:
                        logger.info("Password has been updated successfully")
                        responseObject = {
                            'status': 'success',
                            'message': 'La contraseña ha sido actualizada correctamente',
                        }
                        return make_response(jsonify(responseObject), code[0])
                    else:
                        logger.info("Some errors updating the password")
                        responseObject = {
                            'status': 'fail',
                            'message': f'{code[1]}',
                        }
                        return make_response(jsonify(responseObject), code[0])
                else:
                    responseObject = {
                        'status': 'fail',
                        'message': 'La contraseña no cumple los requisítos mínimos, por favor verifique la contraseña',
                    }
                    return make_response(jsonify(responseObject), 400) 
            elif code[0] == 400:
                responseObject = {
                    'status': 'fail',
                    'message': f'Ocurrió un problema decodificando el token -> {code[1]}',
                }
                return make_response(jsonify(responseObject), code[0])  
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Por favor verifique la estructura del body',
            }
            return make_response(jsonify(responseObject), 400)  
