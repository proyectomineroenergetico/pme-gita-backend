from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.repository.db_services.getUserInfo import getUserInfo

api_user_info= Blueprint('user_info', __name__)
CORS(api_user_info)
@api_user_info.route('/api/user_info',methods=['GET'])
def userInfo():
    """
    Servicio web que retorna la información del usuario.
    
    method: {GET}

    url: host:port/api/user_info
    """
    # get the auth token
    auth_token = request.headers.get('Authorization')
    if auth_token:
        code = getUserInfo(auth_token)
        if code[0] == 200:
            user = code[1]
            responseObject = {
                'status': 'success',
                'data': {
                    'user_id': user.id,
                    'email': user.email,
                    'admin': user.admin,
                    'registered_on': user.registered_on
                }
            }
            return make_response(jsonify(responseObject), 200)
        responseObject = {
            'status': 'fail',
            'message': code[1]
        }
        return make_response(jsonify(responseObject), 401)
    else:
        responseObject = {
            'status': 'fail',
            'message': 'Token invalido.'
        }
        return make_response(jsonify(responseObject), 401)
    