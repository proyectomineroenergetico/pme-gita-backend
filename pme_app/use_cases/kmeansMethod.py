import json

from shared import logger
from six.moves import cPickle as pickle
from sklearn.cluster import KMeans
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis


class kMeansML():
    """
    Permite generar un modelo de K-Means 
    para generar clusters acorde a las caracteristicas de los datos.

    :param data: Características con las que se entrenará  el modelo
    :type data: dataframe
    :param ldaPath: Path donde será almacenada la transformación de los datos LDA
    :type ldaPath: str
    :param file: Path donde será almacenado el modelo
    :type file: str
    :param meters: Lista con los nombres de C/U de los medidores en el mismo orden de los datos
    :type meters: dataframe
    """

    def __init__(self,data,meters,ldaPath, modelPath):
        self.data = data
        self.meters = meters
        self.c_param = 3
        self.lda_path = ldaPath
        self.model_path = modelPath

    def applyLDA(self, l):
        """
        Realiza una transformación de los datos de n dimensiones a 2 dimensiones usando ldaPath.

        :param l: Etiquetas predecidas por K-Means para generar la transformación
        :type l: array
        :returns: Transformación de los datos en dos dimensiones
        :rtype: array
        """
        # define transformation
        lda = LinearDiscriminantAnalysis(n_components=2)
        # prepare transform on dataset
        lda.fit(self.data,l)
        # apply transform to dataset
        transformation = lda.transform(self.data)
        self.save_pickle(lda, self.lda_path)

        return transformation

    def save_pickle(self, data, file_save):
        """
        Guarda el modelo entrenado para que sea utilizado 
        posteriormente en la predicción de nuevos datos.

        :param data: modelo que será guardado.
        :type data: Datos a guardar
        :param file_save: Path donde será almacenado el modelo .pickle
        :type file_save: str
        """
        try:
            f = open(file_save, 'wb')
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
            f.close()
        except Exception as e:
            logger.error(f'Unable to save data to {file_save} -> {e}')

    def fit_and_predict(self):
        """
        Entrena el modelo con los parámetros seleccionados y 
        entrega las predicciones de los clusters y 
        los centroides.
        
        :returns: los medidores con las etiquetas correspondientes según el cluster junto con los centroides
        :rtype: dict,list
        """
        #perform the kmeans alg
        kmeans = KMeans(n_clusters=self.c_param).fit(self.data)
        # Predicting the clusters    
        labels = kmeans.predict(self.data)
        
        d_transformed = self.applyLDA(labels)

        f1 = [a_tuple[0] for a_tuple in d_transformed]
        f2 = [a_tuple[1] for a_tuple in d_transformed]

        #perform the kmeans alg for lda transformation
        kmeans = KMeans(n_clusters=self.c_param).fit(d_transformed)
        centers = kmeans.cluster_centers_
        #save the model
        self.save_pickle(kmeans, self.model_path)
        # Predicting the clusters
        labels_f = kmeans.predict(d_transformed)
        result = {'SicCodes':json.dumps(self.meters),'Feature1':json.dumps(f1),'Feature2':json.dumps(f2),'label':json.dumps(labels_f.tolist()),'centroids':json.dumps(centers.tolist())}
        return result, centers.tolist()

    def predict(self):
        """
        Predice las etiquetas de cluster para nuevos datos cargando un modelo 
        previamente entrenado y retorna las etiquetas para C/U de los medidores.

        :returns: Nombre de los medidores, datos en dos dimensiones para su visualización, etiquetas predichas y los centroides del cluster
        :rtype: dict, list
        """
        with open(self.model_path, 'rb') as f:
            model = pickle.load(f)
        with open(self.lda_path, 'rb') as f:
            lda = pickle.load(f)

        transf_data = lda.transform(self.data)

        f1 = [a_tuple[0] for a_tuple in transf_data]
        f2 = [a_tuple[1] for a_tuple in transf_data]
        y_pred = model.predict(transf_data)
        centers = model.cluster_centers_

        result = {'SicCodes':json.dumps(self.meters),'Feature1':json.dumps(f1),'Feature2':json.dumps(f2),'label':json.dumps(y_pred.tolist()),'centroids':json.dumps(centers.tolist())}
        return result, centers.tolist()
