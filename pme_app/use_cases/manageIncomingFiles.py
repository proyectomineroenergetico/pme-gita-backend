import numpy as np
import pandas as pd
from pme_app.use_cases.utils.tools import order_series
from shared import logger


class manageInputFile():
    """ Transforma el archivo cagado por el usuario y da el formato requerido a los datos. """

    def formattingFile(self,dffile):
        """
        Formatea los datos para su fácil manipulación.

        :param dffile: Dataframe con los datos recién cargados
        :type dffile: dataframe
        :retunrs: Lista con un código de estado y data/Error 
        :rtype: list
        """
        dataSeriesByFrt = {}
        try:
            for i in range(len(dffile['Fecha'])):  # Order dataset by date
                if isinstance(dffile['Fecha'][i], str):
                    dffile['Fecha'][i] = pd.to_datetime(dffile['Fecha'][i])
            dffile['Fecha'] = pd.to_datetime(dffile['Fecha'], format = '%y%m%d')
            dffile = dffile.sort_values('Fecha')      
            index_row = dffile.index[dffile['Act_Tot'] == 0 ].tolist() # Extract rows with Act_tot  = 0
            dffile = dffile.drop(index_row) # Delete the rows with Act_tot = 0, days without consumption
            frtRef = dffile['CodigoSIC'].unique()
        except Exception as e:
            error = f'Expected {e}. Please check the file structure'
            logger.error(error)
            return [400,error]
        for j in frtRef: # Create a dict with the data by each frontier 
            dummySeries = {}
            frtIndex = dffile.index[dffile['CodigoSIC'] == j ].tolist()
            dummySeries = pd.DataFrame(dffile.loc[frtIndex, :])
            dataSeriesByFrt[j] = order_series(dummySeries.sort_values('Fecha'))
        for k,v in dataSeriesByFrt.items():
            dataSeriesByFrt[k]['Active'] =   v['Active']/np.max(v['Active'])
            dataSeriesByFrt[k]['Reactive'] =   v['Reactive']/np.max(v['Reactive'])
            dataSeriesByFrt[k]['Penalty'] =   v['Penalty']/np.max(v['Penalty'])   
        return [200,dataSeriesByFrt] 
