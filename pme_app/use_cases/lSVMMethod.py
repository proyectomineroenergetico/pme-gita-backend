import json

import numpy as np
import pandas as pd
from pme_app.use_cases.utils.tools import k_fold_custom
from shared import logger
from six.moves import cPickle as pickle
from sklearn import metrics, svm


class linearSVM():
    """
    Permite generar un modelo de Support Vector Machine 
    para predecir una clasificación de acuerdo a la 
    clase de problema seleccionado.

    :param data: Características con las que se entrenará  el modelo
    :type data: dataframe
    :param y: etiquetas con las que el modelo será entrenado el modelo
    :type y: dataframe
    :param file: Path donde será almacenado el modelo
    :type file: str
    :param meters: Lista con los nombres de C/U de los medidores en el mismo orden de los datos
    :type meters: dataframe
    """

    def __init__(self, data, y, file, meters):
        self.data = data
        self.y = y
        self.model_file = file
        self.meters = meters
        self.labels = ""

    def save_pickle(self, data, file_save):
        """
        Guarda el modelo entrenado para que sea utilizado 
        posteriormente en la predicción de nuevos datos.

        :param data: modelo que será guardado.
        :type data: Modelo SVM
        :param file_save: Path donde será almacenado el modelo .pickle
        :type file_save: str
        """
        try:
            f = open(file_save, 'wb')
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
            f.close()
        except Exception as e:
            logger.error(f'Unable to save data to {file_save} -> {e}')

    def remove_bad_labels(self):
        """
        Remueve los medidores con etiquetas pero que no están en la base 
        de datos o que no serán utilizadas para el entrenamiento. 
        Esto depende de la clase de problema elegido.
        """
        dat = pd.DataFrame(self.data)
        rm_index = self.y.index[self.y['label'] == 0].tolist()
        l = self.y
        m = self.meters
        for r in rm_index:
            frt = l['frontera'][r]
            dat_index = m.index[m == frt]
            m = m.drop(dat_index)
            dat = dat.drop(dat_index)
            l = l.drop(r)
        l = l.set_index('frontera')
        l = l.reindex(index=m)
        l = l.reset_index()
        m = m.reset_index(drop=True)
        l = l.rename(columns={'Meter': 'frontera'})
        self.y = l
        self.meters = m
        self.data = np.array(dat)

    def fit_and_predict(self):
        """
        Entrena el modelo con los parámetros seleccionados y 
        entrega las predicciones de los datos de test y 
        las métricas de desempeño del modelo.
        
        :returns: Las métricas de desempeño y las predicciones de test junto con las etiquetas correctas
        :rtype: dict
        """
        prec_value = []
        recall_value = []
        acc_value = []
        metrics_f = {}
        best_acc = 0
        self.remove_bad_labels()
        self.labels = self.y['label']
        for train_index, test_index in k_fold_custom(self.data,self.labels,10,0.2):
            X_train, X_test = self.data[train_index], self.data[test_index] 
            y_train, y_test = self.labels[train_index], self.labels[test_index]
            meters_train,meters_test = self.meters[train_index], self.meters[test_index]
            #Generating the svm model 
            #Create a svm Classifier
            clf = svm.SVC(kernel='linear') # Linear Kernel
            #Train the model using the training sets
            clf.fit(X_train, y_train)
            #Predict the response for test dataset
            y_pred = clf.predict(X_test)
            # Evaluating the Model
            acc_value.append(metrics.accuracy_score(np.array(y_test,dtype=int), y_pred))
            prec_value.append(metrics.precision_score(np.array(y_test,dtype=int), y_pred,average='micro'))
            recall_value.append(metrics.recall_score(np.array(y_test,dtype=int), y_pred,average='micro'))
            if metrics.accuracy_score(np.array(y_test,dtype=int), y_pred) > best_acc:
                best_model = clf

        #save the model
        self.save_pickle(best_model, self.model_file)

        metrics_f['accuracies'] = json.dumps(list(acc_value))
        metrics_f['precision'] = json.dumps(list(prec_value))
        metrics_f['recall'] = json.dumps(list(recall_value))
        metrics_f['best_accuracy'] = float(np.max(acc_value))
        metrics_f['acc_mean'] = float(np.mean(acc_value))
        predictions = {'sicCodes':meters_test.tolist(),'label':json.dumps(y_pred.tolist()),'right_label':y_test.tolist()}
        metrics_f['predictions'] = predictions
  
        return metrics_f

    def predict(self):
        """
        Predice las etiquetas para nuevos datos cargando un modelo 
        previamente entrenado y retorna las etiquetas para C/U de los medidores.

        :returns: Nombre de los medidores y etiquetas predichas
        :rtype: dict
        """
        with open(self.model_file, 'rb') as f:
            model = pickle.load(f)

        y_pred = model.predict(self.data)
            
        return {"sicCodes":self.meters.values.tolist(), "label":json.dumps(y_pred.tolist())}
        
