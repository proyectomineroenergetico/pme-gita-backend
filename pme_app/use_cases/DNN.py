import random
import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
from shared import logger
from tqdm import tqdm


class RNNSeries(nn.Module):
    """
    CNN-GRU neural network.

    :param n_layers: Número de capas apiladas del tipo BiGRU  (1)
    :type n_layers: int
    :param n_features: Número de medidores (72) 
    :type n_features: int
    :param len_seq: Tamaño de la secuencia de entrada (30 para un mes de análisis)
    :type len_seq: int
    :param cells: Número de celdas  de memoria para las capas BiGRU
    :type cells: int
    :param hidden_dnn: Número de neuronas en las capas completamente conectadas (full connected layers)
    :type hidden_dnn: int
    :param num_classes: Número de clases a clasificar
    :type num_classes: int
    :param droput_prob: Probabilidad de rechazo para la regularización
    :type droput_prob: float
    """

    def __init__(self, n_layers=1, n_features=72, len_seq=30, cells=128, hidden_dnn=128, num_classes=2, droput_prob=0.5):
        super().__init__()

        self.CNN=nn.Conv1d(n_features, 128, 7)
        self.dropout=nn.Dropout(p=droput_prob)

        self.gru=nn.GRU(input_size=128,hidden_size=cells,
                         num_layers=n_layers, batch_first=True,
                         bidirectional=True, dropout=0.5)

        self.linear = nn.Linear(256, hidden_dnn)  
        self.linear2=nn.Linear(hidden_dnn, num_classes)

        torch.nn.init.kaiming_uniform_(self.linear.weight)
        torch.nn.init.kaiming_uniform_(self.linear2.weight)

    def forward(self, x):
        """
        Definición de la red neuronal.
        
        :returns: la red 
        """
        x=x.permute(0,2,1)
        x=self.CNN(x)
        x=self.dropout(x)
        x=x.permute(0,2,1)
        x,hn=self.gru(x)
        x=x[:,-1,:]
        x=self.dropout(x)
        x = F.leaky_relu(self.linear(x))
        x = F.softmax(self.linear2(x), dim=-1)
        if torch.cuda.is_available():
            self.filters=self.CNN.weight.cpu().detach().numpy()
        else:
            self.filters=self.CNN.weight.detach().numpy()
        return x

class SeqDataset(data.Dataset):
    """
    Generador de datos para entrenar la red CNN-GRU.

    :param tensor: Tensor de train o test
    :type tensor: tensor
    :param labels: Arreglo con la lista de etiquetas asociada a cada muestra
    :type labels: array
    :param randomize: Booleano para definir la aleatorización de parámetros True para train y Flase para test
    :type  randomize: bool
    """

    def __init__(self, tensor, labels, randomize=False):

        index=torch.arange(len(labels))
        if randomize:
            random.shuffle(index) #shuffle method
        self.index =index
        self.tensor=tensor
        self.labels=labels

    def __len__(self):
        return len(self.index)

    def __getitem__(self, idx):
        key_idx = self.index[idx]
        X=self.tensor[key_idx,:,:]
        y=self.labels[key_idx]

        return X, y

class SeqDataset_pred(data.Dataset):
    """
    Generador de datos para entrenar la red CNN-GRU.

    :param tensor: Tensor de train o test
    :type tensor: tensor
    """

    def __init__(self, tensor):

        index=torch.arange(tensor.size()[0])
        self.index =index
        self.tensor=tensor

    def __len__(self):
        return len(self.index)

    def __getitem__(self, idx):
        key_idx = self.index[idx]
        X=self.tensor[key_idx,:,:]
        return X
class Trainer:
    """
    Realiza el entrenamiento o la clasificación de los datos una vez generada la red neuronal.

    :param model: Modelo Torch a entrenar
    :type model: NN model
    :param optimizer: Algoritmo de optimización (see https://pytorch.org/docs/stable/optim.html)
    :type optimizer: torch object
    :param criterion: Función de perdida a optimizar (see https://pytorch.org/docs/stable/nn.html#loss-functions)
    :type criterion: torch object
    :param file_model: Path donde se almacenaŕa el modelo entrenado
    :type file_model: str
    :param n_epochs: Numero de etapas de entrenamiento
    :type n_epochs: int
    :param patience: Tiempo de espera para la detención del entrenamiento
    :type patience: int
    """

    def __init__(self, model, optimizer, criterion, file_model, n_epochs=1000, patience=1000):

        self.model=model
        self.optimizer=optimizer
        self.criterion=criterion
        self.n_epochs=n_epochs
        self.patience=patience
        self.ntrain=0
        self.file_model=file_model
        self.validate=True


    def predict(self, model, test_loader):
        """
        Predice la clasificación de los datos de test.

        :param model: Modelo Entrenado previamente
        :type model: NN model
        :param test_loader: Datos de test
        :type test_loader: Tensor
        :returns: Predicciones del clasificador
        :rtype: array
        """
        model.eval() # prep model for evaluation
        y_pred=[]

        for X, y in test_loader:
            # forward pass: compute predicted outputs by passing inputs to the model
            if torch.cuda.is_available():
                X=X.cuda()
                model.cuda()
            data_val_out= model(X)
            if torch.cuda.is_available():
                data_val_out=data_val_out.cpu()
            _, predicted = torch.max(data_val_out.data, 1)
            y_pred.append(data_val_out.detach().numpy())
        y_pred=np.concatenate(y_pred, axis=0)

        return y_pred

    def predict2(self, model, test_loader):
        """
        Predice la clasificación de los datos nuevos.

        :param model: Modelo Entrenado previamente
        :type model: NN model
        :param test_loader: Datos 
        :type test_loader: Tensor
        :returns: Predicciones del clasificador
        :rtype: array
        """
        model.eval() # prep model for evaluation
        y_pred=[]
    

        for X in test_loader:
            # forward pass: compute predicted outputs by passing inputs to the model
            if torch.cuda.is_available():
                model.cuda()
                X=X.cuda()
            data_val_out=model(X)
            if torch.cuda.is_available():
                data_val_out=data_val_out.cpu()
            y_pred.append(data_val_out.detach().numpy())
        y_pred=np.concatenate(y_pred, axis=0)

        return y_pred

    def fit(self, train_loader, test_loader):
        """
        Entrena el modelo redes neuronales.

        :param train_loader: Datos de entrenamiento
        :type train_loader: tensor
        :param test_loader: Datos de test
        :type test_loader: tensor
        :returns: Modelo de Neural Network
        :rtype: NN model
        """

        if torch.cuda.is_available():
            self.model=self.model.cuda()  
        valid_loss_min = np.Inf # set initial "min" to infinity
        train_loss_min=np.Inf

        if self.ntrain==0:
            self.ntrain=len(train_loader)
        cont_patience=0
        for epoch in range(self.n_epochs):
            start=time.time()
            # monitor training loss
            train_loss = 0.0
            valid_loss = 0.0
            cdata=0
            self.model.train() # prep model for training
            pbar=tqdm(train_loader)
            correct_tr = 0
            total_tr = 0
            for X, y in pbar:
                if self.ntrain>0:
                    if cdata==self.ntrain:
                        break    
                # clear the gradients of all optimized variables
                self.optimizer.zero_grad()
                if torch.cuda.is_available():
                    X=X.cuda()
                    y=y.cuda()
                data_out=self.model(X)
                if torch.cuda.is_available():
                    data_out=data_out.cuda()
                loss = self.criterion(data_out, y)
                loss.backward()
                self.optimizer.step()
                train_loss += loss.item()*data_out.size(0)

                cdata+=1
                _, predicted = torch.max(data_out.data, 1)

                total_tr += y.size(0)
                correct_tr += (predicted == y).sum().item()
            acctrain=100*correct_tr/total_tr
            cdata=0

            ######################    
            # validate the model #
            ######################

            correct = 0
            total = 0
            if self.validate:
                self.model.eval() # prep model for evaluation
                for X, y in test_loader:
                    # forward pass: compute predicted outputs by passing inputs to the model
                    if torch.cuda.is_available():
                        X=X.cuda()
                        y=y.cuda()

                    data_val_out=self.model(X)
                    
                    if torch.cuda.is_available():
                        data_val_out=data_val_out.cuda()

                    # calculate the loss
                    loss = self.criterion(data_val_out, y)
                    # update running validation loss 
                    valid_loss += loss.item()*data_val_out.size(0)
                    cdata+=1
                    _, predicted = torch.max(data_val_out.data, 1)

                    total += y.size(0)
                    correct += (predicted == y).sum().item()

            train_loss = train_loss/len(train_loader.dataset)
            accval=100*correct/total

            if self.validate:

                valid_loss = valid_loss/len(test_loader.dataset)

                logger.info('Epoch: {} \tTr Loss: {:.6f} \tValid Loss: {:.6f} \t Val ACC: {:.6f} \tTime: {:.6f}'.format(
                    epoch, 
                    train_loss,
                    valid_loss,
                    accval,
                    time.time()-start
                    ))
                
                # save model if validation loss has decreased
                if valid_loss <= valid_loss_min:
                    logger.info('Validation loss decreased ({:.6f} --> {:.6f}).  Saving model ...'.format(
                        valid_loss_min,
                        valid_loss))
                    torch.save(self.model.state_dict(), self.file_model+'.pt')
                    valid_loss_min = valid_loss
                    cont_patience=0
                else:
                    cont_patience+=1
                f=open(self.file_model+'_loss.csv', "a")
                f.write(str(train_loss)+", "+str(valid_loss)+", "+str(accval)+", "+str(time.time()-start)+"\n")
                f.close()
            else:
                
                logger.info('Epoch: {} \tTraining Loss: {:.6f}  \tTime: {:.6f}'.format(
                    epoch, 
                    train_loss,
                    time.time()-start
                    ))
                
                if train_loss <= train_loss_min:
                    logger.info('Train loss decreased ({:.6f} --> {:.6f}).  Saving model ...'.format(
                    train_loss_min,
                    train_loss))
                    torch.save(self.model.state_dict(), self.file_model)
                    train_loss_min = train_loss
                    cont_patience=0
                else:
                    cont_patience+=1
                f=open(self.file_model+'_loss.csv', "a")
                f.write(str(train_loss)+", "+str(train_loss_min)+", "+str(time.time()-start)+"\n")
                f.close()
            
            if cont_patience==self.patience:
                break

        return self.model
