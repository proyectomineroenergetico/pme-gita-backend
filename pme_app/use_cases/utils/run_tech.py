import json
from datetime import datetime

import numpy as np
import pandas as pd
from app import app
from flask import Blueprint, jsonify, make_response, request
from flask_cors import CORS
from pme_app.domain.entities.userModel import UserModel
from pme_app.repository.db_services.filesByuser import (getFilesByUser,
                                                        saveFilesByUser)
from pme_app.repository.db_services.getDataByMeterService import getAllData
from pme_app.repository.db_services.getMetersService import getMeterList
from pme_app.repository.db_services.histByMethodService import \
    saveHistByTechnique
from pme_app.repository.db_services.performaceByModelService import \
    savePerformanceData
from pme_app.use_cases.classEnd2End import Classend2end
from pme_app.use_cases.classifier import Classifier
from pme_app.use_cases.featureExtractionTime import FeatureExtraction
from pme_app.use_cases.frecFeatureExtraction import frecuencyFeatures
from pme_app.use_cases.fromTensors import FormTensors
from pme_app.use_cases.getLabelsProblems import labelsProblem
from pme_app.use_cases.kmeansMethod import kMeansML
from pme_app.use_cases.lSVMMethod import linearSVM
from shared import SSE, data_files, logger


def nn_tech_fit_predict(dataPath, label_path, meters, user_id, cp):
    """
    Realiza el entrenamiento del modelo de redes neuronales.

    :param dataPath: Path donde están los datos
    :type dataPath: str
    :param label_path: Path donde están las etiquetas
    :type label_path: str
    :param meters: lista de medidores
    :type meters: list
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: clase de problema seleccionado
    :type cp: str
    """
    with app.app_context():
        name = f"NEURAL-NETWORK_FIT_{user_id}_{cp}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        SSE.publish({"message": "Iniciando la extracción de características de Tiempo",
                    "id": user_id,"status": 0,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Iniciando la extracción de características de Tiempo",
                            user_id=user_id, status_a=0,date=datetime.now())
        modelPath = f'{data_files}/{user_id}_NNModel_{cp}.pt'
        tensorFile = f'{data_files}/{user_id}_tensor_{cp}.pt'
        logger.info('creating Tensor')
        TesorCreator = FormTensors(file_data=dataPath, len_seq=30, labels_data=label_path, nseries=len(meters))

        TesorCreator.form_tensor(tensorFile)
        classifier=Classend2end(file_tensor=tensorFile, class_problem=cp, model_path=modelPath)
        logger.info('training')
        classifier.train_test_split()
        classifier.standardize()
        SSE.publish({"message": "Emtrenando el modelo con la técnica NEURAL-NETWORK",
                    "id": user_id,"status": 20,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a="Emtrenando el modelo con la técnica NEURAL-NETWORK",
                            user_id=user_id, status_a=20,date=datetime.now())
        classifier.train()
        SSE.publish({"message": "Clasificando los datos con la técnica NEURAL-NETWORK",
                    "id": user_id,"status": 70,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a="Clasificando los datos con la técnica NEURAL-NETWORK",
                            user_id=user_id, status_a=70,date=datetime.now())
        logger.info('predicting')
        classifier.predict()
        results = classifier.evaluate_performance()
        lp = labelsProblem(labels_path='',problem_type=cp)
        nc = lp.numClassByProblem()
        results['class_by_label'] = nc[1]
        code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
        logger.info(f"the data for nn_{user_id}_{cp} model has been saved  {code1[0]}")
        code = saveFilesByUser(user_id,nn_path=modelPath, cp=cp)
        logger.info(f"the nn model file has been saved  {code[0]}")
        SSE.publish({"message": "El entrenamiento y clasificación con la técnica NEURAL-NETWORK finalizó correctamente",
                    "id": user_id,"status": 100,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a="El entrenamiento y clasificación con la técnica NEURAL-NETWORK finalizó correctamente",
                            user_id=user_id, status_a=100,date=datetime.now())

def nn_tech_predict(dataPath, modelPath, meters, cp, user_id):
    """
    Realiza la predicción con el modelo de redes neuronales.

    :param dataPath: Path donde están los datos
    :type dataPath: str
    :param model_path: Path donde está el modelo entrenado
    :type model_path: str
    :param meters: lista de medidores
    :type meters: list
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: clase de problema seleccionado
    :type cp: str
    :returns: resultados de la clasificación
    :rtype: dict
    """
    with app.app_context():
        name = f"NEURAL-NETWORK_PREDICT_{user_id}_{cp}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        SSE.publish({"message": "Cargando el archivo con las características de Tiempo",
                    "id": user_id,"status": 0,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                                msg_a= "Cargando el archivo con las características de Tiempo",
                                user_id=user_id, status_a=0,date=datetime.now())
        tensorFile = f'{data_files}/{user_id}_tensor_{cp}.pt'
        TesorCreator=FormTensors(file_data=dataPath, len_seq=30,nseries=len(meters),labels_data='')
        logger.info('crectting  tensor')
        TesorCreator.form_tensor(tensorFile)
        SSE.publish({"message": "Cargando el modelo con la técnica NEURAL-NETWORK",
                    "id": user_id,"status": 20,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                                msg_a= "Cargando el modelo con la técnica NEURAL-NETWORK",
                                user_id=user_id, status_a=20,date=datetime.now())
        classifier=Classend2end(file_tensor=tensorFile, class_problem=cp, model_path=modelPath,evaluate=False)
        lp = labelsProblem(labels_path='',problem_type=cp)
        logger.info('predicting')
        SSE.publish({"message": "Clasificando los datos con la técnica NEURAL-NETWORK ",
                    "id": user_id,"status": 70,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Clasificando los datos con la técnica NEURAL-NETWORK ",
                            user_id=user_id, status_a=70,date=datetime.now())
        nc = lp.numClassByProblem()
        results = classifier.predict(num_classes=nc[0])
        results['class_by_label'] = nc[1]
        code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
        logger.info(f"the data for nn_{user_id}_{cp} model has been saved  {code1[0]}")

        SSE.publish({"message": " La clasificación con la técnica NEURAL-NETWORK finalizó correctamente",
                    "id": user_id,"status": 100,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "La clasificación con la técnica NEURAL-NETWORK finalizó correctamente",
                            user_id=user_id, status_a=100,date=datetime.now())
        logger.info('results')

def frec_feat(featurespath, user_id, selectedBands,technique,do,cp,name,sse=False):
    """
    Extrae las características basadas en frecuencia ya sea tomando unas existentes o
    se calculán unas nuevas.

    :param featurespath: Path donde están almacenadas las características o donde serán guardadas
    :type featurespath: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param selectedBands: Bandas seleccionadas para trabajar con la técnica de ML
    :type selectedBands: bool list
    :param sse: booleano que indica si se utilizarán los sse
    :type sse: bool
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    :returns: | características por banda seleccionada
    :rtype: dataframe
    """
    if sse:
        
        SSE.publish({"message": "Iniciando la extracción de características en frecuencia",
                    "id": user_id,"status": 0,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Iniciando la extracción de características en frecuencia",
                            user_id=user_id, status_a=0,date=datetime.now())

        
        featurespath = f'{data_files}/{user_id}_featuresFrecuency.csv'
        extractor = frecuencyFeatures()
        data = getAllData(user_id)
        features = extractor.featureExtraction(dataSeriesByFrt=data)
        features.to_csv(featurespath)


        SSE.publish({"message": "La extracción de características en Frecuencia terminó correctamente",
                    "id": user_id,"status": 20,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "La extracción de características en Frecuencia terminó correctamente",
                            user_id=user_id, status_a=20,date=datetime.now())

        code = saveFilesByUser(user_id,features_path=featurespath)
        logger.info(f"the files has been saved  {code[0]}")
        featuresByBand = extractor.featuresByBand(features, np.array(selectedBands, dtype=bool))
        selbandind = np.where(selectedBands)[0].tolist()
        logger.info("Feature Extraction was make successfully")
        return featuresByBand, selbandind,features
    else:
        SSE.publish({"message": "Cargando las características en Frecuencia",
                    "id": user_id,"status": 0,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Cargando las características en Frecuencia",
                            user_id=user_id, status_a=0,date=datetime.now())
        features = pd.read_csv(featurespath)
        extractor = frecuencyFeatures()
        featuresByBand = extractor.featuresByBand(features, np.array(selectedBands, dtype=bool))
        selbandind = np.where(selectedBands)[0].tolist()

        SSE.publish({"message": "Las características en Frecuencia se cargaron correctamente",
                    "id": user_id,"status": 20,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Las características en Frecuencia se cargaron correctamente",
                            user_id=user_id, status_a=20,date=datetime.now())
        logger.info("Feature Extraction was make successfully")
        return featuresByBand, selbandind,features

def kmeans_tech_fit_predict(selbandind,featuresByBand, meters, user_id, name):
    """
    Realiza el entrenamiento del modelo de K-Means.

    :param featuresbyBand: características extraidas por frec_feat
    :type featuresByBand: dataframe
    :param selbandind: Indices de las bandas seleccionadas
    :type label_path: list
    :param meters: lista de medidores
    :type meters: list
    :param user_id: Id del usuario actual
    :type user_id: int
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    """
    logger.info('K-MEANS')
    SSE.publish({"message": "Iniciando la técnica K-MEANS",
                "id": user_id,"status": 60,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                                    msg_a= "Iniciando la técnica K-MEANS",
                                    user_id=user_id, status_a=60,date=datetime.now())
    results = {}
    centers = {}
    step = 40/len(selbandind)
    total = 60
    for i,d in zip(selbandind,featuresByBand):
        logger.info(f'BAND {i}')
        ldaPath = f'{data_files}/{user_id}_lda_Band_{i}.pickle'
        modelPath = f'{data_files}/{user_id}_kmeansModel_Band_{i}.pickle'
        kmeandTeach = kMeansML(data=d,meters=list(meters),ldaPath=ldaPath, modelPath=modelPath)
        r,c= kmeandTeach.fit_and_predict()
        if i == 8:
            results[f'Bandas_1-4'] = r
        elif i == 9:
            results[f'Bandas_5-8'] = r
        elif i == 10:
            results[f'Bandas_all'] = r
        else:
            results[f'Banda_{i+1}'] = r
        code = saveFilesByUser(user_id,lda_path=ldaPath,kmeans_path=modelPath,banda=i)
        logger.info(f"the files has been saved  {code[0]}")
        
        total += step
        if total >= 100:
            SSE.publish({"message": "La técnica K-MEANS finalizó correctamente",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
                                    msg_a= "La técnica K-MEANS finalizó correctamente",
                                    user_id=user_id, status_a=total,date=datetime.now())
            code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
            logger.info(f"the data for kmeans_{user_id} model has been saved  {code1[0]}")
        else:
            SSE.publish({"message": "Entrenando el modelo con la técnica K-MEANS",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
                                    msg_a= "Entrenando el modelo con la técnica K-MEANS",
                                    user_id=user_id, status_a=total,date=datetime.now())
    logger.info('the method has been finished successfully')
        

def kmeans_tech_predict(selbandind,featuresByBand, meters, user_id, name):
    """
    Realiza la classificación utilizando un modelo de K-Means entrnado.

    :param featuresbyBand: características extraidas por frec_feat
    :type featuresByBand: dataframe
    :param selbandind: Indices de las bandas seleccionadas
    :type selbandind: list
    :param meters: lista de medidores
    :type meters: list
    :param user_id: Id del usuario actual
    :type user_id: int
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    """
    SSE.publish({"message": "Iniciando la técnica K-MEANS",
                "id": user_id,"status": 60,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                                    msg_a= "Iniciando la técnica K-MEANS",
                                    user_id=user_id, status_a=60,date=datetime.now())
    results = {}
    centers = {}
    step = 40/len(selbandind)
    total = 60
    for i,d in zip(selbandind,featuresByBand):
        ldaPath = getFilesByUser(user_id=user_id,lda_path=True,banda=i)
        modelPath = getFilesByUser(user_id=user_id,kmeans_path=True,banda=i)
        if modelPath[0] == 400 or ldaPath[0] == 400:
            SSE.publish({"message":f"Algunos problemas ocurrieron extrayendo el model path o lda path, por favor ejecute de nuevo entrenar y clasificar. {modelPath[1]},{ldaPath[1]}",
                        "id":user_id,"model_name":name,"date":datetime.now(),"status": 60}, type='error')
            saveHistByTechnique(modelName=name,
                                msg_a= f"Algunos problemas ocurrieron extrayendo el model path o lda path, por favor ejecute de nuevo entrenar y clasificar. {modelPath[1]},{ldaPath[1]}",
                                user_id=user_id, status_a=60,date=datetime.now())
                #TODO: check the errors
        kmeandTeach = kMeansML(data=d,meters=list(meters),ldaPath=ldaPath[1], modelPath=modelPath[1])
        r,c= kmeandTeach.predict()
        if i == 8:
            results[f'Bandas_1-4'] = r
        elif i == 9:
            results[f'Bandas_5-8'] = r
        elif i == 10:
            results[f'Bandas_all'] = r
        else:
            results[f'Banda_{i+1}'] = r
        
        total += step
        if total >= 100:
            logger.info('the method has been finished successfully')
            SSE.publish({"message": "La técnica K-MEANS finalizó correctamente",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
                                msg_a= "La técnica K-MEANS finalizó correctamente",
                                user_id=user_id, status_a=total,date=datetime.now())
            code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
            logger.info(f"the data for kmeans_{user_id} model has been saved  {code1[0]}")
            logger.info('the method has been finished successfully')
        else:
            SSE.publish({"message": "Clasificando los datos con la técnica K-MEANS",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
                                    msg_a= "Clasificando los datsos con la técnica K-MEANS",
                                    user_id=user_id, status_a=total,date=datetime.now())
    logger.info('the method has been finished successfully')


def lsvm_tech_fit_predict(featuresByBand,selbandind,user_id,labels_by_problem, meters,cp,name,classByLabel):
    """
    Realiza el entrenamiento del modelo de SVM.

    :param featuresbyBand: características extraidas por frec_feat
    :type featuresByBand: dataframe
    :param selbandind: Indices de las bandas seleccionadas
    :type selbandind: list
    :param labels_by_problem: etiquetas según la clase de problema seleccionado
    :type labels_by_problem: array
    :param meters: lista de medidores
    :type meters: list
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: clase de problema seleccionado
    :type cp: str
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    :param classByLabel: Correspondencia del tipo de problema con las etiquetas de los resultados
    :type classByLabel: str
    """
    SSE.publish({"message": "Iniciando la técnica LINEAR-SVM",
                "id": user_id,"status": 60,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                        msg_a= "Iniciando la técnica LINEAR-SVM",
                        user_id=user_id, status_a=60,date=datetime.now())
    logger.info('LSVM')
    results = {}
    performace = {}
    logger.info('fit_and_predict')
    step = 40/len(selbandind)
    total = 60
    for i,d in zip(selbandind,featuresByBand):
        logger.info(f'Band_{i}')
        modelPath = f'{data_files}/{user_id}_{cp}_svmModel_band_{i}.pickle'
        #labelsProblem
        lsvmTech = linearSVM(data=d,meters=meters,y=labels_by_problem, file=modelPath)
        performace = lsvmTech.fit_and_predict()
        if i == 8:
            results[f'Bandas_1-4'] = performace
        elif i == 9:
            results[f'Bandas_5-8'] = performace
        elif i == 10:
            results[f'Bandas_all'] = performace
        else:
            results[f'Banda_{i+1}'] = performace

        code = saveFilesByUser(user_id,svm_path=modelPath,banda=i,cp=cp)
        logger.info(f"the files has been saved  {code[0]} {code[1]}")
        
        total += step
        if total >= 100:
            SSE.publish({"message": "La técnica LINEAR-SVM finalizó correctamente",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
                        msg_a=  "La técnica LINEAR-SVM finalizó correctamente",
                        user_id=user_id, status_a=total,date=datetime.now())
            results['class_by_label'] = classByLabel
            code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
            logger.info(f"the data for lsvm_{user_id}_{cp} model has been saved  {code1[0]} {code[1]}")
        else:
            SSE.publish({"message": "Entrenando el modelo con la técnica LINEAR-SVM",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
                        msg_a= "Entrenando el modelo con la técnica LINEAR-SVM",
                        user_id=user_id, status_a=total,date=datetime.now())

def lsvm_tech_predict(featuresByBand,selbandind,user_id, meters,cp,name,classByLabel):
    """
    Realiza la classificación utilizando un modelo de SVM entrnado.

    :param featuresbyBand: características extraidas por frec_feat
    :type featuresByBand: dataframe
    :param selbandind: Indices de las bandas seleccionadas
    :type selbandind: list
    :param meters: lista de medidores
    :type meters: list
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: clase de problema seleccionado
    :type cp: str
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    :param classByLabel: Correspondencia del tipo de problema con las etiquetas de los resultados
    :type classByLabel: str
    """
    SSE.publish({"message": "Iniciando la técnica LINEAR-SVM",
                "id": user_id,"status": 60,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
            msg_a= "Iniciando la técnica LINEAR-SVM",
            user_id=user_id, status_a=60,date=datetime.now())
    results = {}
    performace = {}
    step = 40/len(selbandind)
    total = 60
    for i,d in zip(selbandind,featuresByBand):
        logger.info(f'Band_{i}')
        modelPath = getFilesByUser(user_id=user_id,svm_path=True,banda=i,cp=cp)
        if modelPath[0] == 400:
            SSE.publish({"message":f'Algunos problemas ocurrieron extrayendo el model path o lda path, por favor ejecute de nuevo entrenar y clasificar. {modelPath[1]}',
                        "id": user_id,"model_name":name,"date":datetime.now()}, type='error')
            saveHistByTechnique(modelName=name,
            msg_a= f'Algunos problemas ocurrieron extrayendo el model path o lda path, por favor ejecute de nuevo entrenar y clasificar. {modelPath[1]}',
            user_id=user_id, status_a=60,date=datetime.now())
            #TODO: check the errors
        lsvmTech = linearSVM(data=d,meters=meters,y={}, file=modelPath[1])
        performace["predictions"] = lsvmTech.predict()
        if i == 8:
            results[f'Bandas_1-4'] = performace
        elif i == 9:
            results[f'Bandas_5-8'] = performace
        elif i == 10:
            results[f'Bandas_all'] = performace
        else:
            results[f'Banda_{i+1}'] = performace
        
        total += step
        if total >= 100:
            SSE.publish({"message": "La técnica LINEAR-SVM finalizó correctamente",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
            msg_a= "La técnica LINEAR-SVM finalizó correctamente",
            user_id=user_id, status_a=total,date=datetime.now())
            results['class_by_label'] = classByLabel
            code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
            logger.info(f"the data for lsvm_{user_id}_{cp} model has been saved  {code1[0]} {code1[1]}")
        else:
            SSE.publish({"message": "Entrenando el modelo con la técnica LINEAR-SVM",
                        "id": user_id,"status": total,"model_name":name,"date":datetime.now()}, type='running.method')
            saveHistByTechnique(modelName=name,
            msg_a= "Entrenando el modelo con la técnica LINEAR-SVM",
            user_id=user_id, status_a=total,date=datetime.now())

def time_feat(featurespath, user_id,dataPath,label_path,sse,technique,do,cp,name):
    """
    Extrae las características temporales ya sea tomando unas existentes o
    se calculán unas nuevas.

    :param featurespath: Path donde están almacenadas las características o donde serán guardadas
    :type featurespath: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param dataPath: Path donde se encuentran los datos
    :type dataPath: dataframe
    :param label_path: Path donde se encuentran las etiquetas
    :type label_path: str
    :param sse: booleano que indica si se utilizarán los sse
    :type sse: bool
    :returns: | características por banda seleccionada
    :rtype: dataframe
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    """
    if sse:
        logger.info('calc_feat')
        SSE.publish({"message": "Iniciando la extracción de características de Tiempo",
                    "id": user_id,"status": 0,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Iniciando la extracción de características de Tiempo",
                            user_id=user_id, status_a=0,date=datetime.now())
        featuresPath =  f'{data_files}/{user_id}_featureTime.csv'
        timeFeatures = FeatureExtraction(data1=dataPath, featurePath=featuresPath,labelsPath=label_path)
        features, featuresAndlabels = timeFeatures.runProcess()
        logger.info("Feature Extraction was make successfully")
        code = saveFilesByUser(user_id,features_path2=featuresPath)
        logger.info(f"the files has been saved  {code[0]}")

        SSE.publish({"message": "La extracción de características de Tiempo finalizó correctamente",
                    "id": user_id,"status": 20,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "La extracción de características de Tiempo finalizó correctamente",
                            user_id=user_id, status_a=20,date=datetime.now())

        return featuresAndlabels
    else:
        logger.info('reading feat')
        SSE.publish({"message": "Cargando el archivo con las características de Tiempo",
                    "id": user_id,"status": 0,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Cargando el archivo con las características de Tiempo",
                            user_id=user_id, status_a=0,date=datetime.now())
        featuresAndlabels = pd.read_csv(featurespath)
        logger.info("Feature Extraction was make successfully")
        if 'Unnamed: 0' in featuresAndlabels:
            featuresAndlabels = featuresAndlabels.drop(columns=['Unnamed: 0'])
        SSE.publish({"message": "Se cargó con exito el archivo con las características de Tiempo",
                    "id": user_id,"status": 20,"model_name":name,"date":datetime.now()}, type='running.method')
        saveHistByTechnique(modelName=name,
                            msg_a= "Se cargó con exito el archivo con las características de Tiempo",
                            user_id=user_id, status_a=20,date=datetime.now())
        return featuresAndlabels


def rf_tech_fit_predict(featuresAndlabels,user_id,cp,name,classByLabel):
    """
    Realiza la predicción con el modelo Random Forest entrenado.

    :param featuresAndlabels: Datos a clasificar
    :type featuresAndlabels: dataframe
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: clase de problema seleccionado
    :type cp: str
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    :param classByLabel: Correspondencia del tipo de problema con las etiquetas de los resultados
    :type classByLabel: str
    """ 
    SSE.publish({"message": "Entrenando el modelo con la técnica RANDOM-FOREST",
                "id": user_id,"status": 60,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                            msg_a=  "Entrenando el modelo con la técnica RANDOM-FOREST",
                            user_id=user_id, status_a=60,date=datetime.now())
    modelPath = f'{data_files}/{user_id}_rfModel_{cp}.pickle'
    rf_classifier = Classifier(feature_set=featuresAndlabels, class_problem=cp, model_path=modelPath , classifier='RF')
    rf_classifier.train_test_split()
    rf_classifier.standardize()
    rf_classifier.train()
     
    SSE.publish({"message": "Clasificando los datos con el modelo de RANDOM-FOREST",
                "id": user_id,"status": 80,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                            msg_a= "Clasificando los datos con el modelo de RANDOM-FOREST",
                            user_id=user_id, status_a=80,date=datetime.now())
    rf_classifier.predict()
    results = rf_classifier.evaluate_performance()
    results['class_by_label'] = classByLabel
    code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
    logger.info(f"the data for rf_{user_id}_{cp} model has been saved  {code1[0]}")
    code = saveFilesByUser(user_id,rf_path=modelPath,cp=cp)
    logger.info(f"the files has been saved  {code[0]}")
     
    SSE.publish({"message": "El entrenamiento y clasificación con la técnica RANDOM-FOREST finalizó correctamente",
                "id": user_id,"status": 100,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                            msg_a= "El entrenamiento y clasificación con la técnica RANDOM-FOREST finalizó correctamente",
                            user_id=user_id, status_a=100,date=datetime.now())

def rf_tech_predict(features,modelPath,user_id,cp, name,classByLabel):
    """
    Realiza el entrenamiento del modelo Random Forest.

    :param features: Datos a clasificarcon etiquetas
    :type features: dataframe
    :param modelPath: Path donde será almacenado el modelo
    :type modelPath: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: clase de problema seleccionado
    :type cp: str
    :param name: Identificador del modelo para trazar el historial de ejecución
    :type name: str 
    :param classByLabel: Correspondencia del tipo de problema con las etiquetas de los resultados
    :type classByLabel: str
    """ 
    SSE.publish({"message": "Iniciando la técnica RANDOM-FOREST",
                "id": user_id,"status": 60,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                            msg_a= "Iniciando la técnica RANDOM-FOREST",
                            user_id=user_id, status_a=60,date=datetime.now())
    logger.info(features.keys())
    rf_classifier = Classifier(feature_set=features, class_problem=cp, model_path=modelPath, classifier='RF')
    results = rf_classifier.predict2() 
    SSE.publish({"message": "La clasificación de los datos con la técnica RANDOM-FOREST finalizó correctamente",
                "id": user_id,"status": 100,"model_name":name,"date":datetime.now()}, type='running.method')
    saveHistByTechnique(modelName=name,
                            msg_a= "La clasificación de los datos con la técnica RANDOM-FOREST finalizó correctamente",
                            user_id=user_id, status_a=100,date=datetime.now())
    results['class_by_label'] = classByLabel
    code1 = savePerformanceData(model_name=name,user_id=user_id,data=results)
    logger.info(f"the data for rf_{user_id}_{cp} model has been saved  {code1[0]}")
