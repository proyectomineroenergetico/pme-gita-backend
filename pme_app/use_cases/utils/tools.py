import datetime
import itertools
import random
from collections import Counter, defaultdict

import numpy as np
import pandas as pd
from numpy.lib import stride_tricks
from scipy.signal import butter, freqz, lfilter, spectrogram
from skimage import util


def order_series(series):
    """
    Ordena los datos de entrada entregando una serie por 
    medidor y dando formato a los labels temporales.

    :param series: datos de entrada 
    :type series: dataframe
    :returns: Las series por medidor
    :rtype: dataframe
    """

    NewSeries = pd.DataFrame()
    k = 0
    for index, row in series.iterrows():
        dft = row
        active = dft.iloc[2:26].tolist()
        reactive = dft.iloc[27:51].fillna(0)
        reactive = reactive.tolist()
        penalization = dft.iloc[52:76].fillna(0)
        penalization = penalization.tolist()
        dt = pd.to_datetime(series['Fecha'][index], format='%y%m%d')
        datet = []
        for i in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,0]:
            tm = datetime.time(i,0,0)
            datet.append(dt.combine(dt, tm))
        df = pd.DataFrame(list(zip(datet, active, reactive, penalization)))
        NewSeries = NewSeries.append(df)
        k +=1
    NewSeries.columns = ['Date','Active', 'Reactive', 'Penalty']
        
    return NewSeries

def butter_lowpass(cutoff, fs, order=5):
    """
    Filtro butterwoth pasa bajas.

    :param cutoff: Frecuencia de corte
    :type cutoff: float
    :param fs: Frecuencia de muestreo
    :type fs: float
    :param order: Orden del filtro
    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='lowpass', analog=False)
    return b, a

def butter_bandpass(cutoff, fs, order=5):
    """
    Filtro butterwoth pasa bandas.

    :param cutoff: Frecuencia de corte
    :type cutoff: float
    :param fs: Frecuencia de muestreo
    :type fs: float
    :param order: Orden del filtro
    """
    nyq = 0.5 * fs
    start = cutoff[0] / nyq
    stop = cutoff[1] /nyq
    normal_cutoff = [start,stop]
    b, a = butter(order, normal_cutoff, btype='bandpass', analog=False)
    return b, a

def butter_highpass(cutoff, fs, order=5):
    """
    Filtro butterwoth pasa altas.

    :param cutoff: Frecuencia de corte
    :type cutoff: float
    :param fs: Frecuencia de muestreo
    :type fs: float
    :param order: Orden del filtro
    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='highpass', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    """
    Filtra los datos con un filtro butterwoth pasa bajas.

    :param cutoff: Frecuencia de corte
    :type cutoff: float
    :param fs: Frecuencia de muestreo
    :type fs: float
    :param order: Orden del filtro
    :param data: datos a filtrar
    :type data: array
    :returns: Los datos filtrados
    :rtype: array
    """
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def butter_bandpass_filter(data, cutoff, fs, order=5):
    """
    Filtra los datos con un filtro butterwoth pasa bandas.

    :param cutoff: Frecuencia de corte
    :type cutoff: float
    :param fs: Frecuencia de muestreo
    :type fs: float
    :param order: Orden del filtro
    :param data: datos a filtrar
    :type data: array
    :returns: Los datos filtrados
    :rtype: array
    """
    b, a = butter_bandpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def butter_highpass_filter(data, cutoff, fs, order=5):
    """
    Filtra los datos con un filtro butterwoth pasa altas.

    :param cutoff: Frecuencia de corte
    :type cutoff: float
    :param fs: Frecuencia de muestreo
    :type fs: float
    :param order: Orden del filtro
    :param data: datos a filtrar
    :type data: array
    :returns: Los datos filtrados
    :rtype: array
    """
    b, a = butter_highpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y
    
def windowing(data,framesize,cols,step):
    """
    Realiza el enventanado de los datos y que estos estén preparados para su filtrado.

    :param data: Datos resultantes de order_series
    :type data: array
    :param framesize: Tamaño de la ventana
    :type framesize: int
    :param cols: Número de ventanas totales
    :type cols: int
    :param step: Número de posiciones a mover en la ventana
    :type step: int
    :returns: Los datos enventanados
    :rtype: array
    """
    frame = []
    for win in range(cols):
        start = step*win
        stop = start+framesize
        frame.append(data[start:stop])
    return frame

def hamm_energ(sig, frameSize, overlapFac=0.75, window=np.hamming):
    """
    Aplica un enventanado de Hamming a cada una de las ventanas resultantes de windowing 
    y calcula la energía de cada ventana en cada variable de los datos. Entrega los cálculos de energía
    de cada variable, la energía total por variable y los datos enventadados con la ventana de Hamming.

    :param sig: datos enventanados
    :type sig: array
    :param overlapFac: solapamiento de las ventanas
    :type overlapFac: float
    :param window: tipo de ventana que será aplicada a los datos
    :type window: np object
    :param framesize: Tamaño de la ventana
    :type framesize: int
    :returns: Energía total por cada variable, datos enventanados aplicando la ventana de hamming, Energías por ventada en cada variable
    :rtype: dataframe, array, dataframe
    """
    energyT_vect = {}
    hamming_sig = {}
    energyF_vect = {}
    win = window(frameSize + 1)[:-1]
    hopSize = int(frameSize - np.floor(overlapFac * frameSize))
    for k,v in sig.items():
        
        samplesA = np.array(v['Active'])    
        samplesR = np.array(v['Reactive'])
        samplesP = np.array(v['Penalty'])

        framesA = util.view_as_windows(samplesA, window_shape=(frameSize,), step=hopSize)
        framesR = util.view_as_windows(samplesR, window_shape=(frameSize,), step=hopSize)
        framesP = util.view_as_windows(samplesP, window_shape=(frameSize,), step=hopSize)
        enerA = []
        enerB = []
        enerC = []
        for A,B,C in zip(framesA,framesR,framesP):
            enerA.append(np.sum(abs(A)**2))
            enerB.append(np.sum(abs(B)**2))
            enerC.append(np.sum(abs(C)**2))
        energyframe = {'energframe_Act': enerA, 'energframe_React': enerB, 'energframe_Pen': enerC}
        energyF_vect[k] = pd.DataFrame(energyframe)
        
        
        framesA = framesA * win
        framesR = framesR * win
        framesP = framesP * win
        
        energyVect = {'energ_Act': np.sum(abs(v['Active'])**2), 'energ_React': np.sum(abs(v['Reactive'])**2), 'energ_Pen': np.sum(abs(v['Penalty'])**2)}
        energyT_vect[k] = pd.DataFrame(energyVect, index=[0])
        framesVect = pd.DataFrame(list(zip(framesA, framesR, framesP)))
        framesVect.columns = ['hamm_Act', 'hamm_React', 'hamm_Pen']
        hamming_sig[k] = framesVect
        
        
    return energyT_vect, hamming_sig, energyF_vect

def filtBankAndEnergyWindow(signal):
    """
    Filtra los datos enventanados resultantes de hamm_energ.

    :param signal: datos de entrada 
    :type signal: array
    :returns: La energía por ventana por banda luego de ser filtrados los datos y los datos filtrados por banda
    :rtype: dataframe, dataframne
    """
    
    fs = 2/3600
    f = 1/3600
    nfilter = np.array([1/8,2/8,3/8,4/8,5/8,6/8,7/8])*f
    energyWindow = {}
    bankfilR = {}
    for k,v in signal.items():
        energyw = {}
        f0 = []
        f1 = []
        f2 = []
        f3 = []
        f4 = []
        f5 = []
        f6 = []
        f7 = []
        col1 = []
        col2 = []
        col3 = []
        col4 = []
        col5 = []
        col6 = []
        col7 = []
        col8 = []
        for window in v['hamm_Act']:
            #initial lowpass
            pl1 = butter_lowpass_filter(window,cutoff=nfilter[0], fs=fs, order=6)
            col1.append(np.sum(abs(pl1)**2))
            f0.append(pl1)
            pb1 = butter_bandpass_filter(window,cutoff=[nfilter[0],nfilter[1]], fs=fs, order=6)
            col2.append(np.sum(abs(pb1)**2))
            f1.append(pb1)
            pb2 = butter_bandpass_filter(window,cutoff=[nfilter[1],nfilter[2]], fs=fs, order=6)
            col3.append(np.sum(abs(pb2)**2))
            f2.append(pb2)
            pb3 = butter_bandpass_filter(window,cutoff=[nfilter[2],nfilter[3]], fs=fs, order=6)
            col4.append(np.sum(abs(pb3)**2))
            f3.append(pb3)
            pb4 = butter_bandpass_filter(window,cutoff=[nfilter[3],nfilter[4]], fs=fs, order=6)
            col5.append(np.sum(abs(pb4)**2))
            f4.append(pb4)
            pb5 = butter_bandpass_filter(window,cutoff=[nfilter[4],nfilter[5]], fs=fs, order=6)
            col6.append(np.sum(abs(pb5)**2))
            f5.append(pb5)
            pb6 = butter_bandpass_filter(window,cutoff=[nfilter[5],nfilter[6]], fs=fs, order=6)
            col7.append(np.sum(abs(pb6)**2))
            f6.append(pb6)
            ph1 = butter_highpass_filter(window,cutoff=nfilter[6], fs=fs, order=6)
            col8.append(np.sum(abs(ph1)**2))
            f7.append(ph1)
            
        energyw = {'fil_1':col1, 'fil_2':col2, 'fil_3':col3, 'fil_4': col4, 'fil_5':col5, 'fil_6':col6, 'fil_7':col7, 'fil_8':col8}
        energyWindow[k] = pd.DataFrame(energyw)
        windowsFiltered = {'fil_1':f0, 'fil_2':f1, 'fil_3':f2, 'fil_4': f3, 'fil_5':f4, 'fil_6':f5, 'fil_7':f6, 'fil_8':f7}
        bankfilR[k] = pd.DataFrame(windowsFiltered)
    return energyWindow, bankfilR

def normalizeWindowEnergy(frames):
    """
    Normaliza la energía por ventana por banda

    :param signal: Array con los datos de energía por ventana por banda
    :type signal: dataframe
    :returns: Energías normalizados por banda
    :rtype: dataframe
    """
    frten = {}
    for k,v in frames.items():
        
        enerbyframe = [0,0,0,0,0,0,0,0]
        for index, row in v.iterrows():
            ef1 = np.sum(abs(row['fil_1'])**2)
            ef2 = np.sum(abs(row['fil_2'])**2)
            ef3 = np.sum(abs(row['fil_3'])**2)
            ef4 = np.sum(abs(row['fil_4'])**2)
            ef5 = np.sum(abs(row['fil_5'])**2)
            ef6 = np.sum(abs(row['fil_6'])**2)
            ef7 = np.sum(abs(row['fil_7'])**2)
            ef8 = np.sum(abs(row['fil_8'])**2)
            enerbyframe = np.vstack([enerbyframe,[ef1,ef2,ef3,ef4,ef5,ef6,ef7,ef8]])
        enerbyframe = np.delete(enerbyframe, (0), axis=0)
        energyByBand = [[sum(x) for x in zip(*enerbyframe)]]
        
        frten[k] = pd.DataFrame((enerbyframe/energyByBand))
        frten[k].columns = ['ebn1','ebn2','ebn3','ebn4','ebn5','ebn6','ebn7','ebn8']
    return frten

def frecFeatureExtraction(data):
    """
    Genera los grupos de características que serán utilizados para los modelos de ML.

    :param data: Datos filtrados por banda
    :type data: dataframe
    :returns: Grupos de características
    :rtype: dataframe 
    """
    featuresT = {}
    features = []
    for k, v in data.items():
        media = v.mean(axis=0)
        maximo = v.max(axis=0)
        minimo = v.min(axis=0)
        cuartil1 = v.quantile(0.25,axis=0)
        cuartil2 = v.quantile(0.5,axis=0)
        cuartil3 = v.quantile(0.75,axis=0)
        cuartil4 = v.quantile(1,axis=0)
        curtosis = v.kurtosis(axis=0)
        asimetria = v.skew(axis=0)
        devstand = v.std(axis=0)
        features.append([k,minimo[0],minimo[1],minimo[2],minimo[3],minimo[4],minimo[5],minimo[6],minimo[7],
                    maximo[0],maximo[1],maximo[2],maximo[3],maximo[4],maximo[5],maximo[6],maximo[7],
                    media[0],media[1],media[2],media[3],media[4],media[5],media[6],media[7],
                    devstand[0],devstand[1],devstand[2],devstand[3],devstand[4],devstand[5],devstand[6],devstand[7],
                    cuartil1[0],cuartil1[1],cuartil1[2],cuartil1[3],cuartil1[4],cuartil1[5],cuartil1[6],cuartil1[7],
                    cuartil2[0],cuartil2[1],cuartil2[2],cuartil2[3],cuartil2[4],cuartil2[5],cuartil2[6],cuartil2[7],
                    cuartil3[0],cuartil3[1],cuartil3[2],cuartil3[3],cuartil3[4],cuartil3[5],cuartil3[6],cuartil3[7],
                    cuartil4[0],cuartil4[1],cuartil4[2],cuartil4[3],cuartil4[4],cuartil4[5],cuartil4[6],cuartil4[7],
                    curtosis[0],curtosis[1],curtosis[2],curtosis[3],curtosis[4],curtosis[5],curtosis[6],curtosis[7],
                    asimetria[0],asimetria[1],asimetria[2],asimetria[3],asimetria[4],asimetria[5],asimetria[6],asimetria[7]])
    featuresT = pd.DataFrame(features)
    featuresT.columns = ['Meter','minband1','minband2','minband3','minband4','minband5','minband6','minband7','minband8',
                    'maxband1','maxband2','maxband3','maxband4','maxband5','maxband6','maxband7','maxband8',
                    'meanband1','meanband2','meanband3','meanband4','meanband5','meanband6','meanband7','meanband8',
                    'stdband1','stdband2','stdband3','stdband4','stdband5','stdband6','stdband7','stdband8',
                    'quantile_1_band1','quantile_1_band2','quantile_1_band3','quantile_1_band4','quantile_1_band5','quantile_1_band6','quantile_1_band7','quantile_1_band8',
                    'quantile_2_band1','quantile_2_band2','quantile_2_band3','quantile_2_band4','quantile_2_band5','quantile_2_band6','quantile_2_band7','quantile_2_band8',
                    'quantile_3_band1','quantile_3_band2','quantile_3_band3','quantile_3_band4','quantile_3_band5','quantile_3_band6','quantile_3_band7','quantile_3_band8',
                    'quantile_4_band1','quantile_4_band2','quantile_4_band3','quantile_4_band4','quantile_4_band5','quantile_4_band6','quantile_4_band7','quantile_4_band8',
                    'kurtosisband1','kurtosisband2','kurtosisband3','kurtosisband4','kurtosisband5','kurtosisband6','kurtosisband7','kurtosisband8',
                    'skewband1','skewband2','skewband3','skewband4','skewband5','skewband6','skewband7','skewband8']
    return featuresT

def k_fold_custom(features,labels,folds = 10, porcentage_test=0.3):
    k_list = []
    y_index_by_label = {}
    n_train = int(np.floor((1-porcentage_test) * len(features)))
    n_test = int(len(features) - n_train)
    element_by_class =  int(np.floor(n_test / len(labels.unique())))
    if element_by_class > min(list(Counter(labels).values())):
        element_by_class = min(list(Counter(labels).values()))
        n_train = len(features)-(element_by_class*len(labels.unique()))
        n_test = int(len(features) - n_train)
    for j in labels.unique():
        y_index_by_label[j] = labels == j
    
    for k in range(folds):
        index_test = []
        index_train = []
        for l in labels.unique():
            label_i = [i for i, x in enumerate(y_index_by_label[l]) if x]
            index_test.append(random.sample(label_i, element_by_class))
        index_test = list(itertools.chain.from_iterable(index_test))
        index_train = np.ones(len(features), np.bool)
        index_train[index_test] = 0
        index_train = list(labels[index_train].index)
        k_list.append([index_train,index_test])
    return k_list

def stratified_group_k_fold(X, y, groups, k, seed=None):
    labels_num = np.max(y) + 1
    y_counts_per_group = defaultdict(lambda: np.zeros(labels_num))
    y_distr = Counter()
    for label, g in zip(y, groups):
        y_counts_per_group[g][label] += 1
        y_distr[label] += 1

    y_counts_per_fold = defaultdict(lambda: np.zeros(labels_num))
    groups_per_fold = defaultdict(set)

    def eval_y_counts_per_fold(y_counts, fold):
        y_counts_per_fold[fold] += y_counts
        std_per_label = []
        for label in range(labels_num):
            label_std = np.std([y_counts_per_fold[i][label] / y_distr[label] for i in range(k)])
            std_per_label.append(label_std)
        y_counts_per_fold[fold] -= y_counts
        return np.mean(std_per_label)
    
    groups_and_y_counts = list(y_counts_per_group.items())
    random.Random(seed).shuffle(groups_and_y_counts)

    for g, y_counts in sorted(groups_and_y_counts, key=lambda x: -np.std(x[1])):
        best_fold = None
        min_eval = None
        for i in range(k):
            fold_eval = eval_y_counts_per_fold(y_counts, i)
            if min_eval is None or fold_eval < min_eval:
                min_eval = fold_eval
                best_fold = i
        y_counts_per_fold[best_fold] += y_counts
        groups_per_fold[best_fold].add(g)

    all_groups = set(groups)
    for i in range(k):
        train_groups = all_groups - groups_per_fold[i]
        test_groups = groups_per_fold[i]

        train_indices = [i for i, g in enumerate(groups) if g in train_groups]
        test_indices = [i for i, g in enumerate(groups) if g in test_groups]

        yield train_indices, test_indices

def parseLabelFile(df):
    labelFile = {}
    for k in df.keys():
        labelFile[k] = df[k].tolist()
    return labelFile
