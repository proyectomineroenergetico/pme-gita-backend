import datetime

import jwt
from pme_app.domain.entities.blackListTokenModel import BlacklistToken
from shared import logger, secret_key


def encode_recovery_token(email):
    """
    Genera el token de recuperación.

    :param email: e-mail del usuario
    :type email: str
    :returns: Token de recuperación del usuario
    :rtype: str
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=10,seconds=0),
            'iat': datetime.datetime.utcnow(),
            'e-mail': email
        }
        return jwt.encode(
            payload,
            secret_key,
            algorithm='HS256'
        )
    except Exception as e:
        return e

def decode_recovery_token(token):
    """
    Valida el token de recuperación.

    :param token: Token de autenticación
    :type token: str
    :return: Payload con el id del usuario
    :rtype: int|str
    """
    try:
        payload = jwt.decode(token,secret_key)
        logger.info(payload)
        is_blacklisted_token = BlacklistToken.check_blacklist(token)
        if is_blacklisted_token:
            return [400,'Token blacklisted. Please try in again.']
        else:
            return [200,payload['e-mail']]
    except jwt.ExpiredSignatureError:
        return [400,'Signature expired. Please try in again.']
    except jwt.InvalidTokenError:
        return [400,'Invalid token. Please try in again.']

