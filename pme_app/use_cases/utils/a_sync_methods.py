from datetime import datetime

from app import app
from pme_app.repository.db_services.histByMethodService import \
    saveHistByTechnique
from pme_app.use_cases.utils.run_tech import *
from shared import SSE, data_files, logger


def frecuency_methods(sse,featurespath,user_id,selectedBands,meters,cp,do,technique):
    """
    Metodo que agrupa las técnicas de Ml que dependen de características 
    en frecuencia tanto para entrenamiento como para predicción (ejecución en background).

    :param featurespath: Path donde están o se almacenarán las características
    :type featurespath: str
    :param selectedBands: Bandas seleccionadas para trabajar con la técnica de ML
    :type selectedBands: bool list
    :param meters: lista de medidores
    :type meters: list
    :param cp: clase de problema seleccionado
    :type cp: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param sse: booleano que indica si se utilizarán los sse
    :type sse: bool
    :param do: Acción a realizar (entrenar y predecir, entrenar) 
    :type do: str
    :param technique: técnica de ML seleccionada
    :type technique: str
    """
    with app.app_context():
        logger.info("frec features")
        if do == 'predict':
            if technique == "K-MEANS":  
                name = f"{technique}_PREDICT_{user_id}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
            else:
                name = f"{technique}_PREDICT_{user_id}_{cp}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        else:
            if technique == "K-MEANS":  
                name = f"{technique}_FIT_{user_id}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
            else:
                name = f"{technique}_FIT_{user_id}_{cp}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        featuresByBand,selbandind,features = frec_feat(featurespath=featurespath,user_id=user_id,
                                                        selectedBands=selectedBands,technique=technique,do=do,cp=cp,name=name,sse=sse)
        if  technique == "K-MEANS":
            logger.info("kmenas")
            if do == 'fit_and_predict':
                SSE.publish({'message':f'Iniciando la técnica {technique}.',
                        "id": user_id,"status": 40, "model_name":name,"date":datetime.now()}, type='running.method')
                saveHistByTechnique(modelName=name,msg_a=f'Iniciando la técnica {technique}.',user_id=user_id, status_a=40,date=datetime.now())
                logger.info("fit_and_predict")
                kmeans_tech_fit_predict(selbandind,featuresByBand,meters,user_id,name)
            elif do == 'predict':
                SSE.publish({'message':f'Iniciando la técnica {technique}.',
                        "id": user_id,"status": 40, "model_name":name,"date":datetime.now()}, type='running.method')
                saveHistByTechnique(modelName=name,msg_a=f'Iniciando la técnica {technique}.',user_id=user_id, status_a=40,date=datetime.now())
                logger.info('predict')
                kmeans_tech_predict(selbandind,featuresByBand,meters,user_id,name)
        elif  technique == "LINEAR-SVM":  
            label_path = getFilesByUser(user_id=user_id,labels_path=True)
            if label_path[0] == 400:
                SSE.publish({'message':f'Algunos problemas ocurrieron obteniendo las etiquetas, por favor cargue las etiquetas de nuevo. {label_path[1]}',
                            "id": user_id,"model_name":name,"date":datetime.now()}, type='error')
                saveHistByTechnique(modelName=name,msg_a=f'Algunos problemas ocurrieron obteniendo las etiquetas, por favor cargue las etiquetas de nuevo. {label_path[1]}',
                                user_id=user_id, status_a=40,date=datetime.now())
            lp = labelsProblem(labels_path=label_path[1],problem_type=cp)
            labels_by_problem = lp.getLabels()
            n,classByLabel = lp.numClassByProblem()  
            if do == 'fit_and_predict':
                SSE.publish({'message':f'Iniciando la técnica {technique}.',
                        "id": user_id,"status": 40, "model_name":name,"date":datetime.now()}, type='running.method')
                saveHistByTechnique(modelName=name,msg_a=f'Iniciando la técnica {technique}.',user_id=user_id, status_a=40,date=datetime.now())
                
                if labels_by_problem[0] == 400:
                    SSE.publish({'message':f'Algunos problemas ocurrieron generando las etiquetas por problema: {labels_by_problem[1]}',
                                "id": user_id,"model_name":name,"date":datetime.now()}, type='error')
                    saveHistByTechnique(modelName=name,msg_a=f'Algunos problemas ocurrieron generando las etiquetas por problema: {labels_by_problem[1]}',
                                    user_id=user_id, status_a=40,date=datetime.now())
                lsvm_tech_fit_predict(featuresByBand,selbandind,user_id,labels_by_problem[1],features['Meter'],cp,name,classByLabel)
            elif do == 'predict':
                SSE.publish({'message':f'Iniciando la técnica {technique}.',
                        "id": user_id,"status": 40, "model_name":name,"date":datetime.now()}, type='running.method')
                saveHistByTechnique(modelName=name,msg_a=f'Iniciando la técnica {technique}.',
                                    user_id=user_id, status_a=40,date=datetime.now())
                lsvm_tech_predict(featuresByBand,selbandind,user_id,features['Meter'],cp,name,classByLabel)
        else:
            SSE.publish({'message':f'La técnica de ML seleccionada no es reconocida. Esta debe ser K-MEANS o SVM-LINEAR en vez de  {technique}.',
                        "id": user_id,"date":datetime.now(),"model_name":name}, type='error')
            saveHistByTechnique(modelName=name,msg_a=f'La técnica de ML seleccionada no es reconocida. Esta debe ser K-MEANS o SVM-LINEAR en vez de  {technique}.',
                                    user_id=user_id, status_a=20,date=datetime.now())

def time_method(sse,featurespath,user_id,dataPath,label_path,cp,technique,do):
    """
    Metodo que agrupa las técnicas de Ml que dependen de características 
    temporales tanto para entrenamiento como para predicción (ejecución en background).

    :param featurespath: Path donde están o se almacenarán las características
    :type featurespath: str
    :param cp: clase de problema seleccionado
    :type cp: str
    :param label_path: Path donde están las etiquetas
    :type label_path: str
    :param dataPath: Path donde están los datos a usar
    :type dataPath: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param sse: booleano que indica si se utilizarán los sse
    :type sse: bool
    :param do: Acción a realizar (entrenar y predecir, entrenar) 
    :type do: str
    :param technique: técnica de ML seleccionada
    :type technique: str
    """
    with app.app_context():
        if do == 'predict':
            name = f"{technique}_PREDICT_{user_id}_{cp}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        else:
            name = f"{technique}_FIT_{user_id}_{cp}" + datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        featuresAndlabels = time_feat(featurespath=featurespath,user_id=user_id,dataPath=dataPath,label_path=label_path,sse=sse,technique=technique,do=do,cp=cp,name=name)
        if technique == "RANDOM-FOREST":
            logger.info('rr')
            lp = labelsProblem(labels_path=label_path[1],problem_type=cp)
            labels_by_problem = lp.getLabels()
            n,classByLabel = lp.numClassByProblem()  
            if do == 'fit_and_predict':
                SSE.publish({'message':'Iniciando la técnica RANDOM-FOREST',
                            "id": user_id,"status": 40,"model_name":name,"date":datetime.now()}, type='running.method')
                saveHistByTechnique(modelName=name,msg_a='Iniciando la técnica RANDOM-FOREST',
                                    user_id=user_id, status_a=40,date=datetime.now())
                logger.info('fit predict')
                rf_tech_fit_predict(featuresAndlabels=featuresAndlabels,user_id=user_id,cp=cp,name=name,classByLabel=classByLabel)
            elif do == 'predict':
                SSE.publish({'message':'Iniciando la técnica RANDOM-FOREST',
                            "id": user_id,"status": 40,"model_name":name,"date":datetime.now()}, type='running.method')
                saveHistByTechnique(modelName=name,msg_a='Iniciando la técnica RANDOM-FOREST',
                                    user_id=user_id, status_a=40,date=datetime.now())
                logger.info('predict')
                modelPath = getFilesByUser(user_id=user_id,rf_path=True,cp=cp)
                if modelPath[0] == 400:
                    SSE.publish({'message':f'Algunos problemas ocurrieron obteniendo el model path, pro favior ejecute de nuevo entrenar y clasificar. {modelPath[1]}',
                            "id": user_id,"model_name":name,"date":datetime.now()}, type='error')
                    saveHistByTechnique(modelName=name,
                                    msg_a=f'Algunos problemas ocurrieron obteniendo el model path, pro favior ejecute de nuevo entrenar y clasificar. {modelPath[1]}',
                                    user_id=user_id, status_a=40,date=datetime.now())
                rf_tech_predict(features=featuresAndlabels,modelPath=modelPath[1],user_id=user_id,cp=cp,name=name,classByLabel=classByLabel)
            logger.info('results')
        else:
            SSE.publish({'message':f'La técnica de ML seleccionada no es reconocida. Esta debe ser  RANDOM-FOREST o NEURAL-NETWORK en vez de {technique}.',
                "id": user_id,"date":datetime.now(),"model_name":name}, type='error')
            saveHistByTechnique(modelName=name,
                                msg_a=f'La técnica de ML seleccionada no es reconocida. Esta debe ser  RANDOM-FOREST o NEURAL-NETWORK en vez de {technique}.',
                                user_id=user_id, status_a=20,date=datetime.now())

