from flask_mail import Message
from pme_app.use_cases.utils.recoveryToken import encode_recovery_token
from shared import app_port, host, logger, mail


def sendEmail(email):
    """
    Envía un correo de recuperación con un enlace para acceder y realizar el cambio de contraseña

    :param email: correo del usuario 
    :type email: str
    :returns: Estado de la transacción
    :rtype: int
    """
    token = encode_recovery_token(email)
    url = f"{host}:{app_port}/reset_password?user={token.decode()}"
    # logger.info(f"recovery url -> {url}")
    msg = Message(subject='Recovery Password PME_APP',recipients = [email])
    msg.html = f"""<html>
                <head>
                    <title></title>
                    <link href="https://svc.webspellchecker.net/spellcheck31/lf/scayt3/ckscayt/css/wsc.css" rel="stylesheet" type="text/css" />
                </head>
                <body data-gr-ext-installed="" data-new-gr-c-s-check-loaded="14.1001.0" data-new-gr-c-s-loaded="14.1001.0">Hola, <span style="color:#0000FF">{email}</span>:<br />
                <br />
                Haz clic en el siguiente enlace para continuar con el proceso de recuperaci&oacute;n&nbsp;de contrase&ntilde;a:<br />
                <span style="color:#0000FF">{url}</span><br />
                <br />
                Si no fuiste t&uacute;, ignora este mensaje.<br />
                Gracias.<br />
                <br />
                El equipo de seguridad de PME APP<br />
                &nbsp;</body>
                </html>"""
    mail.send(msg)
    return 200
