import numpy as np
import pandas as pd
import scipy.stats as st
from six.moves import cPickle as pickle
from sklearn.metrics import r2_score
from statsmodels.tsa.ar_model import AutoReg


class FeatureExtraction:

    """
    Extracción de características para el método de Random Forest en el dominio del tiempo.

    :param data1: Los datos preprocesados despues de ejecutar read_preprocess.py.
    :type: dataframe
    :param featuresPath: Path donde se almacenarán las características.
    :type featuresPath: str
    :param labelsPath: Path donde están almacenadas las etiquetas.
    :type labelsPath: str
    """

    def __init__(self, data1, featurePath,labelsPath):

        self.Data1= data1
        self.features_path = featurePath
        self.labelsfile= labelsPath

    def extract_features_data1(self, dataf):
        """
        Extracción de características teniendo en cuenta cada límite 
        económico en todos los sensores (se extraerán 144 características).
        
        :param dataf: Dataframe con los datos.
        :type data: dataframe
        :returns: Características extraídas.
        :rtype: dataframe 
        """
        with open(dataf, 'rb') as f:
            data = pickle.load(f)

        df={"Frontera": [], "year_month": []}

        feat_names=["avg", "std", "skewness", "kurtosis", "max", "min", "argmax", "argmin", "a0", "a1", "a2", "a3", "a4", "zcRxx", "T", "r2_AR", "ar0", "ar1", "ar2", "ar3", "ar4", "ar5", "ar6", "ar7"]
        sequences=["Avg Act", "Avg Reac", "Avg Penal.", "Std Act", "Std Reac", "Std Penal."]
        names=[]
        for f in sequences:
            for s in feat_names:
                df[f+" " +s]=[]
                names.append(f+" " +s)
        df["Frontera"]=data["Frontera"]
        df["year_month"]=data["year_month"]
        for n in range(len(data["series"])):
            series=data["series"][n] # matrix in 72,30
            avgact=np.mean(series[0:24,:], 0) # vector in 1,30
            avgreac=np.mean(series[24:48,:], 0) # vector in 1,30
            avgpen=np.mean(series[48:,:], 0) # vector in 1,30
            stdact=np.std(series[0:24,:], 0) # vector in 1,30
            stdreac=np.std(series[24:48,:], 0) # vector in 1,30
            stdpen=np.std(series[48:,:], 0) # vector in 1,30
            list_signals=[avgact, avgreac, avgpen, stdact, stdreac, stdpen]

            feat_vec=[]
            for e, signal in enumerate(list_signals):

                a=np.polyfit(np.arange(len(signal)), signal, 5)
                
                signalm=signal-np.mean(signal)
                if np.std(signalm)>0:
                    signalm=signalm/np.std(signalm)
                Rxx=np.correlate(signalm, signalm, mode="full")
                Rxx=Rxx[np.argmax(Rxx):]
                for er in np.arange(1,len(Rxx)):
                    if ((Rxx[er]<0) and (Rxx[er-1]>=0)):
                        break
                rxx2=Rxx[er:]
                posMax=np.argmax(rxx2)
                if rxx2[posMax]>0:
                    T=posMax+er
                else:
                    T=len(Rxx)

                signalm[np.isnan(signalm)]=0
                train_data = signalm[0:len(signalm)-7]
                test_data = signalm[len(signalm)-7:]
                model = AutoReg(train_data, 7)
                model_fitted = model.fit()
                ar=model_fitted.params

                predictions = model_fitted.predict(
                                start=len(train_data), 
                                end=len(train_data) + len(test_data)-1, 
                                dynamic=False)

                r2 = r2_score(test_data, predictions)
                feat_vec.append([np.mean(signal), np.std(signal), st.skew(signal), st.kurtosis(signal), 
                                np.max(signal), np.min(signal), np.argmax(signal), np.argmin(signal), 
                                a[0], a[1], a[2], a[3], a[4], er, T, r2, ar[0], ar[1], ar[2], ar[3], ar[4], ar[5], ar[6],ar[7]])
            feat_vec=np.hstack(feat_vec)
            for e, k in enumerate(names):
                df[k].append(feat_vec[e])

        df=pd.DataFrame(df)
        return df

    def get_labels(self,data_feat,labels_path,file_feat=''):
        """
        Agrega a las caracteristicas ya extraídas las etiquetas.
        
        :param data_feat: Características.
        :type data_feat: dataframe
        :param labels_path: Path donde están almacenadas las etiquetas.
        :type labels_path: str
        :param file_feat: Path donde será almacenado dichas características con las etiquetas.
        :type file_feat: str
        :returns: las caracteristicas y las etiquetas.
        :rtype: dataframe
        """

        if labels_path.endswith('.csv'):
            df_labels = pd.read_csv(labels_path)
        elif labels_path.endswith('.xlsx'):
            df_labels = pd.read_excel(labels_path)
        
        label_planta=[]
        label_planta2=[]
        label_planta3=[]
        label_tension=[]
        for k in data_feat["Frontera"]: 
            row_label=df_labels[df_labels["Frontera Proyecto"]==k]

            label_planta.append(row_label["Planta"].values)
            label_planta2.append(row_label["Planta2"].values)
            label_planta3.append(row_label["Planta3"].values)

            label_tension.append(row_label["Nivel Tension"].values)

        data_feat["label_planta"]=np.hstack(label_planta)
        data_feat["label_planta2"]=np.hstack(label_planta2)
        data_feat["label_planta3"]=np.hstack(label_planta3)
        data_feat["label_tension"]=np.hstack(label_tension)
        
        if len(file_feat)>0:
            data_feat.to_csv(file_feat) 

        return data_feat

    def runProcess(self):
        """
        Realiza el proceso necesario para realizar la extracción de características.

        :returns: Las características extraídas y dichas características junto con las etiquetas
        :rtype: dataframe, dataframe
        """
        Features=self.extract_features_data1(dataf=self.Data1)    
        df_label=self.get_labels(Features, self.labelsfile,file_feat=self.features_path)

        return Features, df_label
