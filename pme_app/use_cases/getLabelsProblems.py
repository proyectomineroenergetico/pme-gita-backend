import pandas as pd
from shared import logger


class labelsProblem():
    """
    Genera las clases de problemas que pueden 
    ser utilizados en los métodos supervisados.

    :param labels_path: Path donde están almacenadas las etiquetas
    :type labels_path: str
    :param problem_type: Clase de problema seleccionado [label_planta: {Extracción, Transporte, Biocombustible, Edificio administrativo, Refinería},
    label_planta2: {Transporte, Otros}, label_planta3: {Transporte. Extracción}, label_tension: {1, 2, 3, 4, 5}, label_tension2: {2,3-4,5}]
    :type labels_path: str
    
    """

    def __init__(self,labels_path, problem_type):
        self.labels_path = labels_path
        self.problem_type = problem_type

    def getLabels(self):
        """
        Genera las etiquetas según la clase de problema seleccionado

        :returns: Las etiquetas
        :rtype: dataframe
        """
        if self.labels_path.endswith('.csv'):
            df = pd.read_csv(self.labels_path)
        elif self.labels_path.endswith('.xlsx'):
            df = pd.read_excel(self.labels_path)
        try:
            if self.problem_type == "label_planta":
                new_label = []
                for frt, i in zip(df['Frontera Proyecto'],df['Tipo de planta']):
                    if i == 'Extracción':
                        new_label.append(1)
                    elif i == 'Transporte':
                        new_label.append(2)
                    elif i == 'Biocombustible':
                        new_label.append(3)
                    elif i == 'Edificio administrativo':
                        new_label.append(4)
                    elif i == 'Refinería':
                        new_label.append(5)
                    else:
                        new_label.append(0)
                new_label = pd.DataFrame({'frontera':df['Frontera Proyecto'],'label':new_label})
            elif self.problem_type == "label_planta2":
                new_label = []
                for frt, i in zip(df['Frontera Proyecto'],df['Tipo de planta']):
                    if i == 'Transporte':
                        new_label.append(1)
                    elif i == 'Extracción' or  i == 'Biocombustible' or i == 'Edificio administrativo' or i == 'Refinería':
                        new_label.append(2)
                    else:
                        new_label.append(0)
                new_label = pd.DataFrame({'frontera':df['Frontera Proyecto'],'label':new_label})         
            elif self.problem_type == "label_planta3":
                new_label = []
                for frt, i in zip(df['Frontera Proyecto'],df['Tipo de planta']):
                    if i == 'Extracción':
                        new_label.append(1)
                    elif i == 'Transporte':
                        new_label.append(2)
                    else:
                        new_label.append(0)
                new_label = pd.DataFrame({'frontera':df['Frontera Proyecto'],'label':new_label})
            elif self.problem_type == "label_tension":
                new_label = []
                for frt, i in zip(df['Frontera Proyecto'],df['Nivel Tension']):
                    if i == 1:
                        new_label.append(1)
                    elif i == 2:
                        new_label.append(2)
                    elif i == 3:
                        new_label.append(3)
                    elif i == 4:
                        new_label.append(4)
                    elif i == 5:
                        new_label.append(5)
                    else:
                        new_label.append(0)
                new_label = pd.DataFrame({'frontera':df['Frontera Proyecto'],'label':new_label})
            elif self.problem_type == "label_tension2":
                new_label = []
                for frt, i in zip(df['Frontera Proyecto'],df['Nivel Tension']):
                    if i == 2:
                        new_label.append(1)
                    elif i == 3 or i == 4:
                        new_label.append(2)
                    elif i == 5:
                        new_label.append(3)
                    else:
                        new_label.append(0)
                new_label = pd.DataFrame({'frontera':df['Frontera Proyecto'],'label':new_label})

            return [200,new_label]
        except Exception as e:
            logger.error(f'some error occurred creating the label y problem -> {e}')
            return [400,e]

    def numClassByProblem(self):
        """
        Entrega el número de etiquetas pos clase de problema y las correspondencias a dicho problema

        :returns: Número de etiquetas y dicha correspondencia
        :rtype: int, str
        """

        if self.problem_type == 'label_planta':
            nc = 5
            labels = "Extracción=1, Transporte=2, Biocombustible=3, Edificio administrativo=4, Refinería=5"
        elif self.problem_type == 'label_planta2':
            nc = 2
            labels = "Transporte=1, Otros=2"
        elif self.problem_type == 'label_planta3':
            nc = 2
            labels = "Transporte=1, Extracción=2"
        elif self.problem_type == 'label_tension':
            nc = 4
            labels = "Nuvel 1 =1, Nivel 2 =2 , Nivel 3 =3, Nivel 4 =4, Nivel 5 =5"
        elif self.problem_type == 'label_tension2':
            nc = 3
            labels = "Nivel 2 =1, Nivel 3 y 4 =2, Nivel 5 = 3"
        
        return nc,labels
