import numpy as np
import pandas as pd
import torch
from six.moves import cPickle as pickle


class FormTensors():
    """
    Crea un archivo con el tensor que será utilizado en la técnica Neural Network.

    :param len_seq: Tamaño de los datos de entrada (30 por un mes de análisis)
    :type len_seq: int
    :param file_data: Path de el archivo con los datos (archivo resultante de correr read_preprocess.py)
    :type file_data: str
    :param labels_data: Etiquetas
    :type labels_data: dataframe
    :param nseries: Número de medidores
    :type nseries: int
    """

    def __init__(self, len_seq, nseries, file_data,labels_data):

        self.Data1=file_data
        with open(self.Data1, 'rb') as f:
            self.data = pickle.load(f)
            self.labels_data = labels_data
        if self.labels_data !='':
            if self.labels_data.endswith('.csv'):
                df = pd.read_csv(labels_data)
            elif self.labels_data.endswith('.xlsx'):
                df = pd.read_excel(labels_data)
            self.df_labels=df
        self.len_seq=len_seq
        self.nseries=nseries

    def form_tensor(self, file_tensor):
        """
        Genera el tensor a partir de los datos de entrada para luego ingresarlos a la red neuronal.

        :param file_tensor: Path donde se almacenaá el tensor
        :type file_tensor: str
        """

        df={"Frontera": [], "year_month": [], "X":[], "y1":[], "y2":[], "y3":[], "y4":[]}
        df["X"]=torch.zeros((len(self.data["series"]), self.len_seq, self.nseries)).float()
        if self.labels_data != '':
            df["y1"]=torch.zeros((len(self.data["series"]))).long()
            df["y2"]=torch.zeros((len(self.data["series"]))).long()
            df["y3"]=torch.zeros((len(self.data["series"]))).long()
            df["y4"]=torch.zeros((len(self.data["series"]))).long()
        df["Frontera"]=self.data["Frontera"]
        df["year_month"]=self.data["year_month"]

        for n in range(len(self.data["series"])):
            series=self.data["series"][n] # matrix in 72,30

            me=np.nanmean(series, 1)
            if sum(sum(np.isnan(series)))>0:
                pnan=np.isnan(series)
                for m in range(len(me)):
                    series[m,pnan[m,:]]=me[m]

            seriesT=torch.from_numpy(series).float()

            seriesTP=seriesT.permute(1,0)

            df['X'][n,:,:]=seriesTP
            #condition
            if self.labels_data != '':
                row_label=self.df_labels[self.df_labels["Frontera Proyecto"]==df["Frontera"][n]]

                df['y1'][n]=torch.from_numpy(row_label["Planta"].values).long()
                df['y2'][n]=torch.from_numpy(row_label["Planta2"].values).long()
                df['y3'][n]=torch.from_numpy(row_label["Planta3"].values).long()
                df['y4'][n]=torch.from_numpy(row_label["Nivel Tension"].values).long()

        torch.save(df, file_tensor)

