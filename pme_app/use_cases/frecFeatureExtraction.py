from itertools import compress

import numpy as np
from pme_app.use_cases.utils.tools import (filtBankAndEnergyWindow,
                                           frecFeatureExtraction, hamm_energ,
                                           normalizeWindowEnergy)


class frecuencyFeatures():
    """
    Genera un conjunto de 11 grupos de características a partir 
    del filtrado de datos con ocho filtros en el dominio de la frecuencia. 
    Estos grupos son 
    {banda 1, banda 2, banda 3, banda 4, banda 5, banda 6, banda 7, banda 8, bandas 1-4, bandas 5-8, bandas 1-8} 
    """
    
    def featureExtraction(self,dataSeriesByFrt, frameSize=48, overlapFac=0.875):
        """
        Realiza el filtrado de los datos con los ocho filtros enventanado 
        los datos cada 48 muestras y con solapamiento de 6 muestras.

        :param dataSeries: Serie con los datos a filtrar para cada medidor
        :type dataSeries: dataframe
        :param frameSize: Tamaño de la ventana 
        :type frameSize: int
        :param overlapFac: porcentaje de solapamiento
        :type overlapFac: float
        :returns: Datos filtrados en las ocho bandas 
        :rtype: dataframe
        """

        energy_TotalS, hamm_vec, energy_FrammeS = hamm_energ(dataSeriesByFrt, frameSize=frameSize, overlapFac=overlapFac)

        enerwhf, framehf = filtBankAndEnergyWindow(hamm_vec) #applying the filter's bank

        framehfen = normalizeWindowEnergy(framehf) # Normalizing the energy by window

        features = frecFeatureExtraction(framehfen) #feature extraction 

        return features

    def featuresByBand(self,features,boolean):
        """
        Genera los grupos de características que serán utilizados en las técnicas K-Means y SVM

        :param features: Datos filtrados con los ocho filtros
        :type features: dataframe
        :param boolean: Bandas seleccionadas para el análisis con la técnica deseada
        :type boolean: bool list
        :returns: Bandas seleccionadas
        :rtype list
        """

        X1 = np.array(features[["minband1","maxband1","meanband1","stdband1","quantile_1_band1","quantile_2_band1",
        "quantile_3_band1","quantile_4_band1","kurtosisband1","skewband1"]])
        X2 = np.array(features[["minband2","maxband2","meanband2","stdband2","quantile_1_band2","quantile_2_band2",
        "quantile_3_band2","quantile_4_band2","kurtosisband2","skewband2"]])
        X3 = np.array(features[["minband3","maxband3","meanband3","stdband3","quantile_1_band3","quantile_2_band3",
        "quantile_3_band3","quantile_4_band3","kurtosisband3","skewband3"]])
        X4 = np.array(features[["minband4","maxband4","meanband4","stdband4","quantile_1_band4","quantile_2_band4",
        "quantile_3_band4","quantile_4_band4","kurtosisband4","skewband4"]])
        X5 = np.array(features[["minband5","maxband5","meanband5","stdband5","quantile_1_band5","quantile_2_band5",
        "quantile_3_band5","quantile_4_band5","kurtosisband5","skewband5"]])
        X6 = np.array(features[["minband6","maxband6","meanband6","stdband6","quantile_1_band6","quantile_2_band6",
        "quantile_3_band6","quantile_4_band6","kurtosisband6","skewband6"]])
        X7 = np.array(features[["minband7","maxband7","meanband7","stdband7","quantile_1_band7","quantile_2_band7",
        "quantile_3_band7","quantile_4_band7","kurtosisband7","skewband7"]])
        X8 = np.array(features[["minband8","maxband8","meanband8","stdband8","quantile_1_band8","quantile_2_band8",
        "quantile_3_band8","quantile_4_band8","kurtosisband8","skewband8"]])
        X1_4 = np.array(features[["minband1","maxband1","meanband1","stdband1","quantile_1_band1","quantile_2_band1",
        "quantile_3_band1","quantile_4_band1","kurtosisband1","skewband1","minband2","maxband2","meanband2","stdband2",
        "quantile_1_band2","quantile_2_band2","quantile_3_band2","quantile_4_band2","kurtosisband2","skewband2","minband3",
        "maxband3","meanband3","stdband3","quantile_1_band3","quantile_2_band3","quantile_3_band3","quantile_4_band3",
        "kurtosisband3","skewband3","minband4","maxband4","meanband4","stdband4","quantile_1_band4","quantile_2_band4",
        "quantile_3_band4","quantile_4_band4","kurtosisband4","skewband4"]])
        X5_8 = np.array(features[["minband5","maxband5","meanband5","stdband5","quantile_1_band5","quantile_2_band5",
        "quantile_3_band5","quantile_4_band5","kurtosisband5","skewband5","minband6","maxband6","meanband6","stdband6",
        "quantile_1_band6","quantile_2_band6","quantile_3_band6","quantile_4_band6","kurtosisband6","skewband6","minband7",
        "maxband7","meanband7","stdband7","quantile_1_band7","quantile_2_band7","quantile_3_band7","quantile_4_band7",
        "kurtosisband7","skewband7","minband8","maxband8","meanband8","stdband8","quantile_1_band8","quantile_2_band8",
        "quantile_3_band8","quantile_4_band8","kurtosisband8","skewband8"]])
        Xt = np.array(features.loc[:, features.columns != "Meter"])
        data = [X1,X2,X3,X4,X5,X6,X7,X8,X1_4,X5_8,Xt]
        data = list(compress(data, list(boolean)))
        return data
