import random

import numpy as np
from pme_app.use_cases.utils.tools import stratified_group_k_fold
from scipy.stats import randint
from shared import logger
from six.moves import cPickle as pickle
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.manifold import TSNE
from sklearn.metrics import (accuracy_score, balanced_accuracy_score,
                             classification_report, f1_score)
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import StandardScaler


class Classifier:

    """
    Clasificador basado en Random Forest para modelar características temporales.

    :param feature_set: Características en el domino del tiempo.
    :type feature_set: dataframe
    :param class_problem: Clase de problema seleccionado 
                          label_planta: 5 clases (tipo de planta)
                          label_planta2: transporte vs. otros
                          label_planta3: transporte vs. extracción
                          label_tension: nivel de tensión (2-3-4-5).
    :type class_problem: str
    :param model_path: Path para almacenar el modelo después de ser entrenado.
    :type model_path: str
    :param classifier: Tipo de clasificador (por el momento solo se soporta RF)
    :type classifier: str
    :param applyPCA: Booleano para determinar si se aplica o no PCA a los datos.
    :type applyPCA: bool

    """


    def __init__(self, feature_set, class_problem, model_path, classifier='RF', applyPCA=False):

        self.X, self.ID, self.sesion, self.y= self.read_feat_mat(feature_set, class_problem)

        self.X[np.isinf(self.X)]=0
        self.X[np.isnan(self.X)]=0
        self.class_problem=class_problem
        self.model_path=model_path
        self.applyPCA=applyPCA
        self.classifier=classifier

        if (self.classifier!="RF"):
            raise ValueError("classifier"+ classifier+" is not supported" )


    def read_feat_mat(self, file_feat, class_problem):

        """
        Lee las características con las etiquetas y segmenta los 
        datos para su correcto uso en el clasificador.

        :param file_feat: Características temporales.
        :type file_feat: dataframe
        :param class_problem: clase de problema.
                            label_planta: 5 tipos de planta
                            label_planta2: transporte vs. otros
                            label_planta3: transporte vs. extracción
                            label_tension: nivel de tensión (2-3-4-5)
        :type class_problem: str
        :returns: datos, ids, time label, labels
        """
        feat = file_feat
        keys=feat.keys().tolist()

        keys.remove('Frontera')
        keys.remove('year_month')
        keys.remove('label_planta')
        keys.remove('label_planta2')
        keys.remove('label_planta3')
        keys.remove('label_tension')

        if class_problem=="label_planta":
            feat_removed_noclass=feat[feat[class_problem]!=6]
        elif class_problem=="label_planta2":
            feat_removed_noclass=feat[feat[class_problem]!=6]
        elif class_problem=="label_planta3":
            feat_removed_noclass=feat[feat[class_problem]!=6]
        elif class_problem=="label_tension":
            feat_removed_noclass=feat[feat[class_problem]!=0]
        else:
            raise ValueError("class problem"+ class_problem+" is not supported" )


        idfull=list(feat_removed_noclass['Frontera'])
        sesionfull=list(feat_removed_noclass['year_month'])
        labelfull=list(feat_removed_noclass[class_problem])

        ID=np.asarray([idfull[k] for k in range(len(idfull))])
        sesion=np.asarray([sesionfull[k] for k in range(len(sesionfull))])
        if class_problem=="label_planta":
            label=[k-1 for e, k in enumerate(labelfull)]
        elif class_problem=="label_planta2":
            label=[k-1 for e, k in enumerate(labelfull)]
        elif class_problem=="label_planta3":
            label=[k-1 for e, k in enumerate(labelfull)]

        elif class_problem=="label_tension":
            label=[k-2 for e, k in enumerate(labelfull)]
        else:
            raise ValueError("class problem"+ self.class_problem+" is not supported" )
        
        featmat=np.stack([feat_removed_noclass[k] for k in keys], axis=1)
        self.featnames=keys
        return featmat, np.hstack(ID), np.hstack(sesion), np.hstack(label)

    def train_test_split(self):
        """
        Segmenta los datos de forma estratificada entre train 
        y test un 80%-20% independiente de frontera.
        """
        for train_index, test_index in stratified_group_k_fold(self.X, self.y, self.ID, k=5, seed=42):

            self.Xtrain=self.X[train_index,:]
            self.Xtest=self.X[test_index,:]
            self.ytrain=self.y[train_index]
            self.ytest=self.y[test_index]
            self.IDtrain=self.ID[train_index]
            self.IDtest=self.ID[test_index]
            self.sesiontrain=self.sesion[train_index]
            self.sesiontest=self.sesion[test_index]

            break

    def standardize(self):
        """
        Estandarización Z-score para los datos de train y test.
        """
        scaler=StandardScaler()
        self.Xtrain=scaler.fit_transform(self.Xtrain)
        self.Xtest=scaler.transform(self.Xtest)

    def balance_train(self):
        """
        Addición de información a los datos de train para balancear el número de muestras.
        """
        nuq, counts=np.unique(self.ytrain, return_counts=True)
        order=np.argsort(counts)
        train_index=np.arange(len(self.ytrain))
        for i in range(len(counts)):
            phc=np.where(self.ytrain==nuq[i])[0]
            indextrain=list(train_index[phc])
            difnum=int(np.max(counts)-len(phc))
            if difnum<len(indextrain):
                ysample=random.sample(indextrain, difnum)
                self.Xtrain=np.vstack((self.Xtrain, self.Xtrain[ysample,:]))
                self.ytrain=np.hstack((self.ytrain,self.ytrain[ysample]))
            else:
                self.Xtrain=np.vstack((self.Xtrain, self.Xtrain[indextrain,:]))
                self.ytrain=np.hstack((self.ytrain,self.ytrain[indextrain]))

    def save_pickle(self, data, file_save):
        """
        Almacena el modelo entrenado para ser utilizado en la predicción de nuevos datos.

        :param data: Datos o modelo.
        :type data: RF model
        :param file_save: path donde será almacenado el modelo .pickle
        :type file_save: str
        """
        try:
            f = open(file_save, 'wb')
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
            f.close()
        except Exception as e:
            logger.error(f'Unable to save data to {file_save} -> {e}')


    def train(self):
        """
        Entrna el modelo de clasificación.
        """

        if self.classifier=="RF":
            clf=self.optimize_hyper_parametersRF()
            self.N=clf.best_params_['n_estimators']
            self.D=clf.best_params_['max_depth']

            self.ACCtrain=clf.best_score_
            logger.info(f"UAR training set= {self.ACCtrain*100}")
            self.CLF = RandomForestClassifier(n_estimators=self.N, max_depth=self.D, class_weight= 'balanced')

            if self.applyPCA:
                self.pca= PCA(0.8)
                self.pca.fit(self.Xtrain)
                Xtrainp=self.pca.fit_transform(self.Xtrain)
                self.CLF.fit(Xtrainp, self.ytrain)
            else:

                self.CLF.fit(self.Xtrain, self.ytrain)
            #save model
            self.save_pickle(self.CLF, self.model_path)


    def optimize_hyper_parametersRF(self):
        """
        Aleatorización de los hiperparametros de optimización de el clasificador RF.

        :returns: El clasificador RF
        :rtype: RF model 
        """
        parameters = {'n_estimators':randint(low=2, high=500), 
                      'max_depth': list(range(1, 200)) + [None], 
                      'class_weight': ['balanced']}
        RF = RandomForestClassifier()
        clf=RandomizedSearchCV(RF, parameters, n_jobs=4, cv=4, verbose=1, n_iter=100, scoring='balanced_accuracy')
        
        if self.applyPCA:
            pca= PCA(0.8)
            pca.fit(self.Xtrain)
            Xtrainp=pca.fit_transform(self.Xtrain)
            clf.fit(Xtrainp, self.ytrain)
        else:
        
            clf.fit(self.Xtrain, self.ytrain)

        return clf


    def predict(self):
        """
        Predice las etiquetas para los datos de test.
        """

        if self.classifier=="RF":

            if self.applyPCA:
                Xtestp=self.pca.transform(self.Xtest)
                self.y_pred= self.CLF.predict(Xtestp)
            else:
                self.y_pred= self.CLF.predict(self.Xtest)

    def predict2(self):
        """
        Predice las etiquetas para nuevos datos luego de haber entrenado el modelo de Random Forest.

        :returns: Los nombres de los medidores, etiquetas de tiempo year-month y las etiquetas predichas por el clasificador
        :rtype: dict
        """
        with open(self.model_path, 'rb') as f:
            CLF = pickle.load(f)
        scaler=StandardScaler()
        self.X = scaler.fit_transform(self.X)
        y_pred= CLF.predict(self.X)
        return {"sicCodes":self.ID.tolist(),"year-month": self.sesion.tolist(), "label":y_pred.tolist()}



    def evaluate_performance(self):
        """
        Evalua el desempeño del clasificador.
        
        :returns: Las metricas de desempeño
        :rtype: dict
        """

        acc=accuracy_score(self.ytest, self.y_pred)
        uar=balanced_accuracy_score(self.ytest, self.y_pred)
        fscore=f1_score(self.ytest, self.y_pred, average="macro")

        logger.info("---------TEST----------")
        logger.info(f"ACC= {acc*100}")
        logger.info(f"UAR= {uar*100}")
        logger.info(f"Fscore= {fscore*100}")
        
        if self.class_problem=="label_planta":
            label_l=["Refineria", "Transporte", "Biocombustible", "Edificio administrativo", "Extraccion"]

        elif self.class_problem=="label_planta2":
            label_l=["Otros", "Transporte"]

        elif self.class_problem=="label_planta3":
            label_l=["Extraccion", "Transporte"]

        elif self.class_problem=="label_tension":
            label_l=["2", "3", "4", "5"]
        else:
            raise ValueError("class problem"+ self.class_problem+" is not supported" )

        if self.classifier=="RF":
            importances = list(self.CLF.feature_importances_)
            order=np.argsort(importances)
            order=order[::-1]
            x_values = list(range(20))

            top_importance=[importances[order[o]] for o in range(20)]
            names=[self.featnames[order[o]] for o in range(20)]


        scalerT=StandardScaler()
        Xe=scalerT.fit_transform(self.X)
        X_embedded = TSNE(n_components=2).fit_transform(Xe)
        label_s=[]
        for j in self.y:
            label_s.append(label_l[j])

        dfclass=classification_report(self.ytest, self.y_pred,digits=4)

        logger.info(dfclass)
        np.set_printoptions(precision=2)

        results = {}
        results['UAR_dev'] = self.ACCtrain*100
        results['ACC_test'] = acc*100
        results['Fscore'] = fscore
        results['UAR_SP'] = uar*100
        results['N'] = self.N
        results['D'] = self.D
        results['label'] = self.y_pred.tolist()
        results['sicCodes'] = self.IDtest.tolist()
        results['year-month'] = self.sesiontest.tolist()
        results['right_label'] = self.ytest.tolist()
        
        return results
