
import numpy as np
import pandas as pd
from shared import logger
from six.moves import cPickle as pickle


class ReadPreprocess():

    """
    Leer y preprocesar los datos de los medidores.
    """

    def read_data(self, df):
        """
        Carga los datos de los medidores y crea un diccionario con los límites económicos como llaves.

        :param df: Dataframe de los datos.
        :type df: dataframe
        :returns: Diccionario con los límites económicos como llaves.
        :rtype: dict
        """
        try:
            for i in range(len(df['Fecha'])):  # Dando uniformidad al formato de fecha del dataset
                if isinstance(df['Fecha'][i], str):
                    df['Fecha'][i] = pd.to_datetime(df['Fecha'][i])
            df['Fecha'] = pd.to_datetime(df['Fecha'], format='%y%m%d')
            dfs=df.sort_values('Fecha')
            
            df_clean=self.imputate_data(dfs)
            dataSeriesByFrt2 = {}
            frtRef = df_clean['CodigoSIC'].unique()
        except Exception as e:
            error = f'Expected {e}. Please check the file structure'
            logger.error(error)
            return [400,error]
        for j in frtRef: # Creando un diccionario con los datos de cada frontera para 
                            #discriminar las graficas y los procesos de cada una
            frtIndex = df_clean.index[df_clean['CodigoSIC'] == j ].tolist()
            dummySeries = pd.DataFrame(df_clean.loc[frtIndex, :])
            dataSeriesByFrt2[j] = dummySeries.sort_values('Fecha')
        
        return dataSeriesByFrt2

    def filter_dataMA(self, df, order=7):
        """
        Suaviza la serie temporal con un filtro MA de orden 7 (una semana).
    
        :param df: Diccionario obtenido luego de usar self.read_data().
        :type df: dict
        :returns: Diccionario filtrado con el modelo del filtro MA
        :rtype: dict
        """

        frts=df.keys()

        coef=np.ones(order)/order

        for frt in frts:
            df_fontera=df[frt]
            for e2, power in enumerate(["Act", "Rea",  "Pen"]):
                for p in df_fontera.keys():
                    if p.find(power)>=0:
                        signal=df_fontera[p]
                        med=1
                        if sum(np.isnan(signal))>0:
                            med=np.nanmedian(signal)
                            pos=np.isnan(signal)
                            signal[pos]=med
                        signal=np.convolve(signal, coef, mode="same")
                        df_fontera[p]=signal


            df[frt]=df_fontera

        return df

    def imputate_data(self, df):
        """
        Inserción de datos para cada medidor con la mediana 
        donde no se realizó una reccolección de datos.
        
        :param df: Dataframe con los datos de los medidores.
        :type df: dataframe
        :returns: Los dataframe de los datos actualizados con la media cuando no se recolectaron datos.
        :rtype: dataframe
        """

        keys=df.keys()
        for k in keys:
            if k=="Fecha" or k=="CodigoSIC":
                continue

            frtRef = df['CodigoSIC'].unique()

            for sic in frtRef:

                frt = df[df['CodigoSIC'] == sic ]

                index_row = frt.index[frt['Act_Tot'] == 0 ].tolist() # Extrayendo las filas con Act_tot  = 0
                index_rownz = frt.index[frt['Act_Tot'] != 0 ].tolist() # Extrayendo las filas con Act_tot  != 0
            
                med=np.nanmedian(frt[k][index_rownz])
                df[k][index_row]=med

        return df

    def segment_data(self, dff, time_step):
        """
        Segmenta la serie de tiempo en meses separados.

        :param dff: Diccionario obtenido después de usar self.read_data().
        :type dff: dict
        :returns: Dos diccionarios: (1) Con una representación multi-secuencia, (2) Con muestras individuales dependiendo del sensor
        :rtype: dict, dict
        """

        Data1={"Index":[], "Frontera": [], "series":[], "year_month":[]}
        Data2={"Index":[], "Frontera": [], "series":[], "medidor": [], "year_month":[]}
        cont=0
        cont2=0        
        for k in dff.keys():
            fecha=np.asarray(dff[k]["Fecha"].values)
            year_month=np.asarray([str(fecha[j])[0:7] for j in range(len(fecha))])

            dff[k]["year_month"]=year_month
            unique_month=np.unique(year_month)
            
            df_fontera=dff[k]
            
            for e, j in enumerate(unique_month):
                Data1["Index"].append(cont)
                cont+=1

                month_row=df_fontera[df_fontera['year_month'] == j ]                    
                if time_step=="2month":
                    if e+1>=len(unique_month):
                        continue
                    month_row2=df_fontera[df_fontera['year_month'] == unique_month[e+1] ]
                elif time_step=="3month":
                    if e+2>=len(unique_month):
                        continue
                    month_row2=df_fontera[df_fontera['year_month'] == unique_month[e+1] ]
                    month_row3=df_fontera[df_fontera['year_month'] == unique_month[e+2] ]
                Data1["Frontera"].append(k)
                Data1["year_month"].append(j)
                mat=[]
                for e2, power in enumerate(["Act", "Rea",  "Pen"]):
                    for p in df_fontera.keys():
                        if p.find("Tot")>=0:
                            continue
                        if p.find(power)>=0:
                            Data2["Index"].append(cont2)
                            cont2+=1
                            Data2["Frontera"].append(k)
                            Data2["medidor"].append(p)
                            Data2["year_month"].append(j)

                            serie1=month_row[p].values
                            if len(serie1)==31:
                                serie1=serie1[0:30]
                            elif len(serie1)<30: 
                                temp = np.zeros(30)
                                temp[:len(serie1)]=serie1
                                serie1=temp

                            if time_step=="month":
                                Data2["series"].append(serie1)
                                mat.append(serie1)
                            elif time_step=="2month":
                                serie2=month_row2[p].values
                                if len(serie2)==31:
                                    serie2=serie2[0:30]
                                elif len(serie2)<30: 
                                    temp = np.zeros(30)
                                    temp[:len(serie2)]=serie2
                                    serie2=temp
                                serie=np.hstack((serie1,serie2))
                                Data2["series"].append(serie)
                                mat.append(serie)

                            elif time_step=="3month":
                                serie2=month_row2[p].values
                                if len(serie2)==31:
                                    serie2=serie2[0:30]
                                elif len(serie2)<30: 
                                    temp = np.zeros(30)
                                    temp[:len(serie2)]=serie2
                                    serie2=temp

                                serie3=month_row3[p].values
                                if len(serie3)==31:
                                    serie3=serie3[0:30]
                                elif len(serie3)<30: 
                                    temp = np.zeros(30)
                                    temp[:len(serie3)]=serie3
                                    serie3=temp

                                serie=np.hstack((serie1,serie2, serie3))
                                Data2["series"].append(serie)
                                mat.append(serie)

                mat=np.stack(mat)
                Data1["series"].append(mat)
        return Data1, Data2
    
    def save_pickle(self, data, file_save):
        """
        Guarda los diccionarios preprocesados como .pickle para ser procesados luego.
        
        :param data: Diccionario a ser guardado.
        :type data: dict
        :param file_save: Path en el cual será guardados el diccionario
        :type file_save: str
        """
        try:
            f = open(file_save, 'wb')
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
            f.close()
        except Exception as e:
            logger.error(f'Unable to save data to {file_save} -> {e}')

    def run_process(self,df,folder,userid):
        """
        Ejecuta paso a paso el proceso de tratamiento de datos para generar 
        el archivo Data_1mont que será usado posteriormente.

        :param df: Dataframe de los datos
        :type df: dataframe
        :param folder: Path donde será almacenado el archivo
        :type folder: str
        :param userid: Id del usuario actual
        :retunrs: lista con un código de estado y path/Error
        :rtype: list
        """
        try:
            df_all = self.read_data(df)
            df_all = self.filter_dataMA(df_all, order=7)

            Data1, Data2 = self.segment_data(df_all, "month")
            path = f'{folder}/{userid}_Data1_1month.pickle'
            self.save_pickle(Data1, path)

            return [200,path]
            
        except Exception as e:
            logger.error(f"Some errors ocurred parsing the file -> {e}")
            return [400,e]
