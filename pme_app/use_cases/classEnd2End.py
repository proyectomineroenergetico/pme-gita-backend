import random

import numpy as np
import torch
from pme_app.use_cases.DNN import (RNNSeries, SeqDataset, SeqDataset_pred,
                                   Trainer)
from pme_app.use_cases.utils.tools import stratified_group_k_fold
from shared import logger
from sklearn.metrics import (accuracy_score, balanced_accuracy_score,
                             classification_report, f1_score)
from sklearn.utils.class_weight import compute_class_weight


class Classend2end:

    """
    Clasificador basado en una 1D-CNN.

    :param file_tensor: Tensor creado con la clase FormTensors.py.
    :type file_tensor: tensor
    :param class_problem: Clase de problema seleccionado 
                          label_planta: 5 tipos de planta
                          label_planta2: transporte vs. otros
                          label_planta3: transporte vs. extracción
                          label_tension: nivel de tensión (2-3-4-5)
    :type class_problem: str
    :param model_path: Path donde se almacenará el modelo entrenado
    :type model_path: str
    :param evaluate: Booleano que indica si se clasificarán nuevos datos o no
    :type evaluate: bool
    """


    def __init__(self, file_tensor, class_problem, model_path,evaluate=True):

        self.file_tensor=file_tensor
        self.class_problem=class_problem
        self.model_path=model_path
        self.BATCH_SIZE=128
        self.LR=0.0001
        self.evaluate=evaluate
        self.X, self.y, self.ID, self.sesion,= self.read_tensor()



    def read_tensor(self):

        """
        Lee el tensor guardado por la clase FromTensors.py.

        :returns: Un conjunto de elementos (data, etiquetas, nombres de los medidores, etiqueta temporal) una vez parseado el tensor.
        :rtype: array, array, array, array
        """

        tensor=torch.load(self.file_tensor)
        if self.evaluate:
            if self.class_problem=="label_planta":
                yi=tensor['y1'].detach().numpy()
                index=np.where(yi!=6)[0]
                yi=yi-1
            elif self.class_problem=="label_planta2":
                yi=tensor['y2'].detach().numpy()
                index=np.where(yi!=6)[0]
                yi=yi-1
            elif self.class_problem=="label_planta3":
                yi=tensor['y3'].detach().numpy()
                index=np.where(yi!=6)[0]
                yi=yi-1
            elif self.class_problem =="label_tension":
                yi=tensor['y4'].detach().numpy()
                index=np.where(yi!=0)[0]
                yi=yi-2
            else:
                raise ValueError("class problem"+ self.class_problem+" is not supported" )
            
            y=torch.from_numpy(yi[index]).long()
            idfull=np.asarray([tensor['Frontera'][i] for i in index])
            sesionfull=np.asarray([tensor['year_month'][i] for i in index])
            X=tensor['X'][index, :,:]
        else:
            y=""
            idfull=np.asarray(tensor['Frontera'])
            sesionfull=np.asarray(tensor['year_month'])
            X=tensor['X']

        return X, y, np.asarray(idfull), np.asarray(sesionfull)

    
    def train_test_split(self):
        """
        Segmenta los datos de forma estratificada entre datos de train y datos 
        de test en un 80% - 20% independiente de la frontera.
        """
        yi=self.y.detach().numpy()

        for train_index, test_index in stratified_group_k_fold(yi, yi, self.ID, k=5, seed=42):

            self.Xtrain=self.X[train_index,:,:]
            self.Xtest=self.X[test_index,:,:]
            self.ytrain=self.y[train_index]
            self.ytest=self.y[test_index]
            self.IDtrain=self.ID[train_index]
            self.IDtest=self.ID[test_index]
            self.sesiontrain=self.sesion[train_index]
            self.sesiontest=self.sesion[test_index]
            break


    def standardize(self):

        """
        Estandarización Z-score para los datos de train y test.
        """
        self.avgseq=torch.mean(self.Xtrain, 0)
        self.stdseq=torch.std(self.Xtrain, 0)
        self.Xtrain=self.Xtrain-self.avgseq
        self.Xtrain=self.Xtrain/self.stdseq

        self.Xtest=self.Xtest-self.avgseq
        self.Xtest=self.Xtest/self.stdseq



    def balance_train(self):

        """
        Addición de información a los datos de train para balancear el número de muestras.
        """

        yi=self.ytrain.detach().numpy()
        nuq, counts=np.unique(yi, return_counts=True)

        order=np.argsort(counts)
        train_index=np.arange(len(yi))
        for i in range(len(counts)):
            phc=np.where(yi==nuq[i])[0]
            indextrain=list(train_index[phc])
            difnum=int(np.max(counts)-len(phc))
            if difnum<len(indextrain):
                ysample=random.sample(indextrain, difnum)
                self.Xtrain=torch.cat((self.Xtrain, self.Xtrain[ysample,:,:]), 0)
                self.ytrain=torch.cat((self.ytrain,self.ytrain[ysample]))
            else:
                self.Xtrain=torch.cat((self.Xtrain, self.Xtrain[indextrain,:,:]), 0)
                self.ytrain=torch.cat((self.ytrain,self.ytrain[indextrain]), 0)

    def train(self):
        """
        Entrna el modelo de clasificación.
        """

        self.balance_train()
        yi=self.ytrain.detach().numpy()
        weights=compute_class_weight(class_weight='balanced', classes=np.unique(yi), y=yi)
        class_weights=torch.from_numpy(weights).float()
        train_dataset=SeqDataset(self.Xtrain, self.ytrain, randomize=True)
        test_dataset=SeqDataset(self.Xtest, self.ytest, randomize=False)
        self.nclasses=len(weights)
        self.train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=self.BATCH_SIZE, drop_last=True, num_workers=0)
        self.test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1, drop_last=True, num_workers=0)
        self.DNN=RNNSeries(num_classes=self.nclasses)
        if torch.cuda.is_available():
            criterion = torch.nn.CrossEntropyLoss(weight=class_weights.cuda())
        else:
            criterion = torch.nn.CrossEntropyLoss(weight=class_weights)

        optimizer = torch.optim.Adam(self.DNN.parameters(), lr = self.LR)
        self.trainer=Trainer(self.DNN, optimizer, criterion, self.model_path)
        self.DNN=self.trainer.fit(self.train_loader, self.test_loader)


    def predict(self, num_classes=None):
        """
        Predice las etiquetas para los datos de test.

        :param num_classes: Número de clases que tendra el problema en la clasificación
        :type num_classes: int 
        :retunrs: None/diccionario con las prediciones de datos nuevos
        :rtype: |dict
        """

        if self.evaluate:
            self.nclasses=len(torch.unique(self.ytest))
            self.DNN=RNNSeries(num_classes=self.nclasses)
            self.DNN.load_state_dict(torch.load(self.model_path, map_location='cpu'))

            criterion = torch.nn.CrossEntropyLoss()
            optimizer = torch.optim.Adam(self.DNN.parameters(), lr = self.LR)
            self.trainer=Trainer(self.DNN, optimizer, criterion, self.model_path)
            test_dataset=SeqDataset(self.Xtest, self.ytest, randomize=False)
            self.test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1, drop_last=True, num_workers=0)

            y_pred=self.trainer.predict(self.DNN, self.test_loader)
            self.y_pred=np.argmax(y_pred, 1)
            self.ACCtrain=0
        else:

            self.avgseq=torch.mean(self.X, 0)
            self.stdseq=torch.std(self.X, 0)
            self.X=self.X-self.avgseq
            self.X=self.X/self.stdseq
            # self.nclasses=len(torch.unique(self.ytest))
            self.DNN=RNNSeries(num_classes=num_classes)
            self.DNN.load_state_dict(torch.load(self.model_path, map_location='cpu'))

            criterion = torch.nn.CrossEntropyLoss()
            optimizer = torch.optim.Adam(self.DNN.parameters(), lr = self.LR)
            self.trainer=Trainer(self.DNN, optimizer, criterion, self.model_path)
            dataset=SeqDataset_pred(self.X)
            self.test_loader = torch.utils.data.DataLoader(dataset, batch_size=1, drop_last=True, num_workers=0)

            y_pred=self.trainer.predict2(self.DNN, self.test_loader)
            self.y_pred=np.argmax(y_pred, 1)

            return {"sicCodes":self.ID.tolist(),"year-month": self.sesion.tolist(), "label":self.y_pred.tolist()}

    def evaluate_performance(self):
        """
        Evalua el desempeño del clasificador.
        
        :returns: Las metricas de desempeño
        :rtype: dict
        """
        acc=accuracy_score(self.ytest, self.y_pred)
        uar=balanced_accuracy_score(self.ytest, self.y_pred)
        fscore=f1_score(self.ytest, self.y_pred, average="macro")

        logger.info("---------TEST----------")
        logger.info(f"ACC= {acc*100}")
        logger.info(f"UAR= {uar*100}")
        logger.info(f"Fscore= {fscore*100}")
        
        if self.class_problem=="label_planta":
            label_l=["Refineria", "Transporte", "Biocombustible", "Edificio administrativo", "Extraccion"]

        elif self.class_problem=="label_planta2":
            label_l=["Otros", "Transporte"]

        elif self.class_problem=="label_planta3":
            label_l=["Extraccion", "Transporte"]

        elif self.class_problem=="label_tension":
            label_l=["2", "3", "4", "5"]
        else:
            raise ValueError("class problem"+ self.class_problem+" is not supported" )


        dfclass=classification_report(self.ytest, self.y_pred,digits=4)

        logger.info(dfclass)
        np.set_printoptions(precision=2)
        results = {}
        results['UAR_dev'] = self.ACCtrain*100
        results['ACC_test'] = acc*100
        results['Fscore'] = fscore
        results['UAR_SP'] = uar*100

        results['label'] = self.y_pred.tolist()
        results['sicCodes'] = self.IDtest.tolist()
        results['year-month'] = self.sesiontest.tolist()
        results['right_label'] = self.ytest.tolist()
        return results

