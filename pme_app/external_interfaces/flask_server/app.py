"""It creates the flask server with an environment and returns the server"""

from flask import Flask
from pme_app.external_interfaces.flask_server.settings import DevConfig
from shared import build


def create_app(config_object=DevConfig,env_state=""):
    """Creates the server
    Args:
        config_object (object, optional): Defaults to DevConfig.
            Adds a config when creating the app server
    Returns:
        class 'flask.app.Flask': A Flask app
    """
    if env_state ==  "production":
        app = Flask(__name__, static_folder=build, static_url_path="")
        app.config.from_object(config_object)

    else:
        app = Flask(__name__,template_folder='/home/steven/Documents/UdeA/maestria/Proyecto energía/PME_app/templates')
        app.config.from_object(config_object)
    return app
