from pme_app.domain.entities.userModel import UserModel
from shared import bcrypt, logger


def checkLogin(user_data):
    """
    Este metodo verifica si el usuario y el password son correctos
    y retorna el token correspondiente con la información del usuario.

    :param user_data: La información del usuario
    :type user_data: json
    :returns: | Una lista con el codigo de estado y token/error
    :rtype: str
    """
    try:
        # fetch the user data
        user = UserModel.query.filter_by(
            email=user_data.get('email')
        ).first()
        if user and bcrypt.check_password_hash(
            user.password, user_data.get('password')
        ):
            auth_token = user.encode_auth_token(user.id,user.admin,user.email)
            if auth_token:
                logger.info('message: Successfully logged in.')
                return [200, auth_token]
        else:
            logger.info('User or password are wrong or user does not exist.')
            return [404, None]
            
    except Exception as e:
        logger.error(f'Some error occurred in check login, please try again -> {e}')
        return [500, e]
