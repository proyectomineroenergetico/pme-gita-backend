from pme_app.domain.entities.userModel import UserModel
from shared import bcrypt, db, logger, rounds
from sqlalchemy import exc


def updatePassword(email, password):

    """
    Verifica si el usuario ya se encuentra registrado en la base 
    de datos y actualiza el password del usuario.

    :param email: Email del usuario
    :type email: str
    :param password: Contraseña nueva
    :type password: str
    :returns: lista con un código de estado y None\Error
    :rtype: list
    """
    # check if user already exists
    user = UserModel.query.filter_by(email=email).first()
    if user is not None:
        try:
            user.password=bcrypt.generate_password_hash(
            password,int(rounds)).decode()
            # insert the user
            db.session.add(user)
            db.session.commit()
            # generate the recovery token
            auth_token = user.encode_auth_token(user.id,user.admin,user.email)
            logger.info(f'message: User with email {email} successfully updated. token = {auth_token.decode()}')
            return [200, auth_token.decode()]
        except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
            logger.error(f'Some error occurred in update user. Please try again. -> {e}')
            return [401, f'Ocurrio un error actualizando la información del usuario. por favor pruebe de nuevo. -> {e}']
    else:
        logger.info('message: User does not exists. Please check the user info.')
        return [400, 'El usuario no existe, por favor verifique la información del usuario.']
