from pme_app.domain.entities.userModel import UserModel
from shared import db, logger
from sqlalchemy import exc


def registerUserdb(user_data):
    """
    Verifica si el usuario ya se encuentra registrado en la base 
    de datos en otro caso este debe ser registrado.

    :param user_data: Información del usuario que será registrado
    :type user_data: json
    :returns: lista con un código de estado y None\Error
    :rtype: list
    """
    # check if user already exists
    user = UserModel.query.filter_by(email=user_data.get('email')).first()
    if not user:
        if UserModel.check_password_requirements(passwd=user_data.get('password')) and UserModel.check_email_format(email=user_data.get('email')):
            try:
                user = UserModel(
                    email=user_data.get('email'),
                    password=user_data.get('password')
                )
                # insert the user
                db.session.add(user)
                db.session.commit()
                # generate the auth token
                auth_token = user.encode_auth_token(user.id,user.admin,user.email)
                logger.info('message: User successfully registered.')
                return [201, auth_token]
            except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
                logger.error(f'Some error occurred in register user. Please try again. -> {e}')
                return [401, None]
        else:
            logger.info('message: Please check the password requirements or email structure.')
            return [412, None]
    else:
        logger.info('message: User already exists. Please Log in.')
        return [202, None]
