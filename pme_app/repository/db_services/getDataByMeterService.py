import pandas as pd
from pme_app.domain.entities.metersModel import metersByUserModel
from pme_app.repository.db_services.getMetersService import getMeterList
from shared import logger
from sqlalchemy import exc


def getDataByMeter(frt, userid):
    """
    Retorna los datos para un medidor seleccionado.
    
    :param frt: Nombre del medidor
    :type frt: str
    :param userid: Id del usuario actual
    :type userid: int
    :returns: Datos para el medidor seleccionado
    :rtype: json
    """
    try:
        meters = metersByUserModel.query.filter_by(userId=userid,sicCode=frt).first()
        if meters is not None:
            data = meters.dataByMeter
        return data
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
        logger.error(f"Some error occurred in get data by meter -> {e}")

def getAllData(userid):
    """
    This method returns a dataframe with all meters and their respective data.
    
    :param userid: the id for the current user
    :type userid: int
    :returns: A dataframe with the data for all meters
    :rtype: dataframe
    """
    metersList = getMeterList(userid)
    meterDict = {}
    for frt in metersList:
        frt2 = frt.replace(f'_{userid}','')
        meterDict[frt2] = pd.read_json(getDataByMeter(frt,userid))
    return meterDict
