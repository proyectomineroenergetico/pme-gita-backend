from pme_app.domain.abstract_entities.chech_forms import utils_to_check_forms
from pme_app.domain.entities.userModel import UserModel
from shared import logger
from sqlalchemy import exc


def getUserInfo(auth_token):
    """
    Retorna la información de usuario almacenada en base de datos. 
    
    :param auth_token: Token del usuario actual
    :type auth_token: str
    :returns: Lista con un código de estado y información/Error
    :rtype: list
    """
    try:
        resp = UserModel.decode_auth_token(auth_token)
        if not isinstance(resp, str):
            user = UserModel.query.filter_by(id=resp).first()
            return[200,user]
        return [401,resp]                
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
        logger.error(f"Some error occurred in get user info -> {e}")
        return [401,e]

def checkUserEmail(email):
    """
    Verifica que el email proovisto para recuperar la contraseña
    Exista en la base de datos y esté asociado a un usuario de

    :param email: correo del usuario
    :type email: str
    :returns: estado de la verificación y mensaje|Error
    :rtype: list
    """
    try:
        check = utils_to_check_forms()
        if check.check_email_format(email):
            user = UserModel.query.filter_by(email=email).first()
            if user is not None:
                return [200,'ok']
            else:
                return [400, 'El email dado no existe']
        return [401,'Formato del email incorrecto, porfavor verifique']                
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
        logger.error(f"Some error occurred in check email -> {e}")
        return [400,e]
