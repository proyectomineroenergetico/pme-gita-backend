import pandas as pd
from pme_app.domain.entities.metersModel import metersByUserModel
from shared import db, logger
from sqlalchemy import exc


def createMeterTable(df, user_id):
    """
    Crea o actualiza los datos por medidor. 
    
    :param df: Datos para un medidor específico
    :type df: dataframe
    :param user_id: Id del usuario actual
    :type user_id: int
    :returns: Lista con un código de estado y None/Error
    :rtype: list
    """
    error = []
    for k,v in df.items():
        try:
            frt = f'{k}_{user_id}'
            data = pd.DataFrame(v)
            data.reset_index(drop=True, inplace=True)
            data = data.to_json()
            meter = metersByUserModel.query.filter_by(sicCode=frt,userId=user_id).first()
            if meter is not None:
                meter.dataByMeter = data
                logger.info(f"updating the data for {frt}")
            else:
                logger.info(f"adding the data for {frt}")
                meter = metersByUserModel(sicCode=frt,userId=user_id,dataByMeter=data)
            db.session.add(meter)
            db.session.commit()
        except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
            logger.error(f'Some errors occurred in create meters -> {e}')
            error.append(str(e))

    if error:
        logger.error(f"Some mistakes have occurred saving the meter object to the database: {error} ")
        return [400,error]
    return [200,None]

def deleteMeterTable(user_id):
    pass
