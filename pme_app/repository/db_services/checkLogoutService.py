from pme_app.domain.entities.blackListTokenModel import BlacklistToken
from pme_app.domain.entities.userModel import UserModel
from shared import db, logger
from sqlalchemy import exc


def checkLogout(auth_token):
    """
    Almacena los tokens de los usuarios en una lista negra 
    para que no puedan volver a ser utilizados una vez el 
    usuario cierra sesión.

    :param auth_token: el token correspondiente al usuario
    :type auth_token: str
    :returns: Una lista con un código de estado y "success"/error
    :rtype: list
    """

    resp = UserModel.decode_auth_token(auth_token)
    if not isinstance(resp, str):
        # mark the token as blacklisted
        blacklist_token = BlacklistToken(token=auth_token)
        try:
            # insert the token
            db.session.add(blacklist_token)
            db.session.commit()
            return [200,'success']
        except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
            logger.error(f'Some error occurred in check logout -> {e}')
            return [200, 'fail',e]
    else:
        return [401,resp]
        