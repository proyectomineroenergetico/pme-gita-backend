import json

from pme_app.domain.entities.performanceBytechniqueModel import \
    performanceByTechnique
from shared import db, logger
from sqlalchemy import exc


def savePerformanceData(user_id, data, model_name): #,technique,cp=None):
    """
    Almacena en la base de datos las métricas calculadas al momento de entrenar un modelo de ML.
    
    :param model_name: Nombre del modelo de ML
    :type model_name: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param data: Métricas de desempeño calculadas
    :type data: json
    :returns: lista con un código de estado y None/Error
    :rtype: list
    """
    try:
        if model_name != '':
            perByTech = performanceByTechnique.query.filter_by(model_name=model_name,userId=user_id).first()
        else:
            logger.error(f'Some error occurred in save performace metrics')
            return [400,'Some error occurred in save performace metrics']

        if perByTech is None:
            perByTech = performanceByTechnique(name=model_name,userId=user_id,dataByModel=data)
        else:
            perByTech.dataByModel = data

        db.session.add(perByTech)
        db.session.commit()
        return [200,None]
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
        logger.error(f'Some error occurred in save performace metrics -> {e}')
        return [400,e]

def getPerformanceByData(technique,user_id,cp=None,model_name='', all=False):
    """
    Retorna las métricas almacenadas en la base de datos de uno de los modelos de ML.

    :param technique: Nombre del la técnica de ML
    :type technique: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param cp: Clase de problema seleccionado
    :type cp: str
    :param all: Identifica todo el historial del usuario
    :type all: bool
    :returns: lista con un código de estado y data/Error
    :rtype: list
    """
    try:
        if all:
            perByTech = performanceByTechnique.query.filter_by(userId=user_id)
            lp = []
            for p in perByTech:
                lp.append([p.model_name,p.dataByModel])
            return [200,lp]
        else:
            if model_name != '':
                perByTech = performanceByTechnique.query.filter_by(model_name=model_name,userId=user_id).first()
            else:
                if cp is None:
                    perByTech = performanceByTechnique.query.filter_by(model_name=f'{technique}_{user_id}',userId=user_id).first()
                else:
                    perByTech = performanceByTechnique.query.filter_by(model_name=f'{technique}_{user_id}_{cp}',userId=user_id).first()
            
            if perByTech is not None:
                data = perByTech.dataByModel
                return [200,data]
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
        logger.error(f'Some error occurred in get performace metrics -> {e}')
        return [400,e]
        
