import json
from datetime import datetime

from pme_app.domain.entities.histBymethodModel import historyByTechnique
from shared import db, logger
from sqlalchemy import exc


def saveHistByTechnique(modelName,msg_a,user_id, status_a,date):
    """
    Guarda el historial de ejecución de cada método. 
    
    :param modelName: Nombre del modelol
    :type modelName: str
    :param msg_a: Mensage a guardar
    :type msg_a: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param status_a: Progreso de la ejecición
    :type status_a: int
    :param date: Fecha de ejecución de cada acción
    :type date: datetime
    """

    def myconverter(o):
        if isinstance(o, datetime):
            return o.__str__()
    try:
        hist = historyByTechnique.query.filter_by(model_name=modelName,userId=user_id).first()
        if hist is None:
            msg = {}
            msg['status'] = [status_a]
            msg['history'] = [msg_a]
            msg['date'] = [date]
            hist = historyByTechnique(name=modelName,userId=user_id,date=datetime.now(),msg=json.dumps(msg, default = myconverter))
        else:
            msg = json.loads(hist.history)
            if status_a == 0:
                status_b = [status_a]
                msg_b = [msg_a]
                date_b = [date]
                msg = {
                    'status':status_b,
                    'history':msg_b,
                    'date': date_b
                }
            else:
                msg['status'].append(status_a)
                msg['history'].append(msg_a)
                msg['date'].append(date)
            hist.history = json.dumps(msg, default = myconverter)
        logger.info(hist.history)
        db.session.add(hist)
        db.session.commit()
        return[200,None]               
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
        logger.error(f"Some error occurred in save history by Technique -> {e}")
        return [401,e]

def getHistByTechnique(modelName,user_id,all=False):

    """    
    Extrae el historial de ejecución de cada método. 
    
    :param modelName: Nombre del modelol
    :type modelName: str
    :param user_id: Id del usuario actual
    :type user_id: int
    :param all: Parámetro que permite retornar todo el historial por usuario
    :type all: bool
    :returns: Historial de ejecución, estado y fecha de ejecución
    :rtype: json
    """

    try:
        if all:
            hist = historyByTechnique.query.filter_by(userId=user_id)
            hm = []
            for h in hist:
                hm.append([h.model_name,h.history])
            return [200,hm]
        else:
            if modelName != '':
                hist = historyByTechnique.query.filter_by(model_name=modelName,userId=user_id).first()
                if hist is not None:
                    message = json.loads(hist.history)
            return [200,message]
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
        logger.error(f'Some error occurred in get history by technique -> {e}')
        return [400,e]
