from pme_app.domain.entities.metersModel import metersByUserModel
from shared import logger
from sqlalchemy import exc


def getMeterList(userid):
    """
    Retorna una lista de los medidores almacenados en la base de datos para el usuario actual.
    
    :param userid: Id del usuario actual
    :type userid: int
    :retunrs: Lista de nombres de los medidores
    :rtype: list
    """
    frt = []
    try:
        meters = metersByUserModel.query.filter_by(userId=userid)
        for meter in meters:
            frt.append(meter.sicCode)
        return frt
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError) as e:
        logger.error(f"Some error occurred in get meter list -> {e}")
