from pme_app.domain.entities.metersModel import metersByUserModel
from pme_app.repository.db_services.getMetersService import getMeterList
from shared import db, logger
from sqlalchemy import exc


def deleteMetersByUser(user_id):
    meters = getMeterList(user_id)
    error = []
    try:
        for frt in meters:
            meter = metersByUserModel.query.filter_by(sicCode=frt,userId=user_id).first()
            db.session.delete(meter)
            db.session.commit()
            logger.info(f'Meter {frt} was delete from DB successfully')
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
        error.append(e)
    if error:
        logger.error(f"Some mistakes have occurred deleting the meter from the database: {error} ")
        return [400,error]
    return [200,None]
