import json

from pme_app.domain.entities.filesByUserModel import filesByUserModel
from shared import db, logger
from sqlalchemy import exc


def saveFilesByUser(user_id, data_path="", features_path="",kmeans_path="",svm_path="",rf_path="",nn_path="",
                    lda_path="",labels_path="", features_path2="", banda=None, cp=None):
    """
   Este método guarda en base de datos las rutas para cada modelo de ML
   según la técnica utilizada bajo un formato JSON, adicionalmente guarda 
   los paths para las características extraídas y las etiquetas.
 
   :param user_id: El id correspondiente al usuario actual
   :type user_id: int
   :param data_path: El path donde será guardado el modelo
   :type data_path: str
   :param features_path: Path para las característica de frecuencia
   :type features_path: str
   :param kmeans_path: Path para el modelo de K-Means según la banda
   :type kmeans_path: str
   :param svm_path: Path para el modelo de SVM según la banda y la clase de problema elegido con las etiquetas
   :type svm_path: str
   :param rf_path: Path para el modelo de Random-Forest según la clase de problema elegido con las etiquetas
   :type fr_path: str
   :param nn_path: Path para el modelo de Neural-Network según la clase de problema elegido con las etiquetas
   :type nn_path: str
   :param features_path2: Path para las característica de tiempo
   :type features_path2: str
   :param labels_path: Path para las etiquetas utilizadas en el entrenamiento de las técnicas supervisadas
   :param banda: la banda de frecuencia correspondiente a la técnica entrenada
   :type banda: int
   :param cp: Clase de problema seleccionado según las etiquetas
   :type cp: str
   :returns: Una lista con el código de estado y None/error
   :rtype: list

    """

    try:
        fbu = filesByUserModel.query.filter_by(userId=user_id).first()
        if fbu is None:
            fbu = filesByUserModel(userId=user_id)
            if data_path != "":
                fbu.data_path = data_path
            if features_path != "":
                fbu.features_path = features_path
            if features_path2 != "":
                fbu.features_path2 = features_path2
            if kmeans_path != "":
                if banda >=0 and banda <=10:
                    j_kmeans = {'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }
                    j_kmeans[f'band_{banda}'] = kmeans_path
                    fbu.kmeans_path = json.dumps(j_kmeans)
            if svm_path != "":
                if cp != '':
                    if banda >=0 and banda <=10:
                        j_svm = {'label_planta':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' },
                            'label_planta2':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' },
                            'label_planta3':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }, 
                            'label_tension':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' },
                            'label_tension2':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }}
                    j_svm[cp][f'band_{banda}'] = svm_path
                    fbu.svm_path =  json.dumps(j_svm)
            if rf_path != "":
                if cp:
                    j_rf = {'label_planta':'', 'label_planta2':'',
                            'label_planta3':'', 'label_tension':'','label_tension2':''}
                    j_rf[cp] = rf_path
                    fbu.rf_path = json.dumps(j_rf)
            if nn_path != "":
                if cp:
                    j_nn = {'label_planta':'', 'label_planta2':'',
                            'label_planta3':'', 'label_tension':'','label_tension2':''}
                    j_nn[cp] = nn_path
                    fbu.nn_path = json.dumps(j_nn)
            if lda_path != "":
                if banda >=0 and banda <=10:
                    j_lda = {'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }
                    j_lda[f'band_{banda}'] = lda_path
                    fbu.lda_path =  json.dumps(j_lda)
            if labels_path != "":
                fbu.labels_path = labels_path
        else:
            if data_path != "":
                fbu.data_path = data_path
            if features_path != "":
                fbu.features_path = features_path
            if features_path2 != "":
                fbu.features_path2 = features_path2
            if kmeans_path != "":
                j_kmeans = json.loads(fbu.kmeans_path)
                if banda >=0 and banda <=10:
                    if j_kmeans == "":
                        j_kmeans = {'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }
                    j_kmeans[f'band_{banda}'] = kmeans_path
                fbu.kmeans_path = json.dumps(j_kmeans)
            if svm_path != "":
                j_svm = json.loads(fbu.svm_path)
                if cp != '':
                    if banda >=0 and banda <=10:
                        if j_svm == "" :
                            j_svm = {'label_planta':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                                'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                                'band_8':'', 'band_9':'', 'band_10':'' },
                                'label_planta2':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                                'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                                'band_8':'', 'band_9':'', 'band_10':'' },
                                'label_planta3':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                                'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                                'band_8':'', 'band_9':'', 'band_10':'' }, 
                                'label_tension':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                                'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                                'band_8':'', 'band_9':'', 'band_10':'' },
                                'label_tension2':{'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                                'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                                'band_8':'', 'band_9':'', 'band_10':'' }}
                        j_svm[cp][f'band_{banda}'] = svm_path
                fbu.svm_path = json.dumps(j_svm)
            if rf_path != "":
                j_rf = json.loads(fbu.rf_path)
                if cp:
                    if j_rf == "":
                        j_rf = {'label_planta':'', 'label_planta2':'',
                                'label_planta3':'', 'label_tension':'','label_tension2':''}
                    j_rf[cp] = rf_path
                fbu.rf_path = json.dumps(j_rf)
            if nn_path != "":
                j_nn = json.loads(fbu.nn_path)
                if cp:
                    if j_nn == "":
                        j_nn = {'label_planta':'', 'label_planta2':'',
                                'label_planta3':'', 'label_tension':'','label_tension2':''}
                    j_nn[cp] = nn_path
                fbu.nn_path = json.dumps(j_nn)
            if lda_path != "":
                j_lda = json.loads(fbu.lda_path)
                if banda >=0 and banda <=10:
                    if j_lda == "":
                        j_lda = {'band_0':'', 'band_1':'', 'band_2':'', 'band_3':'',
                            'band_4':'', 'band_5':'', 'band_6':'', 'band_7':'',
                            'band_8':'', 'band_9':'', 'band_10':'' }
                    j_lda[f'band_{banda}'] = lda_path
                fbu.lda_path = json.dumps(j_lda)
            if labels_path != "":
                fbu.labels_path = labels_path
        db.session.add(fbu)
        db.session.commit()
        return [200,None]
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
        logger.error(f" Some errors occurred in Save files by user ->{e}")
        return [400,e]

def getFilesByUser(user_id, data_path=False, features_path2=False, features_path=False,kmeans_path=False,
                    svm_path=False,rf_path=False,nn_path=False,lda_path=False,labels_path=False, banda=None, cp=None):
    """
    Este método extrae de la base de datos las rutas para cada modelo entrenado de ML
    según la técnica utilizada o los paths para las características y las etiquetas.
    
    :param user_id: El id correspondiente al usuario actual
    :type user_id: int
    :param data_path: El path donde será guardado el modelo
    :type data_path: str
    :param features_path: Path para las característica de frecuencia
    :type features_path: str
    :param kmeans_path: Path para el modelo de K-Means según la banda
    :type kmeans_path: str
    :param svm_path: Path para el modelo de SVM según la banda y la clase de problema elegido con las etiquetas
    :type svm_path: str
    :param rf_path: Path para el modelo de Random-Forest según la clase de problema elegido con las etiquetas
    :type fr_path: str
    :param nn_path: Path para el modelo de Neural-Network según la clase de problema elegido con las etiquetas
    :type nn_path: str
    :param features_path2: Path para las característica de tiempo
    :type features_path2: str
    :param labels_path: Path para las etiquetas utilizadas en el entrenamiento de las técnicas supervisadas
    :param banda: la banda de frecuencia correspondiente a la técnica entrenada
    :type banda: int
    :param cp: Clase de problema seleccionado según las etiquetas
    :type cp: str
    :returns: Una lista con el código de estado y Path/error
    :rtype: list

    """
    try:
        fbu = filesByUserModel.query.filter_by(userId=user_id).first()
        if fbu is not None:
            if data_path:
                path = fbu.data_path
            if features_path:
                path = fbu.features_path
            if kmeans_path:
                j_kmeans = json.loads(fbu.kmeans_path)
                path = j_kmeans[f'band_{banda}']
            if svm_path:
                j_svm = json.loads(fbu.svm_path)
                path = j_svm[cp][f'band_{banda}']
            if rf_path:
                j_rf = json.loads(fbu.rf_path)
                path = j_rf[cp]
            if nn_path:
                j_nn = json.loads(fbu.nn_path)
                path = j_nn[cp]
            if lda_path:
                j_lda = json.loads(fbu.lda_path)
                path = j_lda[f'band_{banda}']
            if labels_path:
                path = fbu.labels_path
            if features_path2:
                path = fbu.features_path2
            return [200,path]
            
    except (Exception, exc.SQLAlchemyError, exc.DBAPIError)  as e:
        logger.error(f'Some errors occurred in get file path by user -> {e}')
        return [400,e]

