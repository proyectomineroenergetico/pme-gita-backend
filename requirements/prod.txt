alabaster==0.7.12
alembic==1.5.8
asgiref==3.3.1
astroid==2.5.1
Babel==2.9.0
bcrypt==3.2.0
blinker==1.4
certifi==2020.12.5
cffi==1.14.5
chardet==4.0.0
click==7.1.2
cycler==0.10.0
dataclasses==0.6
decorator==4.4.2
docutils==0.16
Flask==1.1.2
Flask-Bcrypt==0.7.1
Flask-Cors==3.0.10
Flask-JWT==0.3.2
Flask-Mail==0.9.1
Flask-Migrate==2.7.0
Flask-Script==2.0.6
Flask-SQLAlchemy==2.5.1
Flask-SSE==1.0.0
future==0.18.2
gevent==21.1.2
greenlet==1.0.0
gunicorn==20.0.4
idna==2.10
imageio==2.9.0
imagesize==1.2.0
isort==5.8.0
itsdangerous==1.1.0
Jinja2==2.11.3
joblib==1.0.1
jsonlib-python3==1.6.1
kiwisolver==1.3.1
lazy-object-proxy==1.6.0
Mako==1.1.4
MarkupSafe==1.1.1
matplotlib==3.3.4
mccabe==0.6.1
networkx==2.5
numpy==1.20.1
packaging==20.9
pandas==1.1.2
patsy==0.5.1
Pillow==8.1.2
psycopg2==2.8.6
psycopg2-binary==2.8.6
pycparser==2.20
Pygments==2.8.1
PyJWT==1.4.2
pylint==2.7.2
pylint-flask==0.6
pylint-flask-sqlalchemy==0.2.0
pylint-plugin-utils==0.6
pyparsing==2.4.7
python-dateutil==2.8.1
python-dotenv==0.15.0
python-editor==1.0.4
pytz==2021.1
PyWavelets==1.1.1
redis==3.5.3
requests==2.25.1
rq==1.7.0
scikit-image==0.18.1
scikit-learn==0.23.2
scipy==1.5.2
six==1.12.0
sklearn==0.0
snowballstemmer==2.1.0
Sphinx==3.5.3
sphinx-rtd-theme==0.5.1
sphinxcontrib-applehelp==1.0.2
sphinxcontrib-devhelp==1.0.2
sphinxcontrib-htmlhelp==1.0.3
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.3
sphinxcontrib-serializinghtml==1.1.4
SQLAlchemy==1.4.3
statsmodels==0.11.1
threadpoolctl==2.1.0
tifffile==2021.3.17
toml==0.10.2
torch==1.7.0
torchsummary==1.5.1
tqdm==4.23.4
typing-extensions==3.7.4.3
urllib3==1.26.4
Werkzeug==1.0.1
wrapt==1.12.1
xlrd==1.2.0
zipp==3.4.1
zope.event==4.5.0
zope.interface==5.2.0
