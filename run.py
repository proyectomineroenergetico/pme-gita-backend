#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run.py

Este archivo registra todos las rutas blueprints creadas 
para exponer los diferentes servicios y corre la aplicación 
con los parámetros respectivos del .env file
"""
from app import app
from pme_app.interfaces_adapters.rest_adapters.web_services.checkEmailForRecoveryService import \
    api_recovery
from pme_app.interfaces_adapters.rest_adapters.web_services.dataByMeterService import \
    api_get_data_by_meter
from pme_app.interfaces_adapters.rest_adapters.web_services.featuresAndTechniquesService import \
    api_featuresAndTech
from pme_app.interfaces_adapters.rest_adapters.web_services.getPerformanceDataByModel import \
    api_perByModel
from pme_app.interfaces_adapters.rest_adapters.web_services.histByTechniqueService import \
    api_histByTech
from pme_app.interfaces_adapters.rest_adapters.web_services.indexService import \
    index_r
from pme_app.interfaces_adapters.rest_adapters.web_services.loadFileService import \
    api_upload_file
from pme_app.interfaces_adapters.rest_adapters.web_services.loadLabelsService import \
    api_upload_labels_file
from pme_app.interfaces_adapters.rest_adapters.web_services.loginService import \
    api_login
from pme_app.interfaces_adapters.rest_adapters.web_services.logoutService import \
    api_logout
from pme_app.interfaces_adapters.rest_adapters.web_services.meterListService import \
    api_get_meters
from pme_app.interfaces_adapters.rest_adapters.web_services.newPasswordForRecoveryService import \
    api_reset
from pme_app.interfaces_adapters.rest_adapters.web_services.registerUserService import \
    api_register_user
from pme_app.interfaces_adapters.rest_adapters.web_services.removeDatabaseByUserService import \
    api_delete_metters
from pme_app.interfaces_adapters.rest_adapters.web_services.rootService import \
    api_home
from pme_app.interfaces_adapters.rest_adapters.web_services.userInfoService import \
    api_user_info
from shared import SSE, app_port, host

from gevent import monkey; monkey.patch_all()


app.register_blueprint(index_r)
app.register_blueprint(api_home)
app.register_blueprint(api_upload_file)
app.register_blueprint(api_get_meters)
app.register_blueprint(api_get_data_by_meter)
app.register_blueprint(api_user_info)
app.register_blueprint(api_register_user)
app.register_blueprint(api_login)
app.register_blueprint(api_logout)
app.register_blueprint(api_featuresAndTech)
app.register_blueprint(api_upload_labels_file)
app.register_blueprint(api_perByModel)
app.register_blueprint(api_histByTech)
app.register_blueprint(api_recovery)
app.register_blueprint(api_reset)
app.register_blueprint(api_delete_metters)
app.register_blueprint(SSE, url_prefix='/stream')


if __name__ == '__main__':
    app.run(host=host,port=app_port)
